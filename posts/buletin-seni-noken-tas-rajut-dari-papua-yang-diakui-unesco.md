---
author: Kementerian Seni dan Budaya
title: 'Buletin Seni: Noken, Tas Rajut dari Papua yang Diakui Unesco'
date: 2020-12-23
thumbnail: "/thumbail-noken.jpg"

---
_Naskah:_

Noken sudah tersohor di tanah Papua dengan kearifan sosial budayanya. Noken dibuat dari berbagai bahan serat pohon, kulit kayu dan daun pandan seta rumput rawa. Noken Papua digunakan untuk mengisi, menyimpan, dan membawa berbagai barang.

Biasanya noken dipakai untuk membawa hasil-hasil pertanian dan membawa barang dagangan ke pasar. Penggunaan noken pun cukup unik. Di mana tidak memakai tangan tapi kepala.

![](/gambar-1-noken.jpg)

Bahkan noken ditetapkan sebagai sebagai warisan budaya tak benda dari UNESCO pada, 4 Desember 2012 lalu. Noken digolongkan dalam kategori 'in Need of Urgent Safeguarding' atau warisan budaya yang membutuhkan perlindungan mendesak.

![](/gambar-2-noken.jpg)

Bagi suku Dani yang bermukim di pegunungan tengah Papua, noken dijadikan sebagai alat tukar. Noken dengan jumlah tertentu dapat ditukar dengan seekor babi. Tidak hanya itu, perempuan Papua yang tidak bisa membuat Noken juga tidak boleh menikah, sampai ia benar-benar bisa membuat Noken dengan tangannya sendiri. Namun, seiring dengan perubahan jaman, adat istiadat seperti itu sudah mulai terkikis dan perlahan mulai hilang.

Sumber referensi:  
 [https://kebudayaan.kemdikbud.go.id/ditwdb/apa-itu-noken-cermin-noken-papua/](https://kebudayaan.kemdikbud.go.id/ditwdb/apa-itu-noken-cermin-noken-papua/ "https://kebudayaan.kemdikbud.go.id/ditwdb/apa-itu-noken-cermin-noken-papua/")  
 [https://www.kompas.com/skola/read/2020/04/25/173000869/noken-tas-tradisional-khas-papua-yang-diakui-unesco?page=all](https://www.kompas.com/skola/read/2020/04/25/173000869/noken-tas-tradisional-khas-papua-yang-diakui-unesco?page=all "https://www.kompas.com/skola/read/2020/04/25/173000869/noken-tas-tradisional-khas-papua-yang-diakui-unesco?page=all")

[#Kemensenbud](https://www.instagram.com/explore/tags/kemensenbud/)[#BuletinSeni](https://www.instagram.com/explore/tags/buletinseni/)[#KabinetSodara](https://www.instagram.com/explore/tags/kabinetsodara/)[#BEMUEU2021](https://www.instagram.com/explore/tags/bemueu2021/)