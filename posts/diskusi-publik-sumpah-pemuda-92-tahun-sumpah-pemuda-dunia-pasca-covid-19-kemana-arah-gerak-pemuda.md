---
author: Kementerian Kajian dan Aksi Strategi
title: Diskusi Publik Sumpah Pemuda "92 Tahun Sumpah Pemuda Dunia Pasca Covid-19,
  Kemana Arah Gerak Pemuda?"
date: 2020-10-29
thumbnail: "/762.JPG"

---
Jakarta - Halo para sodara!

Kementrian Kajian dan Aksi Strategis BEM Universitas Esa Unggul memperingati Sumpah Pemuda dengan menggelar Diskusi Publik dengan tema "92 Sumpah Pemuda: Dunia Paska Covid-19, Kemana Arah Gerak Pemuda" acara ini digelar di Meruya Utara, Jakarta Barat.

Acara tersebut dilaksanakan dengan konsep Dialog Interaktif sesuai protocol Covid-19 dengan Menghadirkan Narasumber yang luar biasa yaitu Alfian Akbar Balyanan (Founder of Mimika Youth Center), Nasrul Pradana (Founder of Asosiasi Praktisi Manajemen Indonesia), Surya Muhammad Nur (Akademik Politik Kebangsaan), dan Rizky Al Farizie (Presiden Mahasiswa Universitas Esa Unggul).

![](/1.JPG)![](/2.JPG)![](/3.JPG)![](/4.JPG)

\#SumpahPemuda #Kemenkastrat #Kemenkominfo #KabinetSodara #BEMUEU2021