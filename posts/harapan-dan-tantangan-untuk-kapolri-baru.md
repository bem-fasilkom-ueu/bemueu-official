---
author: Kementerian Kajian dan Aksi Strategi
title: Harapan dan Tantangan untuk Kapolri Baru
date: 2021-01-29
thumbnail: "/012.jpg"

---
"Kami berangkat dari potret Polri di masyarakat dan apa yang diharapkan masyarakat terhadap Polri dan bagaimana menampilkan Polri yang Tegas namun Humanis," Listyo Sigit Prabowo

5 Rencana Kerja Sigit saat _Feet and Proper Test_ ()

1\. Menghidupkan lagi PAM SWAKARSA atau Pasukan Pengamanan Masyarakat Swakarsa)

2\. Memangkas peran Kepolisian Sektor (Polsek)

3\. Mengembangkan Tilang Elektronik

4\. Kasus Tewasnya Laskar FPI

5\. Angka Aduan Komnas HAM

Menghidupkan lagi PAM SWAKARSA atau Pasukan Pengamanan Masyarakat Swakarsa)

![](/013.jpg)

Dalam sejarah Indonesia tercatat bahwa Pam Swakarsa berkali-kali terlibat bentrokan dengan pengunjuk rasa yang menentang Sidang Istimewa MPR (SI MPR) tahun 1998. Pam Swakarsa juga dinilai diskriminatif terhadap mahasiswa waktu itu. Sehingga masyarakat menjadi antipati pada kelompok Pam Swakarsa. Bahkan sejak sebelum Sidang Istimewa MPR dimulai, bentrokan fisik antara masyarakat dengan anggota kelompok Pam Swakarsa pun sudah terjadi.

1\. Memangkas peran Kepolisian Sektor (Polsek)

Salah satu rencana aksinya ialah mengubah kewenangan Polsek hanya untuk pemeliharaan keamanan dan ketertiban masyarakat. Hal ini menuai banyak kritikan Sebab, rencana tersebut akan menjauhkan masyarakat dari penegakan hukum yang dibutuhkan.

Pengamat Politik Keamanan Universitas Padjadjaran (Unpad), Muradi memaparkan, peran Polsek sebenarnya tidak dikurangi, tetapi terdapat beberapa Polsek yang tidak perlu memiliki fungsi penyidikan. Ia menyebut, rencana tersebut tidak berlaku untuk seluruh Polsek dan jika dilihat dari tahapannya akan dimulai di wilayah perkotaan terlebih dahulu.

2\. Mengembangkan Tilang Elektronik

ETLE(Electronic Traffic Law Enforcement) sudah berjalan empat tahun sejak dilakukan pertama kali pada 2016 oleh Polda Metro Jaya. Meski begitu, penerapan ETLE bukannya tanpa kendala. Saat ini kendala paling signifikan adalah belum meratanya infrastruktur tilang elektronik di semua daerah. Untuk Jakarta saja, kamera tilang elektronik baru terdapat di jalan-jalan protokol. Sementara jalan lainnya masih menggunakan tilang konvensional.

Sejauh ini, berdasarkan data yang diperoleh Kompas.com dari Divisi Humas Polri, sistem ETLE sudah beroperasi di DKI Jakarta, Jawa Timur, dan Yogyakarta. Lokasi kamera ETLE paling banyak terpasang di Jakarta dengan total 58 titik antara lain, jembatan penyeberangan orang (JPO) Kementerian Pariwisata, kawasan Patung Kuda di Jalan Medan Merdeka Selatan, serta perempatan Sarinah.

3\. Kasus Tewasnya Laskar FPI

Anggota Komnas HAM sekaligus Ketua Tim Penyelidikan Peristiwa Karawang Choirul Anam mengatakan pihaknya menemukan terdapat enam anggota Laskar FPI yang tewas dalam dua konteks peristiwa berbeda.

Disimpulkan dua anggota FPI meninggal dunia dalam peristiwa saling serempet antara mobil yang mereka pergunakan dengan polisi, hingga terjadi kontak tembak di antara Jalan Internasional Karawang sampai KM 49 Tol Jakarta-Cikampek dan berakhir di KM 50. Kemudian, empat orang lainnya masih hidup dan dibawa polisi, dan diduga ditembak hingga tewas di dalam mobil petugas saat dalam perjalanan dari KM 50 menuju Markas Polda Metro Jaya.

Komnas HAM menduga terdapat pelanggaran HAM atas tewasnya empat Laskar FPI yang dilakukan oleh aparat Kepolisian. Untuk itu, Komnas HAM merekomendasikan para pelaku diproses hukum melalui mekanisme pengadilan pidana.

4\. Angka Aduan Komnas HAM

Dalam catatan akhir tahun kinerja Komisi Nasional Perlindungan Hak Asasi Manusia (Komnas HAM) 2019 yang dirilis pada medio 2020, kepolisian menjadi lembaga yang paling banyak diadukan. Yaitu ada 744 aduan.

Berdasarkan catatan Komnas HAM, aduan mengenai proses hukum tidak sesuai prosedur mendominasi dengan 46,8 persen dari seluruh aduan kepada Polri. Lebih lanjut, ada 22,3 persen mengadu lambatnya penanganan kasus.

Hal ini perlu diselesaikan secara tebuka agar isu tidak menjadi liar di masyarakat sehingga publik bisa Kembali percaya secara penuh kepada apparat

Harapan untuk Kapolri Baru

Semoga Polri menjadi semakin profesional kedepannya dalam mengayomi masyarakat dan memberikan rasa aman

Referensi :

[https://nasional.tempo.co/read/1427064/resmi-dilantik-jadi-kapolri-listyo-sigit-kami-tampilkan-polri-yang-humanis](https://nasional.tempo.co/read/1427064/resmi-dilantik-jadi-kapolri-listyo-sigit-kami-tampilkan-polri-yang-humanis "https://nasional.tempo.co/read/1427064/resmi-dilantik-jadi-kapolri-listyo-sigit-kami-tampilkan-polri-yang-humanis")

[https://nasional.tempo.co/read/1426995/hari-ini-dilantik-jadi-kapolri-ini-catatan-dan-pr-untuk-listyo-sigit/](https://nasional.tempo.co/read/1426995/hari-ini-dilantik-jadi-kapolri-ini-catatan-dan-pr-untuk-listyo-sigit/ "https://nasional.tempo.co/read/1426995/hari-ini-dilantik-jadi-kapolri-ini-catatan-dan-pr-untuk-listyo-sigit/")

[https://www.rmol.id/peran-polsek-akan-diubah-pengamat-untuk-efektifkan-kerja-polisi](https://www.rmol.id/peran-polsek-akan-diubah-pengamat-untuk-efektifkan-kerja-polisi "https://www.rmol.id/peran-polsek-akan-diubah-pengamat-untuk-efektifkan-kerja-polisi")

[https://www.cnnindonesia.com/nasional/20210108093918-12-591046/komnas-ham-polisi-langgar-ham-soal-tewasnya-4-laskar-fpi](https://www.cnnindonesia.com/nasional/20210108093918-12-591046/komnas-ham-polisi-langgar-ham-soal-tewasnya-4-laskar-fpi "https://www.cnnindonesia.com/nasional/20210108093918-12-591046/komnas-ham-polisi-langgar-ham-soal-tewasnya-4-laskar-fpi")

[https://nasional.tempo.co/read/1425775/2-permintaan-komnas-ham-untuk-kapolri-terpilih-listyo-sigit/](https://nasional.tempo.co/read/1425775/2-permintaan-komnas-ham-untuk-kapolri-terpilih-listyo-sigit/ "https://nasional.tempo.co/read/1425775/2-permintaan-komnas-ham-untuk-kapolri-terpilih-listyo-sigit/")