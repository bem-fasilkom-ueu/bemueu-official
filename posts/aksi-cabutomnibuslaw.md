---
author: Kementerian Kajian dan Aksi Strategi
title: 'Aksi #CabutOmnibusLaw'
date: 2020-10-11
thumbnail: "/324.jpg"

---
\[PRESS RELEASE AKSI #CABUTOMNIBUSLAW RUU CIPTA KERJA\]

Assalamualaikum Warahmatullahi Wabarakatuh. Pada 08 Oktober 2020, BEM Universitas Esa Unggul melakukan aksi penolakan atas pengesahan UU Cipta Kerja Omnibus Law yang dilakukan DPR. Aksi yang dihadiri oleh berbagai kampus di Jakarta bersama Buruh dan Pekerja menggelar aksi dengan longmarch dari Universitas Bung Karno menuju Istana Negara.

Massa aksi berkumpul terlebih dahulu di Universitas Esa Unggul kemudian berangkat menuju Universitas Bung Karno pada pukul 09:00. Setelah sampai di UBK dilanjutkan dengan Orasi Presiden Mahasiswa dari berbagai kampus yang tergabung dalam Aliansi.

![](/poi.JPG)

Massa aksi dengan penuh semangat memulai longmarch dari Universitas Bung Karno menuju Istana Negara pada pukul 11:30. Namun sebelum menyentuh titik aksi kita tertahan di Jl.Merdeka Selatan, tepatnya depan Gedung Kedutaan Besar Amerika pada sekitar pukul 15:30 dikarenakan mulai terdengar suara tembakan gas air mata dari aparat.

Pada pukul 16:00 keadaan sudah semakin chaos dan kami memutuskan menarik mundur dan evakuasi di depan Tugu Tani bersama dengan massa aksi lainnya. Pada sekitar pukul 16:40 karena tembakan gas air mata sampai pada daerah Tugu tani kami memutuskan untuk kembali ke Universitas Bung Karno melewati Kelurahan Kwitang yang mana di sepanjang jalan warga memberikan ucapan semangat dan logistik ke massa aksi yang lewat. Setelah sampai di Universitas Bung Karno kita kembali melakukan pendataan mahasiswa Universitas Esa Unggul oleh Korlap tiap fakultas.

Untuk kabar sampai saat ini Alhamdulilah semua Mahasiswa Universitas Esa Unggul yang terdata dalam Korlap dipastikan sudah aman. Maka dengan ini kami dari BEM Universitas Esa Unggul menginstruksikan kepada seluruh civitas mahasiswa Universitas Esa Unggul untuk tetap satu komando dan tidak mengambil langkah langkah lain yang merugikan berbagai pihak, tetap satu komando dan bersama kita mengawal UU Omnibus Law. Terima kasih juga untuk teman-teman Mahasiswa/i Universitas Esa Unggul sudah meluangkan waktu,tenaga, aspirasi dan pengorbanannya membantu menyuarakan suara Rakyat Indonesia dengan turun ke jalan bersama. Semoga suara kita semua didengar dan harapan kita semua bisa terealisasi.