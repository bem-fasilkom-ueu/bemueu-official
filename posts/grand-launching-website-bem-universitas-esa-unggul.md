---
author: Kementerian Komunikasi dan Informatika
title: Grand Launching Website BEM Universitas Esa Unggul
date: 2021-01-12
thumbnail: "/thumbnail-12-web.jpg"

---
Telah di selenggarakan “Grand Launching Website BEM UEU" pada:

Hari : Selasa

Tanggal : 12 Januari 2021

Jam: 13.00 - 14.00 WIB

Acara grand launching ini dilaksanakan secara hybrid (tamu undangan hadir secara virtual melalui zoom meeting).

Kami BEM Universitas Esa Unggul periode 2020/2021 mengucapkan terima kasih kepada Wakil Rektor Bidang kemahasiswaan dan alumni, Biro Kemahasiswaan, Biro Humas, Biro IT, ILUNI, MPMU, DPMU, Anggota BEMU, dan BEMF yang telah ikut berpartisipasi dalam acara

"Grand Launching Website BEM UEU”

Semoga dengan adanya Website BEM UEU ini bisa menjadikan BEM UEU lebih profesional dalam hal penyebaran informasi, serta menerapkan penggunaan teknologi informasi secara kreatif dan konstruktif.

![](/gambar-1-12-web.jpg)

Acara dipandu oleh MC

![](/gambar-2-12-web.jpg)

Sambutan oleh Febriano Aditiya (Menteri Komunikasi dan Informatika BEM UEU 20/21).

![](/gambar-3-12-web.jpg)

Sambutan oleh Rizky Al Farizie (Presiden Mahasiswa UEU 20/21).

![](/gambar-4-12-web.png)

Sambutan oleh Metha Magdalena Tenggara (SekJen ILUNI UEU).

![](/gambar-5-12-web.png)

Sambutan oleh Ari Pambudis, S.Kom, M.Kom (Wakil Rektor Bidang Kemahasiswaan dan Alumnie UEU).

![](/gambar-6-12-web.jpg)

Peresmian dengan pemotongan pita.

![](/gambar-7-12-web.jpg)

Pemotongan tumpeng.

![](/gambar-8-12-web.jpg)

Penampilan Hiburan oleh Riana Magdalena.

![](/gambar-9-12-web.jpg)

Pembacaan doa oleh Muh Faiz Azhar.

![](/gambar-10-12-web.png)

Tamu Undangan.

\#KemenKominfo #KabinetSodara #BEMUEU2021