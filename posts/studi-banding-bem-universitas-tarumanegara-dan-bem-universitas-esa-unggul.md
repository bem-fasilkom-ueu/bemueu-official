---
author: Kementerian Luar Kampus
title: Studi Banding BEM Universitas Tarumanegara dan BEM Universitas Esa Unggul
date: 2020-11-28
thumbnail: "/thumbnail-studi-banding-bem-universitas-tarumanegara-dan-bem-universitas-esa-unggul.jpeg"

---
Kementrian luar kampus BEMUEU mengucapkan terimakasih kepada departemen eksternal kampus universitas Tarumanagara atas undangannya dalam acara studi banding antar kampus pada tanggal 28 November 2020

BEMUEU berharap dengan terlaksananya kegiatan ini dapat mempererat tali silaturahmi dan bermanfaat antar kedua kampus.

\#Kemenluk #KabinetSodara #BEMUEU2021