---
author: Kementerian Komunikasi dan Informatika
title: Pelantikan Menteri dan Wamen BEM UEU 20-21
date: 2020-10-17
thumbnail: "/thumbnail.JPG"

---
\[ PELANTIKAN MENTERI & WAKIL MENTERI \]

Pelantikan Menteri dan Wakil Menteri BEM Universitas Esa Unggul Periode 2020/2021.

Pelantikan ini diadakan pada:

Hari/Tanggal : Sabtu, 10 Oktober 2020  
 Tempat : Kampus Universitas Esa Unggul Kebon Jeruk

![](/gambar-1.png)

Semoga dengan kepengurusan BEM Universitas Esa Unggul pada Kabinet Sodara di periode 2020/2021 ini bisa menjadi cerminan sikap dan moral dalam menjalankan amanahnya dan menjadi teladan bagi para mahasiswa.![](/gambar-3.png)

\#KemenKominfo #KabinetSodara #BEMUEU2021