---
author: Kementerian Kajian dan Aksi Strategi
title: ":  Diskusi Online: Refleksi 1 Tahun Kinerja Jokowi-Ma'ruf Amin"
date: 2020-10-20
thumbnail: "/thumbnail.png"

---
\[PRESS RELEASE DISKUSI ONLINE KEMENTERIAN KAJIAN DAN AKSI STRATEGIS BEM UNIVERSITAS ESA UNGGUL\]

Jakarta – Dalam rangka memperingati 1 tahun kinerja Jokowi danMa’ruf Amin, Kementrian Kajian dan Aksi Strategis BEM Universitas Esa Unggul mengadakan Diskusi Online yang bertemakan “Refleksi 1 Tahun Kinerja Jokowi – Ma’ruf Amin”. Diskusi ini mengundang narasumber yang berkompeten di antaranya Tauhid Ahmad (Direktur Eksekutif Indef), Dr. Ujang Komarudin, M.Si (Direktur Eksekutif Indonesia Political Review), Halili Hasan (Direktur Riset Setara Institute) dan Rizky Al Farizie (Presiden Mahasiswa Universitas Esa Unggul). Diskusi Online yang berlangsung dari pukul 19:30 sampai 21:30 WIB.

Untuk press release bit.ly/KASTRATUEU

![](/salinan-20-10-2020-3-4.png)

\#KemenKastrat #KabinetSodara #BEMUEU2021