---
author: Kementerian Seni dan Budaya
title: 'Buletin Seni: Tari Reog Ponorogo'
date: 2020-12-30
thumbnail: "/thumbnail-buletin-seni_-tari-reog-ponorogo.jpg"

---
Reog adalah salah satu kesenian budaya yang berasal dari Jawa Timur bagian barat-laut dan Ponorogo dianggap sebagai kota asal Reog yang sebenarnya. Gerbang kota Ponorogo dihiasi oleh sosok Warok dan Gemblak, dua sosok yang ikut tampil pada saat Reog dipertunjukkan.

![](/gambar-1-buletin-seni_-tari-reog-ponorogo.jpg)

Kesenian asal Ponorogo secara resmi bercerita mengenai perang antara Kerajaan Kediri dengan Ponorogo akibat Singabarong (Raja Kediri) tidak merestui putrinya Dewi Ragil Kuning- untuk dilamar Klono Sewandono (Raja Ponorogo).

5 penari utama Tari Reog:

1. Klono Sewandono
2. Barongan
3. Jathil
4. Warok
5. Bujang Ganong

Seni pertunjukan reog Ponorogo, merupakan salah satu tradisi masyarakat Ponorogo yang yang masih hidup dan bertujuan mempererat tali silaturahmi masyarakat Ponorogo. Pementasan reog mulai muncul sejak tahun 1920 hingga saat ini.

[#Kemensenbud](https://www.instagram.com/explore/tags/kemensenbud/)[#BuletinSeni](https://www.instagram.com/explore/tags/buletinseni/)[#KabinetSodara](https://www.instagram.com/explore/tags/kabinetsodara/)[#BEMUEU2021](https://www.instagram.com/explore/tags/bemueu2021/)