---
author: Kementerian Sosial dan Lingkungan
title: Bakti Sosial Peduli Banjir Bandang Cibunian
date: 2020-10-26
thumbnail: "/234.jpg"

---
\#BEMEsaUnggulPeduli

Halo para Sodara!

Banjir bandang yang melanda Kampung Muara Satu, Desa Cibunian, Kec.Pamijahan, Bogor pada tanggal 21 September 2020 pukul 17:00 WIB, meskipun tidak ada korban jiwa namun mengakibatkan banyak bangunan yang terendam air dan mengalami kerusakan.![](/gambar-1.jpg)

Sehubungan dengan bencana tersebut para Civitas dan ORMAWA Universitas Esa Unggul melakukan Bakti Sosial pada tanggal 26 dan 27 September 2020 untuk membantu masyarakat Pondok Pesantren Talimul Qur'an yang terdampak banjir bandang. Berbagai bantuan Telah didistribusikan seperti bahan pokok makanan, pakaian muslim, selimut, Al-Qur'an, peralatan masjid, uang tunai, dan pemberian Trauma Healing untuk anak-anak Pondok Pesantren Talimul Qur'an![](/gambar-2.JPG)

\#Kemensosling #KabinetSodara #BEMUEU2021