---
author: Kementerian Dalam Kampus
title: Pelantikan Para Ketua BEM Fakultas UEU
date: 2020-10-31
thumbnail: "/890.png"

---
Jakarta - Halo para sodara!

Pelantikan Ketua dan Wakil Ketua BEM Fakultas Universitas Esa Unggul periode 2020/2021.

Selamat dan sukses kepada para Ketua dan Wakil Ketua BEMF UEU! Semoga dengan kepengurusan baru BEM di setiap Fakultas Universitas Esa Unggul pada periode 2020/2021 bisa lebih memberikan semangat dan inovasi baru tidak terpengaruh oleh keadaan dikala pandemik seperti saat ini. Diharapkan juga menjadi kepengurusan yang bisa menjadi teladan untuk seluruh mahasiswa/i, khususnya mahasiswa/i Universitas Esa Unggul.

![](/q.png)

![](/w.png)

![](/r.png)

![](/t.png)

![](/u.png)

![](/o.png)

![](/p.png)

\#Kemendak #KabinetSodara #BEMUEU2021.