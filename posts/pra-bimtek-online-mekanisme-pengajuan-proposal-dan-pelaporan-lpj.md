---
author: Kementerian Keuangan
title: PRA-BIMTEK ONLINE Mekanisme Pengajuan Proposal dan Pelaporan LPJ
date: 2020-11-07
thumbnail: "/290.png"

---
  
Telah dilaksanakan kegiatan Pra-BIMTEK pada hari Sabtu, 7 November 2020 yang dimulai pukul 08:30 s/d 11:00 WIB. Pra-BIMTEK ini dibuka dengan sambutan oleh Presiden dan Wakil Presiden Mahasiswa, kemudian dilanjutkan dengan pemaparan regulasi pengajuan proposal dan LPJ oleh Kementerian Keuangan dan Kementerian Dalam Kampus, serta sesi tanya jawab dengan organisasi mahasiswa.

Acara ini diadakan untuk menyampaikan informasi mengenai alur kegiatan yang akan diadakan berupa pengajuan proposal dan pelaporan LPJ program kerja organisasi mahasiswa fakultas dan UKM, khususnya dalam hal mekanisme PAB.

![](/a.png)

![](/s.png)

\#Kemenkeu #Kemendak #KabinetSodara #BEMUEU2021.