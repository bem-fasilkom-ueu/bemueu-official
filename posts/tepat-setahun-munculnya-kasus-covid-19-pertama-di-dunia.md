---
author: Kementerian Komunikasi dan Informatika
title: Tepat Setahun Munculnya Kasus COVID-19 Pertama di Dunia
date: 2020-11-18
thumbnail: "/543.png"

---
17 November 2019, tanggal ini bisa mengubah dunia dalam sekejap. Dimana pada tanggal itu seorang pria berumur 55 tahun di provinsi Hubei, Cina terjangkit sebuah virus yang dinamakan Coronavirus Disease 2019 (COVID-19). Sangat sulit untuk menangani virus ini bagi para medis. Tetapi mereka tetap berjuang keras demi kesembuhan masyarakat.

Selain tim medis, banyak masyarakat yang kena dampak virus ini. Ini sangat merugikan ekonomi bagi setiap perusahaan besar hingga pedagang kaki lima. Di bidang pendidikan juga terdampak sangat besar.

![](/f.png)

Kenapa di bidang pendidikan sangat besar dampaknya? Karena untuk menghentikan penyebaran virus ini pelajar/mahasiswa, dosen, guru, dan staff lainnya harus menghentikan kegiatan nya di dalam universitas maupun sekolah. Para pelajar/mahasiswa harus mengikuti kelas secara online, begitu pun dosen dan guru yang menyampaikan materi.

![](/g.png)

Ketidaksiapan semua unsur dalam pendidikan menjadi kendala yang besar, dengan adanya perubahan dalam belajar mengajarnya. Perubahan sistem luring (luar jaringan) menjadi sistem daring (dalam jaringan) membutuhkan kesiapan yang harus matang.

Sebagai seorang mahasiswa, kalian pasti sangat ingin semua kembali normal dan bisa memulai kuliah secara offline. Maka dari itu mari kita mengikuti setiap protokol kesehatam dan peraturan yang ditetapkan pemerintah kepada kita. Agar Bumi kita bisa kembali membaik dan normal kembali seperti semula.![](/h.png)

Cukup 1 tahun saja anniversary bersama Covid-19, jangan sampai ada anniversary kedua.

Semoga pandemi covid-19 ini segera berlalu ☺️

\#Kemenkominfo #covid19 #KabinetSodara #BEMUEU2021.