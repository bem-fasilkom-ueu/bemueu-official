import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian_Keuangan.png", role: "Menteri", nama: "Ni Luh Ratna Puspadewi", faculty: "Fakultas Ekonomi dan Bisni 2017", desc: "Special thing takes effort." },
    { img: "/wamen_Kementerian_Keuangan.png", role: "Wakil Menteri", nama: "Roro Mustika Wulan Suci", faculty: "Fakultas Ekonomi dan Bisnis 2017", desc: "Wherever life plants you, bloom with grace." },
]

const departmentMembers = [
    { img: "/anggota_1_Kementerian_Keuangan.png"},
    { img: "/anggota_2_Kementerian_Keuangan.png"},
    { img: "/anggota_3_Kementerian_Keuangan.png"},
]

const importantProps = [
    { 
        dept: "Kementerian Keuangan", 
        desc: "Kementerian Keuangan dibentuk untuk melayani dan menjembatani kebutuhan organisasi mahasiswa baik di tingkat fakultas, universitas, maupun Unit Kegiatan Mahasiswa (UKM) khususnya di bidang keuangan dalam menjalankan program kerja dan kegiatannya yang berkaitan dengan Tri Dharma Perguruan Tinggi. Kementerian Keuangan secara responsif akan berkomunikasi dengan seluruh organisasi mahasiswa dalam menjalankan alur pengajuan, pencairan dan pelaporan keuangan kegiatan dengan pihak universitas. Sehingga, diharapkan Kementerian Keuangan mampu memfasilitasi keuangan organisasi mahasiswa untuk menunjang kelancaran kegiatannya. Visi Kementerian Keuangan yakni menjadi lembaga Pengelola Keuangan Organisasi Mahasiswa dan UKM Universitas Esa Unggul yang Profesional, Berintegritas dan Berkeadilan sesuai asas Tri Dharma Perguruan Tinggi.",
        logo: "/logo_Kementerian_Keuangan.png",
        membersPicture: "/foto_seluruh_anggota_Kementerian_Keuangan.png"
    },
]

const tooltipProps = [
    { title: "BIMTEK (Bimbingan Teknis)", desc: "Bimbingan Teknis merupakan Bimbingan teknis pada organisasi mahasiswa tentang teknis pengajuan dan pelaporan dana kegiatan di universitas. Dengan tujuan	  supaya tercipta tertib administrasi dan kelancaran pendanaan kegiatan organisasi mahasiswa." },
    { title: "Forum dan Sidang Dana", desc: "Pertemuan dengan organisasi mahasiswa untuk berdiskusi dan menetapkan proporsi dana pagu. Tujuannya agar menghasilkan kesepakatan bersama mengenai proporsi dana pagu pada organisasi mahasiswa." },
    { title: "Kunjungan Instansi / Kementerian Terkait", desc: "Kunjungan Ke BEI (Bursa Efek Indonesia). Tujuan untuk meningkatkan pengetahuan dan minat mahasiswa dalam berinvestasi dengan mengenal instrumen investasi." },
    { title: "Workshop Investasi Online", desc: "Pelatihan mahasiswa mengenai investasi reksadana secara online sekaligus mengimplementasikan praktiknya yang akan bekerja sama dengan perusahaan/start up investasi. Tujuannya untuk meningkatkan pengetahuan dan minat mahasiswa dalam instrumen investasi sebagai bekal masa depan dan passive income." },
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemenkeu = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="kemenkeu" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemenkeu;