import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian_Komunikasi_dan_Informatika.png", role: "Menteri", nama: "Febriano Yusuf Aditiya", faculty: "Fakultas Desain dan Industri Kreatif 2018", desc: "Open-minded people do not care to be right, they care to understand. There is never a right or wrong answer, there is just different perspective. And everything is about understanding." },
    { img: "/wameni_Kementerian_Komunikasi_dan_Informatika.png", role: "Wakil Menteri", nama: "Dea Rizki Amalia", faculty: "Fakultas Teknik 2018", desc: "Do not waste your time just for sadness." },
]

const departmentMembers = [
    { img: "/anggota_1_kementeria_0fkKw.jpg"},
    { img: "/anggota_2_kementeria_VQUzh.jpg"},
    { img: "/anggota_3_kementeria_oYUw3.jpg"},
    { img: "/anggota_4_kementeria_PIdJs.jpg"},
]

const importantProps = [
    { 
        dept: "Kementerian Komunikasi dan Informatika", 
        desc: "Kementerian Komunikasi dan Informatika merupakan sebuah divisi yang berada di dalam Badan Eksekutif Mahasiswa Universitas Esa Unggul. Kementerian Komunikasi dan Informatika berfungsi sebagai pemberi informasi, yaitu mempublikasikan seluruh kegiatan atau acara BEM UEU dan juga seluruh Ormawa yang ada di UEU agar dapat terjangkau oleh seluruh masyarakat UEU terkhusus para teman mahasiswa.",
        logo: "/logo_Kementerian_Komunikasi_dan_Informatika.png",
        membersPicture: "/foto_seluruh_anggota_Kementerian_Komunikasi_dan_Informatika.png"
    },
]

const tooltipProps = [
    { title: "Mengelola Sosial Media", desc: "Digunakan untuk memberikan informasi mengenai kegiatan BEM UEU dan seluruh mahasiswa UEU yang ada di lingkup universitas maupun diluar melalui media sosial BEM UEU." },
    { title: "Membuat Website BEM UEU", desc: "Menjadikan BEM UEU lebih profesional dalam hal penyebaran informasi, serta menerapkan penggunaan teknologi informasi secara kreatif dan konstruktif." },
    { title: "Mengelola Website UEU", desc: "website BEMU digunakan sebagai alat penunjang untuk memberikan informasi mengenai berbagai kegiatan dan juga profile dari BEM UEU, serta profile dari BEMF dan UKM ." },
    { title: "Workshop dan Lomba KOMINFEST", desc: "KOMINFEST adalah acara yang terbuka untuk umum yang terdiri dari Workshop fotografi dan juga 3 lomba, yaitu lomba fotografi, lomba videografi, dan lomba desain poster."},
    { title: "Acara KOMINFAMS", desc: "KOMINFAMS merupakan sebuah acara gathering divisi Kominfo (atau sejenisnya) seluruh ormawa di UEU yang digerakkan oleh Kementerian Kominfo BEM UEU. Di acara ini terdapat seminar dengan narasumber pihak Kementerian Kominfo RI,  serta acara pemberian penghargaan." },
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemenkominfo = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="kominfo" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemenkominfo;