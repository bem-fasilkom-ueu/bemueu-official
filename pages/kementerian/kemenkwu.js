import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian_Kemandirian_dan_Kewirausahaan.png", role: "Menteri", nama: "Muhamad Ade Sutrisno", faculty: "Fakultas Teknik 2017", desc: "Ketika suara dibungkam, kritik dilarang tanpa alasan. Hanya ada satu kata yaitu LAWAN!" },
    { img: "/wamen_Kementerian_Kemandirian_dan_Kewirausahaan.png", role: "Wakil Menteri", nama: "Moh. Aldiyansyah", faculty: "Fakultas Teknik 2018", desc: "Sukses tidak perlu menunggu tua, kalau bisa sukses di usia muda, kenapa harus ditunda?" },
]

const departmentMembers = [
    { img: "/anggota_1_Kementerian_Kemandirian_dan_Kewirausahaan.png"},
    { img: "/anggota_2_Kementerian_Kemandirian_dan_Kewirausahaan.png"},
]

const importantProps = [
    { 
        dept: "Kementerian Kemandirian dan Kewirausahaan", 
        desc: "Kementerian Kemandirian dan Kewirausahaan (Kemenkwu) merupakan kemampuan kreatif dan inovatif yang dijadikan dasar, dan sumber daya untuk mencari peluang menuju sukses. Kementerian Kemandirian dan Kewirausahaan juga berfungsi sebagai sarana dan wadah untuk mengembangkan minat, bakat, serta potensi dalam bidang kewirausahaan atau sebagai wadah aspirasi mahasiswa dalam berbisnis/berwirausaha untuk mengembangkan usahanya.",
        logo: "/logo_Kementerian_Kemandirian_dan_Kewirausahaan.png",
        membersPicture: "/foto_seluruh_anggota_Kementerian_Kemandirian_dan_Kewirausahaan.png"
    },
]

const tooltipProps = [
    { title: "Menyediakan Atribut BEM UEU", desc: "Berkordinasi dengan seluruh kementrian dalam membuat Baju PDH dan keperluan lainya (Sticker, PIN, Spanduk)." },
    { title: "Workshop pelatihan tentang kewirausahaan", desc: "Seminar online berkolaborasi dengan Kementerian Luar Kampus dengan tujuan : membuat pelatihan kewirausahaan di masa pandemic, pemilihan usaha yang tepat dimasa pandemi, bagaimana cara mengembangkan bisnis yang sudah dibuat agar terlaksana dengan baik." },
    { title: "Podcast Motivasi Kesuksesan", desc: "Upload konten video (Podcast) yang berkolaborasi dengan Kemenkomiinfo, konten berupa Motivasi kesuksesan dalam membangun bisnis." },
    { title: "Mengadakan Bazaar Mahasiswa", desc: "Mengadakan Acara Bazar untuk membantu usaha/small bisnis para Mahasiswa." },
    { title: "Mengadakan Koperasi", desc: "Mengadakan Koperasi, yang bekerjasama dengan himpunan ketua masjid." },
    { title: "Membuat Sosmed Kemenkwu", desc: "Membuat media dan mengelola sosmed Kementerian Kemandirian dan Kewirausahaan." },
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemenkwu = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="kemenkwu" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemenkwu;