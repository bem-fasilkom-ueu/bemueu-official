import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian_Seni_dan_Budaya.png", role: "Menteri", nama: "Muhammad Haekal ", faculty: "Fakultas Desain dan Industri Kreatif 2017", desc: "Belum hidup jika masih memperkaya diri sendiri." },
    { img: "/wamen_Kementerian_Seni_dan_Budaya.png", role: "Wakil Menteri", nama: "Petrus Elwino Suban Wada", faculty: "Fakultas Ekonomi dan Bisnis 2017", desc: "Banyak ide-ide bagus untuk dijalankan dalam sebuah rumah, tetapi akan lebih bagus ide itu diterjemahkan bersama seisi rumah." },
]

const departmentMembers = [
    { img: "/anggota_1_Kementerian_Seni_dan_Budaya.png"},
    { img: "/anggota_2_Kementerian_Seni_dan_Budaya.png"},
    { img: "/anggota_3_Kementerian_Seni_dan_Budaya.png"},
]

const importantProps = [
    { 
        dept: "Kementerian Seni dan Budaya", 
        desc: "Kementerian Kementerian Seni dan Budaya merupakan sebuah divisi yang berada dibawah Badan Eksekutif Mahasiswa Universitas Esa Unggul. Kementrian Seni dan Budaya berfungsi sebagai sarana dan wadah untuk mengembangkan minat, bakat, serta potensi yang berkaitan dengan kesenian dan kebudayaan.",
        logo: "/logo_Kementerian_Seni_dan_Budaya.png",
        membersPicture: "/foto_seluruh_anggota_Kementerian_Seni_dan_Budaya.png"
    },
]

const tooltipProps = [
    { title: "Membuat buletin seni", desc: "Publikasi yang mengangkat seni dan budaya melalui media sosial dengan konten yang menarik." },
    { title: "Membuat acara pesta seni 21", desc: "Acara pagelaran seni yang terdiri dari perlombaan (musik, vokal : koplo & pop), tari tradisional, poster)  dan pameran virtual." },
    { title: "Membuat acara Bini BEMU (Bincang Seni BEM UEU)", desc: "Talkshow interaktif melalui ig live mengenai seni dengan mengundang penggiat seni tradisional." },
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemensenbud = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="senbud" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemensenbud;