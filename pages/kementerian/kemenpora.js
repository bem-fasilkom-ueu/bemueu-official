import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian_Pemuda_dan_Olahraga.png", role: "Menteri", nama: "Irsad Hidayattullah", faculty: "Fakultas Ilmu Komputer 2017", desc: "Kita tidak berhenti berolahraga karena menjadi renta. Kita menjadi renta karena berhenti berolahraga. Tetap bergerak agar tetap hidup." },
    { img: "/wamen_Kementerian_Pemuda_dan_Olahraga.png", role: "Wakil Menteri", nama: "Akbar Adivi Gaos", faculty: "Fakultas Teknik 2017", desc: "Latihan serta bekerja keras adalah kunci penting untuk memenangkan suatu pertandingan. Salam Vini, Vidi, Vici. Salam Olahraga!" },
]

const departmentMembers = [
    { img: "/anggota_1_Kementerian_Pemuda_dan_Olahraga.png"},
    { img: "/anggota_2_Kementerian_Pemuda_dan_Olahraga.png"},
]

const importantProps = [
    { 
        dept: "Kementerian Pemuda dan Olahraga", 
        desc: "Kementerian Pemuda dan Olahraga (Kemenpora) adalah kementerian dalam Badan Eksekutif Mahasiswa Universitas Esa Unggul yang membidangi urusan dan menaungi UKM Olahraga, serta hal hal yang berkaitan dengan scene kompetitif baik dari internal maupun eksternal. Kementerian Pemuda dan Olahraga dipimpin oleh seorang Menteri Pemuda dan Olahraga (Menpora).",
        logo: "/logo_Kementerian_Pemuda_dan_Olahraga.png",
        membersPicture: "/foto_seluruh_anggota_Kementerian_Pemuda_dan_Olahraga.png"
    },
]

const tooltipProps = [
    { title: "Esport Cycle. Deskripsi", desc: "Membuat siklus kompetitif untuk menghidupkan dan mendata sdm yang berkaitan dengan perlombaan baik E-sport maupun Sport." },
    { title: "Olahraga Bareng", desc: "Olahraga bersama 1x/bulan untuk internal (Futsal, Badminton, Jogging, dan Berenang). Untuk menjaga tali silaturahim antar kabinet." },
    { title: "Perlombaan E-sport jenjang SMA sederajat", desc: "Ajang kompetisi E-sport game mobile baik itu moba ataupun battle royale untuk jenjang SMA sederajat." },
    { title: "Perlombaan E-sport internal Universitas Esa Unggul", desc: "Ajang kompetisi E-sport game mobile baik itu moba ataupun battle royale untuk kawan kawan mahasiswa Universitas Esa Unggul." },
    { title: "Perlombaan Futsal internal Universitas Esa Unggul", desc: "Futsal internal Universitas Esa Unggul Ajang kompetisi futsal yang dibuat untuk mahasiswa Universitas Esa Unggul, dan memperkuat ikatan sosial antar fakultas." },
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemenpora = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="kemenpora" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemenpora;