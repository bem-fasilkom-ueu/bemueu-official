import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian_Kajian_dan_Aksi_Strategi.png", role: "Menteri", nama: "Muh Faiz Azhar", faculty: "Fakultas Ilmu Komputer 2017", desc: "Sederhana adalah Koentji, karena membuat hal sederhana menjadi rumit itu biasa , tetapi membuat sesuatu yang rumit menjadi sederhana itulah kreativitas." },
    { img: "/wamen_Kementerian_Kajian_dan_Aksi_Strategi.png", role: "Wakil Menteri", nama: "Fajar Swantantra", faculty: "Fakultas Ekonomi dan Bisnis 2017", desc: "Pengetahuan adalah keheningan, alam adalah guru dalam 'diam' menyaksikan semuanya terus tumbuh, mati dan berganti." },
]

const departmentMembers = [
    { img: "/anggota_1_Kementerian_Kajian_dan_Aksi_Strategi.png"},
    { img: "/anggota_2_Kementerian_Kajian_dan_Aksi_Strategi.png"},
]

const importantProps = [
    { 
        dept: "Kementerian Kajian dan Aksi Strategi", 
        desc: "Kementerian Kajian dan Aksi Strategis (Kastrat) adalah Kementrian yang concern terhadap permasalahan lingkungan kampus, masyarakat dan bangsa pada umumnya, melakukan kajian dan penyikapan terhadap kebijakan-kebijakan kampus dan publik yang akan, sedang, atau telah ditetapkan, menjalin hubungan baik dengan elemen-elemen masyarakat dan lembaga-lembaga kemahasiswaan di lingkungan Esa Unggul. Kementerian yang hadir dengan tujuan untuk mengembangkan daya kritis, daya nalar, dan kepedulian mahasiswa terhadap isu-isu nasional maupun internasional.",
        logo: "/logo_Kementerian_Kajian_dan_Aksi_Strategi.png",
        membersPicture: "/foto_seluruh_anggota_Kementerian_Kajian_dan_Aksi_Strategi.png"
    },
]

const tooltipProps = [
    { title: "Forum Diskusi Kastrat", desc: "Forum diskusi antara mahasiswa esa unggul dan tidak menutup kemungkinan mahasiswa kampus lain untuk hadir. Forum ini mengangkat isu-isu hangat dan terbaru untuk menjaring aspirasi mahasiswa mengenai isu tersebut dengan mengundang pembicara profesional sebagai pemantik diskusi." },
    { title: "Sekolah Kastrat", desc: "Sekolah Kastrat diharapkan memberikan wawasan kepada seluruh mahasiswa Universitas Esa Unggul khususnya pengurus Ormawa mengenai Kajian, Strategi, Taktik dll. Mengingat banyaknya isu-isu Strategis mengenai problematika yang dihadapi oleh mahasiswa kedepannya." },
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemenkastrat = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="kastrat" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemenkastrat;