import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian_Sosial_dan_Lingkungan.png", role: "Menteri", nama: "Dwi Karlina Rahmayani", faculty: "Fakultas Psikologi 2017", desc: "From the little star to start to be a big star." },
    { img: "/wamen_Kementerian_Sosial_dan_Lingkungan.png", role: "Wakil Menteri", nama: "Dimas Bagus Dwi Prasetyo", faculty: "Fakultas Fisioterapi 2017", desc: "Better to have knowledge, Instead of unlimited attitude." },
]

const departmentMembers = [
    { img: "/anggota_1_Kementerian_Sosial_dan_Lingkungan.png"},
    { img: "/anggota_2_Kementerian_Sosial_dan_Lingkungan.png"},
    { img: "/anggota_3_Kementerian_Sosial_dan_Lingkungan.png"},
]

const importantProps = [
    { 
        dept: "Kementerian Sosial dan Lingkungan", 
        desc: "Kementerian Sosial dan Lingkungan merupakan sebuah divisi yang berada dibawah Badan Eksekutif Mahasiswa Universitas Esa Unggul. Kementerian Sosial dan Lingkungan berfungsi menyelenggarakan dan membidangi urusan dalam lingkup sosial dan lingkungan.",
        logo: "/logo_Kementerian_Sosial_dan_Lingkungan.png",
        membersPicture: "/foto_seluruh_anggota_Kementerian_Sosial_dan_Lingkungan.png"
    },
]

const tooltipProps = [
    { title: "Majalah Sosial", desc: "Sharing cerita inspiratif di sosial media yang berkaitan dengan sosial dan lingkungan (Ig Bemu)." },
    { title: "Bakti Sosial", desc: "Bakti sosial untuk mahasiswa Esa Unggul perantau yg ngekos di sekitar kampus, karyawan kampus, dan masyarakat sekitar kampus." },
    { title: "Sosling Goes to Masyarakat (Razia Perut Laper)", desc: "Membagikan makanan kepada masyarakat yang membutuhkan di bulan ramadhan." },
    { title: "Sharing Session 'Hari Kesetiakawanan Nasional'", desc: "Membuat diskusi online bertemakan hari kesetiakawan." },
    { title: "Webinar 'Penanaman Hidroponik'", desc: "Mengadakan webinar tentang penanaman hidroponik." },
    { title: "Webinar 'Kesehatan Mental'", desc: "Mengadakan webinar yang berkaitan dengan kesehatan mental." },
    { title: "Membuat Pojok Bunga", desc: "Membuat sebuah pojok bunga di satu titik kampus." },
    { title: "Peduli Anak Negeri", desc: "Membantu anak-anak yang tinggal dibawah jembatan yang dilakukan sebulan sekali." },
    { title: "Menerapkan Pemakaian Non-Plastic di Area Kampus", desc: "Bekerja sama dengan kantin untuk mengurangi penggunaan plastic dilingkungan kampus." },
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemensosling = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="sosling" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemensosling;