import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian_Pendidikan_dan_SDM.png", role: "Menteri", nama: "Andes Wiranata", faculty: "Fakultas Teknik 2017", desc: "Panta Rhei Kai Uden Menei (semuanya mengalir dan tidak ada sesuatu pun yang tinggal tetap)." },
    { img: "/wamen_Kementerian_Pendidikan_dan_SDM.png", role: "Wakil Menteri", nama: "Inggrid Devi Putri Arum Sari", faculty: "Fakultas Keguruan dan Ilmu Pendidikan 2017", desc: "Sebuah berlian tidak akan dapat dipoles tanpa gesekan, demikian juga seseorang tidak akan menjadi sukses tanda tantangan." },
]

const departmentMembers = [
    { img: "/anggota_1_Kementerian_Pendidikan_dan_SDM.png"},
    { img: "/anggota_2_Kementerian_Pendidikan_dan_SDM.png"},
]

const importantProps = [
    { 
        dept: "Kementerian Pendidikan dan SDM", 
        desc: "Kementerian Pendidikan dan SDM berfungsi sebagai wadah, konseptor dan fasilitator pengembangan sumber daya manusia dalam hal ini mahasiswa Universitas Esa Unggul. Berupaya mengintegrasikan berbagai potensi yang dimiliki oleh mahasiswa dalam bidang pendidikan untuk mengembangkan minat, bakat dan potensinya.",
        logo: "/logo_Kementerian_Pendidikan_dan_SDM.png",
        membersPicture: "/foto_seluruh_anggota_Kementerian_Pendidikan_dan_SDM.png"
    },
]

const tooltipProps = [
    { title: "LKMU XVIII", desc: "LKMU XVIII merupakan Kegiatan Latihan Kepemimpinan Mahasiswa Universitas Esa Unggul yang diadakan setiap tahun guna untuk  menjunjung tinggi Tri Darma Perguruan Tinggi. Meningkatkan dan  mengembangkan pengetahuan,sikap, rasa percaya diri pada mahasiswa. Meningkatkan kualitas diri tentang kepemimpinan ( baik diri sendiri,dalam organisasi )." },
    { title: "PKM Center", desc: "PKM Center merupakan suatu program dalam bidang kepenulisan yang terdiri dari beberapa rangkaian , yakni Workshop PKM untuk umum dan Pembinaan mahasiswa yang mengikuti PKM." },
    { title: "Esa Unggul Mengajar", desc: "Esa unggul mengajar nantinya akan menjadi wadah mahasiswa Universitas Esa Unggul agar bisa melakukan pengabdian masyarakat dalam bidang pendidikan dengan cara mengajar dan kedepannya esa unggul mengajar ini akan mencoba mengekspansi ke daerah-daerah lain yang dunia pendidikannya masih kurang. Esa unggul mengajar nantinya akan menjadi wadah mahasiswa Universitas Esa Unggul agar bisa melakukan pengabdian masyarakat dalam bidang pendidikan dengan cara mengajar dan kedepannya esa unggul mengajar ini akan mencoba mengekspansi ke daerah-daerah lain yang dunia pendidikannya masih kurang." },
    { title: "Webinar/Kajan Online", desc: "Program kajian yang membahas isu pendidikan melalui Zoom/Google meet." },
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemenpsdm = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="kemenpsdm" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemenpsdm;