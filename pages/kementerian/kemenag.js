import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian_Agama.png", role: "Menteri", nama: "Rizki Akbaraziz Dwiyanto", faculty: "Fakultas Ilmu Komputer 2017", desc: "Menjadi bahagia adalah pilihan mu sendiri, maka dari itu jangan pernah berhenti bersyukur, berdo'a dan berusahaa semaksimal mungkin, lampaui batasan mu.." },
    { img: "/wamen_Kementerian_Agama.png", role: "Wakil Menteri", nama: "Yan Kify Mohi", faculty: "Fakultas Ilmu-Ilmu Kesehatan 2018", desc: "Dibutuhkan banyak keberanian untuk melepaskan yang akrab dan tampaknya aman untuk merangkul yang baru & tidak dikenal." },
]

const departmentMembers = [
    { img: "/anggota_1_Kementerian_Agama.png"},
    { img: "/anggota_2_Kementerian_Agama.png"},
    { img: "/anggota_3_Kementerian_Agama.png"},
]

const importantProps = [
    { 
        dept: "Kementerian Agama", 
        desc: "Kementerian agama adalah kementerian yang ada di badan eksekutif mahasiswa universitas esa unggul. Kementerian agama ini bergerak dalam ranah keagamaan. Kementerian agama ini memiliki tujuan untuk menumbuhkan nilai toleransi antar agama serta mengembangkan nilai-nilai agama yang ada di dalam lingkungan kampus. Kementerian ini menaungi ukm keagamaan di Universitas Esa Unggul.",
        logo: "/logo_Kementerian_Agama.png",
        membersPicture: "/foto_seluruh_anggota_Kementerian_Agama.png"
    },
]

const tooltipProps = [
    { title: "Buka Bersama dan Santunan Anak Yatim", desc: "Suatu kegiatan tahunan yang dilakukan untuk meningkatkan ikatan ukhuwah dan islamyah dan juga berlomba lomba dalam kebaikan." },
    { title: "Bakti Sosial Lintas Agama", desc: "Suatu kegiatan antar umat beragama di Republik Indonesia khusus nya Univ. Esa Unggul agar saling bertoleransi dan saling menghargai melalui bantuan sosial kepada masyarakat." },
    { title: "Jumat Berkah", desc: "Suatu kegiatan berbagi makanan atau minuman di area Masjid Baitul Gafur Esa Unggul." },
    { title: "Kajian Keagamaan", desc: "Suatu kegiatan mengkaji nilai-nilai pancasila dalam perspektif dari masing-masing agama."},
    { title: "ISYEF (Islamic Youth Economic Foundation)", desc: "Suatu Kegiatan yang dilaksanakan dalam kampus untuk membuka peluang usaha mahasiswa Universitas Esa Unggul."},
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemenag = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="kemenag" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemenag;