import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian_Luar_Kampus.png", role: "Menteri", nama: "Ahmad Fariyan", faculty: "Faculty info to be updated...", desc: "yesterday is history, tomorrow is a mystery. But today is a gift, what’s it’s called the present." },
    { img: "/wamen_Kementerian_Luar_Kampus.png", role: "Wakil Menteri", nama: "Amelia Rahmawati", faculty: "Faculty info to be updated...", desc: "Jangan merasa menyesal atas keputusan yang sudah dipilih, jadikan itu pelajaran untuk keputusan selanjutnya." },
]

const departmentMembers = [
    { img: "/anggota_1_Kementerian_Luar_Kampus.png"},
    { img: "/anggota_2_Kementerian_Luar_Kampus.png"},
    { img: "/anggota_3_Kementerian_Luar_Kampus.png"},
]

const importantProps = [
    { 
        dept: "Kementerian Luar Kampus", 
        desc: "Kementrian Luar Kampus merupakan sebuah divisi yang berada dibawah Badan Eksekutif Mahasiswa Universitas Esa Unggul. Kementrian ini membangun hubungan dan menjalin kerjasama dari internal dan eksternal kampus.",
        logo: "/logo_Kementerian_Luar_Kampus.png",
        membersPicture: "/foto_seluruh_anggota_Kementerian_Luar_Kampus.png"
    },
]

const tooltipProps = [
    { title: "Pengabdian Masyarakat", desc: "Pengabdian masyarakat ini merupakan serangkaian acara bakti sosial  yang akan di laksanakan di dekat kampus Universitas Esa Unggul dan di selenggarakan oleh Badan Eksekutif Mahasiswa Universitas Esa Unggul. Didalam kegitan ini akan ada edukasi dan penyuluhan tentang pentingnya untuk cek kesehatan seperti gula darah, cek tensi, kolesterol dll. Selain itu kami mengharapkan masyarakat sekitar yang akan di berikan penyuluhan dan edukasi dapat tercipta kondisi baik dan sehat." },
    { title: "Kunjungan Kementrian", desc: "Kunjungan Kementerian dilakukan agar dapat menjalin hubungan kerjasama yang baik dengan pemerintah sehingga dirasa dapat membantu pergerakan mahasiswa dalam melakukan kerjasama program ataupun kerjasama gerakan." },
    { title: "Kongres BEM SI", desc: "Pertemuan para aliansi universitas yang mempunyai kepentingan besar yang menyangkut organisasi atau pihak pihak yang memiliki kepentingan untuk mendiskusikan masalah dan mengambil keputusan." },
    { title: "Konsolidasi Kampus Cabang", desc: "Menyatukan persepsi dan pendapat dengan kampus cabang agar keakraban selalu terjaga."},
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemenluk = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="kemenluk" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemenluk;