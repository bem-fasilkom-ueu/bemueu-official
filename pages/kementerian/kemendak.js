import { getSortedPostsData } from '../../lib/posts'
import AppKementerian from "../../components/AppKementerian";

const mostValuableMember = [
    { img: "/menteri_Kementerian Dalam Kampus.png", role: "Menteri", nama: "Saiful Anwar Husein", faculty: "Fakultas Ekonomi dan Bisnis 2017", desc: "Self Quote Info To Be Updated..." },
    { img: "/wamen_Kementerian Dalam Kampus.png", role: "Wakil Menteri", nama: "Muhamad Farhan Lazuardi", faculty: "Fakultas Ekonomi dan Bisnis 2017", desc: "Self Quote Info To Be Updated..." },
]

const departmentMembers = [
    { img: "/anggota_1_Kementerian Dalam Kampus-01.png"},
    { img: "/anggota_2_Kementerian Dalam Kampus-01.png"},
    { img: "/anggota_3_Kementerian Dalam Kampus-01.png"},
]

const importantProps = [
    { 
        dept: "Kementerian Dalam Kampus", 
        desc: "Kementerian Dalam Kampus adalah kementerian yang mengawasi urusan dalam kampus dan menaungi ORMAWA-ORMAWA serta bersinergi civitas akademika di UEU.",
        logo: "/logo_Kementerian Dalam Kampus-01.png",
        membersPicture: "/foto seluruh anggota_Kementerian Dalam Kampus.png"
    },
]

const tooltipProps = [
    { title: "Symposium", desc: "Simposium adalah kegiatan pertemuan yang di hadiri oleh civitas akademika Universitas Esa Unggul termasuk Rektor ataupun Wakil rector dan seluruh Organisasi Mahasiswa dari semua Fakultas. Adapun salah satu kegiatan yang nantinya ada dalam acara tersebut adalah penghargaan bagi beberapa Organisasi Mahasiswa yang memiliki kinerja yang baik." },
    { title: "Diskusi Triwulan", desc: "Diskusi Triwulan merupakan kegiatan diskusi terbuka bersama Ormawa dan pejabat kampus yang di laksanankan setiap tiga bulan sekali." },
]

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData
        }
    }
}

const kemendak = ({allPostsData}) => {
    return (
        <>
            <AppKementerian customString="kemendak" blockCaption={importantProps[0].dept} blockSubCaption="BEM UEU 2020/2021" mostValuableMember={mostValuableMember} membersGallery={departmentMembers} membersPicture={importantProps[0].membersPicture} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} allPostsData={allPostsData} tooltipProps={tooltipProps} departmentFilter={importantProps[0].dept} />
        </>
    )
}
 
export default kemendak;