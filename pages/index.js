import AppNavbar from '../components/AppNavbar'
import { getSortedPostsData } from '../lib/posts'
import Link from 'next/link'
import { useState } from 'react'
import Image from 'next/image'
import dynamic from 'next/dynamic'

const AppHeader = dynamic(() => import('../components/AppHeader'))
const AppCarousel = dynamic(() => import('../components/AppCarousel'))
const AppSVGMountain = dynamic(() => import('../components/AppSVGMountain'))
const AppAnimatedBackgroundStd = dynamic(() => import('../components/AppAnimatedBackgroundStd'))
const AppInstagram = dynamic(() => import('../components/AppInstagram'))

const kementerianArray = [
    { img: "/logo_kementerian_dal_HCkFT.jpg", slug: "kemendak", title: "Kementerian Dalam Kampus", desc: "Kementerian Dalam Kampus adalah kementerian yang mengawasi urusan dalam kampus dan menaungi ORMAWA-ORMAWA serta bersinergi civitas akademika di UEU." },
    { img: "/logo_kementerian_kom_Z5daW.jpg", slug: "kemenkominfo", title: "Kementerian Komunikasi dan Informatika", desc: "Kementerian Komunikasi dan Informatika berfungsi sebagai pemberi informasi, yaitu mempublikasikan seluruh kegiatan atau acara BEM UEU dan juga seluruh Ormawa yang ada di UEU agar dapat terjangkau oleh seluruh masyarakat UEU terkhusus para teman mahasiswa." },
    { img: "/logo_kementerian_lua_OU2Wv.jpg", slug: "kemenluk", title: "Kementerian Luar Kampus", desc: "Kementerian Luar Kampus merupakan sebuah divisi yang berada dibawah Badan Eksekutif Mahasiswa Universitas Esa Unggul. Kementrian ini membangun hubungan dan menjalin kerjasama dari internal dan eksternal kampus." },
    { img: "/logo_kementerian_aga_UikLx.jpg", slug: "kemenag", title: "Kementerian Agama", desc: "Kementerian agama ini bergerak dalam ranah keagamaan. Kementerian agama ini memiliki tujuan untuk menumbuhkan nilai toleransi antar agama serta mengembangkan nilai-nilai agama yang ada di dalam lingkungan kampus. Kementerian ini menaungi ukm keagamaan di Universitas Esa Unggul." },
    { img: "/logo_kementerian_kaj_bMmz6.jpg", slug: "kemenkastrat", title: "Kementerian Kajian dan Aksi Strategi", desc: "Kementerian Kajian dan Aksi Strategis (Kastrat) adalah Kementrian yang concern terhadap permasalahan lingkungan kampus, masyarakat dan bangsa pada umumnya, melakukan kajian dan penyikapan terhadap kebijakan-kebijakan kampus dan publik yang akan, sedang, atau telah ditetapkan, menjalin hubungan baik dengan elemen-elemen masyarakat dan lembaga-lembaga kemahasiswaan di lingkungan Esa Unggul." },
    { img: "/logo_kementerian_keu_2mqwP.jpg", slug: "kemenkeu", title: "Kementerian Keuangan", desc: "Kementerian Keuangan dibentuk untuk melayani dan menjembatani kebutuhan organisasi mahasiswa baik di tingkat fakultas, universitas, maupun Unit Kegiatan Mahasiswa (UKM) khususnya di bidang keuangan dalam menjalankan program kerja dan kegiatannya yang berkaitan dengan Tri Dharma Perguruan Tinggi." },
    { img: "/logo_kementerian_sos_E1hVl.jpg", slug: "kemensosling", title: "Kementerian Sosial dan Lingkungan", desc: "Kementerian Sosial dan Lingkungan merupakan sebuah divisi yang berada dibawah Badan Eksekutif Mahasiswa Universitas Esa Unggul. Kementerian Sosial dan Lingkungan berfungsi menyelenggarakan dan membidangi urusan dalam lingkup sosial dan lingkungan." },
    { img: "/logo_kementerian_sen_uf395.jpg", slug: "kemensenbud", title: "Kementerian Seni dan Budaya", desc: "Kementerian Seni dan Budaya merupakan sebuah divisi yang berada dibawah Badan Eksekutif Mahasiswa Universitas Esa Unggul. Kementrian Seni dan Budaya berfungsi sebagai sarana dan wadah untuk mengembangkan minat, bakat, serta potensi yang berkaitan dengan kesenian dan kebudayaan." },
    { img: "/logo_kementerian_pen_EzxOW.jpg", slug: "kemenpsdm", title: "Kementerian Pendidikan dan SDM", desc: "Kementerian Pendidikan dan SDM berfungsi sebagai wadah, konseptor dan fasilitastor pengembangan sumber daya manusia dalam hal ini mahasiswa Universitas Esa Unggul. Berupaya mengintegrasikan berbagai potensi yang dimiliki oleh mahasiswa dalam bidang pendidikan untuk mengembangkan minat, bakat dan potensinya." },
    { img: "/logo_kementerian_pem_puXAX.jpg", slug: "kemenpora", title: "Kementerian Pemuda dan Olahraga", desc: "Kementerian Pemuda dan Olahraga (Kemenpora) adalah kementerian dalam Badan Eksekutif Mahasiswa Universitas Esa Unggul yang membidangi urusan dan menaungi UKM Olahraga, serta hal hal yang berkaitan dengan scene kompetitif baik dari internal maupun eksternal. Kementerian Pemuda dan Olahraga dipimpin oleh seorang Menteri Pemuda dan Olahraga (Menpora)." },
    { img: "/logo_kementerian_kem_BjR1N.jpg", slug: "kemenkwu", title: "Kementerian Kemandirian dan Kewirausahaan", desc: "Kementerian Kemandirian dan Kewirausahaan (Kemenkeu) merupakan kemampuan kreatif dan inovatif yang dijadikan dasar, dan sumber daya untuk mencari peluang menuju sukses." },
]

const pengurusIntiArray = [
    { img: "/pengurus_inti_presma.png", nama: "Rizky Al Farizie", role: "Presiden Mahasiswa", faculty: "Fakultas Ekonomi dan Bisnis 2017", desc: "Pimpinan utama yang bertanggung jawab terhadap kepengurusan BEM UEU (Pasal 38 Kepengurusan BEM UEU ayat 3 ART KBM UEU) dalam menjalankan proses kepemimpinan berupa penetapan kebijakan dan mengkoordinasikan kabinet kerja dalam menjalankan tugas pokok dan fungsi BEM UEU.", swap: true },
    { img: "/pengurus_inti_wakil presma.png", nama: "Zatmiko Setiawan", role: "Wakil Presiden Mahasiswa", faculty: "Fakultas Teknik 2017", desc: "Membantu tugas Presiden Mahasiswa dengan berkoordinasi kepada Presiden Mahasiswa.", swap: false },
    { img: "/pengurus_inti_sekretaris umum.png", nama: "Shintya Astriani", role: "Sekertaris Umum", faculty: "Fakultas Ekonomi dan Bisnis 2017", desc: "Membantu Presiden Mahasiswa dalam mengkoordinasikan kebutuhan dan keperluan administrasi birokrasi BEM UEU.", swap: false },
    { img: "/pengurus_inti_bendahara umum.png", nama: "Wiwik Widiawati", role: "Bendahara Umum", faculty: "Fakultas Ekonomi dan Bisnis 2017", desc: "Membantu Presiden Mahasiswa dalam mengkoordinasikan progres keuangan internal BEM UEU berupa pencatatan dan pelaporan penggalian sumber dana, penggunaan sumber dana, serta melakukan evaluasi keuangan internal BEM UEU.", swap: true },
]

const miscArray = [
    { groupPicture: "/pengurus_inti_button pop up.png" },
]

export async function queryMedia(params, token) {
    const req = await fetch(`https://graph.instagram.com/${params}?fields=id,media_type,media_url,permalink,username,timestamp,thumbnail_url&access_token=${token}`)

    return req.json()
}

export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    const tokenRequest = await fetch(`https://ig.instant-tokens.com/users/${process.env.NEXT_PUBLIC_INSTANTTOKEN_USER_ID}/instagram/${process.env.NEXT_PUBLIC_INSTAGRAM_USER_ID}/token?userSecret=${process.env.NEXT_PUBLIC_INSTANTTOKEN_SECRET}`)
    const currentToken = await tokenRequest.json()
    const mediaIDRequest = await fetch(`https://graph.instagram.com/me/media?fields=id&limit=18&access_token=${currentToken.Token}`)
    const mediaIDResult = await mediaIDRequest.json()
    const responseLength = Object.keys(mediaIDResult.data).length
    const mediaResultArray = []

    for (let i in mediaIDResult.data) {
        const query = await queryMedia(mediaIDResult.data[i].id, currentToken.Token)
        mediaResultArray.push(query)
    }
    
    const rawString = JSON.stringify(Object.assign({}, mediaResultArray))
    const instagramMedia = JSON.parse(rawString)

    return {
        props: {
            allPostsData,
            instagramMedia,
        }
    }
}

const IndexPage = ({allPostsData, instagramMedia}) => {
    const processedMedia = Object.keys(instagramMedia).map(data => data = instagramMedia[data])
    const [showMoreInstagramPost, setShowMoreInstagramPost] = useState(false)

    return (
        <div>
            <AppNavbar currentActive="beranda" />
            <AppHeader />
            <div className="container mx-auto mt-10">
                <div className="text-center">
                    <h1 className="text-3xl xl:text-5xl green-base-color font-black uppercase">Tentang BEM UEU</h1>
                    <div className="flex justify-center my-5">
                        <div className="md:flex bg-white p-5 hover:shadow-xl">
                            <img src="/tentang_bem_ueu_logo_WIg0I.jpg" width="200px" height="200px" className="mx-auto md:mx-3"></img>
                            <img src="/header_logo kabinet.png" width="200px" height="200px" className="mx-auto md:mx-3"></img>
                        </div>
                    </div>
                    <p>Badan Eksekutif Mahasiswa Universitas Esa Unggul adalah lembaga tertinggi pemegang kekuasaan dalam <br/> Keluarga Besar Mahasiswa (KBM) Universitas Esa Unggul (Pasal 11 ayat 3 AD KBM UEU)</p>
                </div>
                <div className="flex-1 md:flex flex-row mt-10">
                    <div className="flex-1 flex-col">
                        <div className="text-center text-4xl xl:text-7xl font-black">
                            <span className="mx-1 text-transparent customString-stroke-black">VISI</span>
                            <span className="mx-1 green-base-color">VISI</span>
                            <span className="mx-1 text-transparent customString-stroke-black">VISI</span>
                        </div>
                        <div className="px-6 mt-5 text-center">
                            <div className="p-2 rounded-3xl header-misi tippy-customContent my-3 hover:shadow-xl">Menggerakkan BEM UEU sebagai ormawa tingkat universitas yang kolaboratif, responsif, dan aktif dengan nilai solidaritas dan kreativitas yang berperan terhadap seluruh Mahasiswa Universitas Esa Unggul dalam upaya terwujudnya Tri Dharma Perguruan tinggi</div>
                        </div>
                    </div>
                    <div className="flex-1 flex-col mt-5 md:mt-0">
                        <div className="text-center text-4xl xl:text-7xl font-black">
                            <span className="mx-1 text-transparent customString-stroke-black">MISI</span>
                            <span className="mx-1 green-base-color">MISI</span>
                            <span className="mx-1 text-transparent customString-stroke-black">MISI</span>
                        </div>
                        <div className="px-6 mt-5 text-center">
                            <div className="p-2 rounded-3xl header-misi tippy-customContent my-3 hover:shadow-xl">Merangkul tiap kalangan mahasiswa dengan berbagai latar belakang sebagai bentuk kolaborasi yang terintegritas dalam solidaritas</div>
                            <div className="p-2 rounded-3xl header-misi tippy-customContent my-3 hover:shadow-xl">Membangun keaktifan dengan responsibilitas, peka terhadap situasi dan kondisi terkini baik terkait isu internal maupun eksternal</div>
                            <div className="p-2 rounded-3xl header-misi tippy-customContent my-3 hover:shadow-xl">Meningkatkan kreativitas BEM UEU yang merupakan bentuk penyesuaian dan pengembangan organisasi dalam berbagai kondisi</div>
                        </div>
                    </div>
                </div>
                <div className="flex-1 md:flex flex-row mt-10 green-base-color">
                    <div className="flex-1 flex-col text-center">
                        <div className="text-4xl xl:text-7xl font-bold">19</div>
                        <div className="">Staff</div>
                    </div>
                    <div className="flex-1 flex-col text-center mt-5 md:mt-0">
                        <div className="text-4xl xl:text-7xl font-bold">37</div>
                        <div className="">Badan Pengurus Harian</div>
                    </div>
                    <div className="flex-1 flex-col text-center mt-5 md:mt-0">
                        <div className="text-4xl xl:text-7xl font-bold">11</div>
                        <div className="">Kementerian</div>
                    </div>
                    <div className="flex-1 flex-col text-center mt-5 md:mt-0">
                        <div className="text-4xl xl:text-7xl font-bold">54</div>
                        <div className="">Proker</div>
                    </div>
                </div>
            </div>
            <hr className="my-5" />
            <AppCarousel sourceArray={allPostsData} />
            <AppAnimatedBackgroundStd noText={false} sourceArray={pengurusIntiArray} blockCaption="pengurus inti" customString="KABINET SODARA" miscPicture={miscArray[0].groupPicture} />
            <div className="green-base-background relative">
                <div className="container mx-auto py-10">
                    <h1 className="uppercase text-xl xl:text-5xl text-white font-black text-center mb-5">kementerian</h1>
                    <div className="grid grid-col grid-cols-1 md:grid-cols-3">
                        {
                            kementerianArray.map(item => (
                                <div key={item.img} className="text-center p-6 bg-white rounded-xl m-5 hover:shadow-xl">
                                    <Link href={`/kementerian/${item.slug}`}>
                                        <a>
                                            <Image 
                                                src={item.img}
                                                alt={item.img}
                                                height={180}
                                                width={180}
                                                layout="intrinsic"
                                                objectFit="fill"
                                            />
                                        </a>
                                    </Link>
                                    <h1 className="font-medium text-lg my-3">
                                        <strong>{item.title}</strong>
                                    </h1>
                                    <p>{item.desc}</p>
                                </div>
                            ))
                        }
                    </div>
                </div>
                <AppSVGMountain customColor="#f0f0f0" customClass="gray-base-background" />
                <div className="gray-base-background">
                    <div className="container mx-auto py-5 AppInstagram-parent">
                        <h1 className="text-3xl xl:text-5xl green-base-color font-black uppercase text-center my-5">Update Sosial Media Kami</h1>
                        <div className="grid grid-col grid-cols-1 xl:grid-cols-3">
                            {
                                processedMedia.slice(0,9).map(item => (
                                    <AppInstagram key={item.id} postLink={item.permalink} postThumbnail={item.media_url} postTimestamp={item.timestamp} postMediaType={item.media_type} postVideoThumbnail={item.thumbnail_url} />
                                ))
                            }
                        </div>
                        <div className={(showMoreInstagramPost ? "hidden" : "flex") + " my-5"}>
                            <button className="bg-black text-white rounded-lg py-1 px-3 mx-auto hover:shadow-xl" onClick={() => setShowMoreInstagramPost(true)}>Lihat Lebih Banyak...</button>
                        </div>
                        <div className={(showMoreInstagramPost ? "grid" : "hidden") + " grid-col grid-cols-1 md:grid-cols-3"}>
                            {
                                processedMedia.slice(9, processedMedia.length).map(item => (
                                    <AppInstagram key={item.id} postLink={item.permalink} postThumbnail={item.media_url} postTimestamp={item.timestamp} postMediaType={item.media_type} postVideoThumbnail={item.thumbnail_url} />
                                ))
                            }
                        </div>
                        <div className="flex my-5 justify-center">
                            <a href="https://www.instagram.com/bemueu_official" rel="noreferer noopener" target="_blank">
                                <button className="green-base-background text-white rounded-lg py-1 px-3 text-lg lg:text-xl text-3xl hover:shadow-xl">Ikuti Instagram Kami @bemueu_official</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
 
export default IndexPage;