import { getAllPostIds, getPostData } from '../../lib/posts'
import Head from 'next/head'
import Date from '../../components/AppDate'
import AppNavbar from '../../components/AppNavbar'
import AppComment from '../../components/AppComment'

export async function getStaticPaths() {
    const paths = getAllPostIds()
    return {
        paths,
        fallback: false
    }
}

export async function getStaticProps({ params }) {
    const postData = await getPostData(params.id)
    return {
        props: {
            postData
        }
    }
}

const Post = ({postData}) => {
    return (
        <>
            <Head>
                <title>{`${postData.title} - BEMUEU Official`}</title>
            </Head>
            <AppNavbar currentActive="press release" /> 
            <div className="container mx-auto py-5 w-10/12 lg:w-3/6 pt-24">
                <div className="mt-6 prose prose-sm sm:prose lg:prose-lg xl:prose-xl mx-auto">
                    <h3>{postData.title}</h3> 
                    <span>By <strong>{postData.author}</strong></span>
                    <img className="my-0" src={postData.thumbnail}></img>
                    <Date dateString={postData.date} />
                    <div className="mt-6" dangerouslySetInnerHTML={{__html: postData.contentHtml}} />
                </div>
                <AppComment />
            </div>
        </>
    )
}
 
export default Post;