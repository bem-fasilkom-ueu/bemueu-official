import AppAnimatedBackgroundBlank from "../../components/AppAnimatedBackgroundBlank";
import AppNavbar  from "../../components/AppNavbar";
import Head from "next/head";

const kementerianArray = [
    { id: 1, img: "/logo_bem_fdik.jpeg", title: "Fakultas Desain dan Industri Kreatif", link: "/bem-fakultas/FDIK", desc: "Kabinet Inovatif" },
    { id: 2, img: "/Logo BEM FEB.png", title: "Fakultas Ekonomi dan Bisnis", link: "/bem-fakultas/FEB", desc: "Kabinet KORELASI (KOntributif, kREatif, koLAborasi, SInergi)" },
    { id: 3, img: "/logo bem fikom.jpg", title: "Fakultas Ilmu Komunikasi", link: "/bem-fakultas/FIKOM", desc: "Kabinet 2020/2021" },
    { id: 4, img: "/logo bem ft.jpeg", title: "Fakultas Teknik", link: "/bem-fakultas/Teknik", desc: "KABINET BERAKSI ( Berani, Aktif dan Sinergi)" },
    { id: 5, img: "/Logo Bemf fasilkom.png", title: "Fakultas Ilmu Komputer", link: "/bem-fakultas/Fasilkom", desc: "Kabinet Aksara" },
    { id: 6, img: "/Logo bemf fisioterapi.jpeg", title: "Fakultas Fisioterapi", link: "/bem-fakultas/Fisioterapi", desc: "Kabinet 2020/2021" },
    { id: 7, img: "/LOGO BEM FH.jpg", title: "Fakultas Hukum", link: "/bem-fakultas/Hukum", desc: "Kabinet 2020/2021" },
    { id: 8, img: "/Logo BEMF Psikologi.png", title: "Fakultas Psikologi", link: "/bem-fakultas/Psikologi", desc: "Kabinet BESTARI" },
    { id: 9, img: "/LOGO BEM FIKES.png", title: "Fakultas Ilmu-Ilmu Kesehatan", link: "/bem-fakultas/Fikes", desc: "Kabinet 2020/2021" },
    { id: 10, img: "/logo BEM Fakultas Keguruan dan Ilmu Pendidikan.jpg", title: "Fakultas Keguruan dan Ilmu Pendidikan", link: "/bem-fakultas/FKIP", desc: "Kabinet 2020/2021" },
]

const index = () => {
    return (
        <>
            <Head>
                <title>Badan Eksekutif Mahasiswa Fakultas - BEMUEU Official</title>
            </Head>
            <AppNavbar currentActive="bem fakultas" />
            <AppAnimatedBackgroundBlank sourceArray={kementerianArray} blockCaption="badan eksekutif mahasiswa fakultas" blockSubCaption="description info to be updated ..."/>
        </>
    )
}
 
export default index;