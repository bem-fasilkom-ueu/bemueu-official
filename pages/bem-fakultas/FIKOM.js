import AppBEMF  from "../../components/AppBEMF";


const importantProps = [
    { 
        dept: "Fakultas Ilmu Komunikasi", 
        desc: "BEM Fakultas Ilmu Komunikasi (Fikom) adalah Badan Eksekutif Mahasiswa yang menaungi 4 himpunan mahasiswa jurusan yang berada di fakultas ilmu komunikasi yaitu HMJ Public Relation (PR), HMJ Jurnalistik (Jurnal), HMJ Marketing Komunikasi (Markom), dan HMJ Broadcasting (BC). Serta menjadi jembatan antara mahasiswa Fikom maupun HMJ dengan pihak fakultas, BEM universitas, dan juga Universitas Esa Unggul.",
        kabinet: "2020/2021",
        logo: "/logo bem fikom.jpg",
        slug: "fikom",
        video: "OT82nmpWxmM"
    },
]

const orgMembers = [
    { id: 1, nama: "Muhikal Imamul Dzuhri", role: "Ketua", img: "/Muhikal Imamul Dzuhri - Ketua BEM FIKOM.jpg" },
    { id: 2, nama: "Meita Kalila Zahra", role: "Wakil Ketua", img: "/Meita Kalila Zahra - Wakil Ketua BEM FIKOM.jpg" },
    { id: 3, nama: "Farah Oktaviannisa", role: "Sekretaris", img: "/Farah Oktaviannisa - Sekretaris.jpeg" },
    { id: 4, nama: "Pricilla Cantika Putri A", role: "Bendahara", img: "/Pricilla Cantika Putri A - Bendahara.jpeg" },
    { id: 5, nama: "Muhammad Akbar Malik", role: "PIC People Development", img: "/Muhammad Akbar Malik  - PIC People Development.jpg" },
    { id: 6, nama: "Hanif Zulviansyah", role: "People Development", img: "/Hanif Zulviansyah - People Development.jpg" },
    { id: 7, nama: "Riandy Nurmanov", role: "People Development", img: "/Riandy Nurmanov - People Development.jpg" },
    { id: 8, nama: "Regita Maulidya Cahyani", role: "PIC Event Development", img: "/Regita Maulidya Cahyani - PIC Event Development.jpg" },
    { id: 9, nama: "Elpa Mega Octavia S", role: "Event Development", img: "/Elpa Mega Octavia S - Event Development.jpg" },
    { id: 10, nama: "Miftah Tarina", role: "Event Development", img: "/Miftah Tarina - Event Development.jpg" },
    { id: 11, nama: "Muzib Muhammad Sadiq", role: "Event Development", img: "/Muzib Muhammad Sadiq - Event Development.jpg" },
    { id: 12, nama: "Muhammad Cahya Piu", role: "PIC Creative Development", img: "/Muhammad Cahya Piu - PIC Creative Development.jpg" },
    { id: 13, nama: "Andian Indah Puspita", role: "Creative Development", img: "/Andian Indah Puspita - Creative Development.jpg" },
    { id: 14, nama: "Indrawansyah", role: "Creative Development", img: "/Indrawansyah- Creative Development.jpg" },
    { id: 15, nama: "Muhammad Yusuf R", role: "Creative Development", img: "/Muhammad Yusuf R - Creative Development.jpg" },
    { id: 16, nama: "Shelby Leona Rizky", role: "Creative Development", img: "/Shelby Leona Rizky - Creative Development.jpg" },
]

const FIKOM = () => {
    return (
        <>
            <AppBEMF customString={importantProps[0].slug} blockCaption={importantProps[0].dept} blockSubCaption={importantProps[0].kabinet} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} videoId={importantProps[0].video} />
        </>
    )
}
 
export default FIKOM;