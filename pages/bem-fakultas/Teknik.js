import AppBEMF  from "../../components/AppBEMF";


const importantProps = [
    { 
        dept: "Fakultas Teknik", 
        desc: "BEM FT UEU adalah Badan Eksekutif Mahasiswa di tingkat Fakultas Teknik yang bertugas untuk mengkoordinasikan seluruh Organisasi Mahasiswa (Ormawa) yang ada di Fakultas Teknik. Dengan semangat dan professional. BEM FT UEU bersama dengan seluruh Ormawa FT serta civitas akademika Fakultas Teknik berusaha mewujudkan Fakultas Teknik ke arah yang lebih baik.",
        kabinet: "KABINET BERAKSI ( Berani, Aktif dan Sinergi)",
        logo: "/logo bem ft.jpeg",
        slug: "teknik",
        video: ""
    },
]

const orgMembers = [
    { id: 1, nama: "Arif Triantoro", role: "Ketua", img: "/Ketua - Arif Triantoro.jpg" },
    { id: 2, nama: "Danny Primasmada", role: "Wakil Ketua", img: "/Wakil -  Danny Primasmada.jpeg" },
    { id: 3, nama: "Khofifah Dwi Yuliati", role: "Sekretaris", img: "/Sekertaris - Khofifah Dwi Yuliati.jpg" },
    { id: 4, nama: "Aulia Rahman Putri", role: "Bendahara", img: "/Bendahara - Aulia Rahman Putri.jpeg" },
    { id: 5, nama: "Anggie Nathania Astria", role: "Divisi Kominfo", img: "/Divisi Kominfo - Anggie Nathania Astria.jpg" },
    { id: 6, nama: "Christin Nadia Rumaherang", role: "Divisi Kominfo", img: "/Divisi Kominfo - Christin Nadia Rumaherang.jpeg" },
    { id: 7, nama: "Nanang Fahri", role: "Divisi Kominfo", img: "/Divisi Kominfo - Nanang Fahri.jpg" },
    { id: 8, nama: "Aida Indah Vitayala", role: "Divisi Pendidikan dan Kebudayaan", img: "/Divisi Pendidikan dan Kebudayaan - Aida Indah Vitayala.jpg" },
    { id: 9, nama: "Ichsan Adi Dharmastyo", role: "Divisi Pendidikan dan Kebudayaan", img: "/Divisi Pendidikan dan Kebudayaan - ichsan adi dharmastyo.jpg" },
    { id: 10, nama: "Irfan Romadloni", role: "Divisi Pendidikan dan Kebudayaan", img: "/Divisi Pendidikan dan Kebudayaan - Irfan Romadloni.jpg" },
    { id: 11, nama: "Muhamad Alif Bayhaqi", role: "Divisi Pendidikan dan Kebudayaan", img: "/Divisi Pendidikan dan Kebudayaan - Muhamad Alif Bayhaqi.jpeg" },
    { id: 12, nama: "Fadillah Darmawanty Listanto", role: "Divisi POSDM", img: "/Divisi POSDM - Fadillah Darmawanty Listanto.jpg" },
    { id: 13, nama: "Rahmat Wahyu Aji Saputra", role: "Divisi POSDM", img: "/Divisi POSDM - Rahmat Wahyu Aji Saputra.jpg" },
    { id: 14, nama: "Ricko Anggara", role: "Divisi POSDM", img: "/Divisi POSDM - Ricko Anggara.jpeg" },
    { id: 15, nama: "Salahudin Badaruny", role: "Divisi POSDM", img: "/Divisi POSDM - Salahudin Badaruny.jpeg" },
    { id: 16, nama: "Dimas Bagaskara Putra", role: "Divisi Sosial dan Kerohanian", img: "/Divisi Sosial dan Kerohaniaan - Dimas Bagaskara Putra.jpg" },
    { id: 17, nama: "Kristof Sulaeman Harefa", role: "Divisi Sosial dan Kerohanian", img: "/Divisi Sosial dan Kerohaniaan - Kristof Sulaeman Harefa.jpg" },
    { id: 18, nama: "Muhammad Rehan Ramadhan", role: "Divisi Sosial dan Kerohanian", img: "/Divisi Sosial dan Kerohaniaan - Muhammad Rehan Ramadhan.jpg" },
    { id: 19, nama: "Samuel Dean Van Harling.jpg", role: "Divisi Sosial dan Kerohanian", img: "/Divisi Sosial dan Kerohaniaan - Samuel Dean Van Harling.jpg" },
]

const Teknik = () => {
    return (
        <>
            <AppBEMF customString={importantProps[0].slug} blockCaption={importantProps[0].dept} blockSubCaption={importantProps[0].kabinet} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} videoId={importantProps[0].video} />
        </>
    )
}
 
export default Teknik;