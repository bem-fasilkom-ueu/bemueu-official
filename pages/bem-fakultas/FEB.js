import AppBEMF  from "../../components/AppBEMF";


const importantProps = [
    { 
        dept: "Fakultas Ekonomi dan Bisnis", 
        desc: "Organisasi yang menaungi aspirasi dari mahasiswa/i FEB UEU baik formal ataupun informal. BEMFEB Periode 2020/2021 ini memiliki 60 anggota yang dibagi dalam beberapa divisi.",
        kabinet: "Kabinet KORELASI (KOntributif, kREatif, koLAborasi, SInergi)",
        logo: "/Logo BEM FEB.png",
        slug: "feb",
        video: "EAiGQxte_Kk"
    },
]

const orgMembers = [
    { id: 1, nama: "Junita Dwi Astut", role: "Ketua", img: "/Ketua_BEM_-_Junita_Dwi_Astuti.jpeg" },
    { id: 2, nama: "Salim", role: "Wakil Ketua", img: "/Wakil _etua_BEM_-_Salim.jpeg" },
    { id: 3, nama: "Nensi Evelin Katerina Senandi", role: "Sekertaris Umum", img: "/Sekertaris_Umum_-_Nensi_Evelin_Katerina_Senandi.jpeg" },
    { id: 4, nama: "Eka Nur Cahyani", role: "Sekertaris", img: "/Eka_Nur_Cahyani.jpeg" },
    { id: 5, nama: "Ranti Ela Septiani", role: "Bendahara Umum", img: "/Bendahara_Umum_-_Ranti_Ela_Septiani.jpeg" },
    { id: 6, nama: "Siti Puji Primastuti", role: "Bendahara Bidang", img: "/Bendahara_Bidang_-_Siti_Puji_Primastuti.jpg" },
    { id: 7, nama: "Ade Syilvianita", role: "Divisi Komunikasi dan Informasi", img: "/Divisi_Komunikasi_dan_Informasi_-_Ade_Syilvianita.jpeg" },
    { id: 8, nama: "Apni Suryani", role: "Divisi Komunikasi dan Informasi", img: "/Divisi_Komunikasi_dan_Informasi_-_Apni_Suryani.jpeg" },
    { id: 9, nama: "Felisita Ferdinanda", role: "Divisi Komunikasi dan Informasi", img: "/Divisi_Komunikasi_dan_Informasi_-_Felisita_Ferdinanda.jpg" },
    { id: 10, nama: "Nanda Deprylia", role: "Divisi Komunikasi dan Informasi", img: "/Divisi_Komunikasi_dan_Informasi_-_Nanda_Deprylia.jpeg" },
    { id: 11, nama: "Nizar Rahmansyah", role: "Divisi Komunikasi dan Informasi", img: "/Divisi_Komunikasi_dan_Informasi_-_Nizar_Rahmansyah.jpeg" },
    { id: 12, nama: "Suci Indah", role: "Divisi Komunikasi dan Informasi", img: "/Divisi_Komunikasi_dan_Informasi_-_Suci_Indah.jpg" },
    { id: 13, nama: "Ahmad Dani", role: "Divisi Pendidikan dan Teknologi", img: "/Divisi_Pendidikan_dan_Teknologi_- _Ahmad_Dani.jpeg" },
    { id: 14, nama: "Albertus Valerino", role: "Divisi Pendidikan dan Teknologi", img: "/Divisi_Pendidikan_dan_Teknologi_-_Albertus_Valerino.jpeg" },
    { id: 15, nama: "Dzikri Ismun", role: "Divisi Pendidikan dan Teknologi", img: "/Divisi_Pendidikan_dan_Teknologi_-_Dzikri_Ismun.jpeg" },
    { id: 16, nama: "Fitriatul Zahroh", role: "Divisi Pendidikan dan Teknologi", img: "/Divisi_Pendidikan_dan_Teknologi_-_Fitriatul_Zahroh.jpeg" },
    { id: 17, nama: "Teguh Candra Maulana", role: "Divisi Pendidikan dan Teknologi", img: "/Divisi_Pendidikan_dan_Teknologi_-_Teguh_Candra_Maulana.jpeg" },
    { id: 18, nama: "Yogi Aska", role: "Divisi Pendidikan dan Teknologi", img: "/Divisi_Pendidikan_dan_Teknologi_-_Yogi_Aska.jpeg" },
    { id: 19, nama: "Diah Fitriani", role: "Divisi Kerohanian", img: "/Divisi_Kerohanian_- _Diah_Fitriani.jpeg" },
    { id: 20, nama: "Helmi Fikri", role: "Divisi Kerohanian", img: "/Divisi_Kerohanian_-_Helmi_Fikri.jpeg" },
    { id: 21, nama: "Irwan Ahmad Maulana", role: "Divisi Kerohanian", img: "/Divisi_Kerohanian_-_Irwan_Ahmad_Maulana.jpeg" },
    { id: 22, nama: "Jacob Steven", role: "Divisi Kerohanian", img: "/Divisi_Kerohanian_-_Jacob_Steven.jpeg" },
    { id: 23, nama: "Muhammad Al Haiqi Husin", role: "Divisi Kerohanian", img: "/Divisi_Kerohanian_-_Muhammad_Al_Haiqi_Husin.jpeg" },
    { id: 24, nama: "Ulfa Dianik", role: "Divisi Kerohanian", img: "/Divisi_Kerohanian_-_Ulfa_Dianik.jpeg" },
    { id: 25, nama: "Anggy Nur Afrilizah", role: "Divisi Kewirausahaan", img: "/Divisi_Kewirausahaan_-_Anggy_Nur_Afrilizah.jpg" },
    { id: 26, nama: "Debora Siagian", role: "Divisi Kewirausahaan", img: "/Divisi_Kewirausahaan_-_Debora_Siagian.jpg" },
    { id: 27, nama: "Eka Wahyu Febriyanti", role: "Divisi Kewirausahaan", img: "/Divisi_Kewirausahaan_-_Eka_Wahyu_Febriyanti.jpeg" },
    { id: 28, nama: "Feren Aprilia", role: "Divisi Kewirausahaan", img: "/Divisi_Kewirausahaan_-_Feren_Aprilia.jpeg" },
    { id: 29, nama: "Heru Setiawan", role: "Divisi Kewirausahaan", img: "/Divisi_Kewirausahaan_-_Heru_Setiawan.jpeg" },
    { id: 30, nama: "Ika Marliana Nurhayati", role: "Divisi Kewirausahaan", img: "/Divisi_Kewirausahaan_-_Ika_Marliana_Nurhayati.jpg" },
    { id: 31, nama: "Adtya Fahmi", role: "Divisi Minat dan Bakat", img: "/Divisi_Minat_dan_bakat_-_Adtya_Fahmi.jpeg" },
    { id: 32, nama: "Fachmi", role: "Divisi Minat dan Bakat", img: "/Divisi_Minat_dan_bakat_-_Fachmi.jpeg" },
    { id: 33, nama: "Fikri Prananda", role: "Divisi Minat dan Bakat", img: "/Divisi_Minat_dan_bakat_-_Fikri_Prananda.jpeg" },
    { id: 34, nama: "Fransisca Sisilia Sinaga", role: "Divisi Minat dan Bakat", img: "/Divisi_Minat_dan_bakat_-_Fransisca_Sisilia_Sinaga.jpeg" },
    { id: 35, nama: "Gigieh Giri Prianka", role: "Divisi Minat dan Bakat", img: "/Divisi_Minat_dan_bakat_-_Gigieh_Giri_Prianka.jpeg" },
    { id: 36, nama: "Gilang Damarjati", role: "Divisi Minat dan Bakat", img: "/Divisi_Minat_dan_bakat_-_Gilang_Damarjati.jpeg" },
    { id: 37, nama: "Indriyani Kristin", role: "Divisi Minat dan Bakat", img: "/Divisi_Minat_dan_bakat_-_Indriyani_Kristin.jpeg" },
    { id: 38, nama: "Ade Diskaningtyas", role: "Divisi Penelitian dan Pengembangan", img: "/Divisi_Penelitian_dan_Pengembangan_-_Ade_Diskaningtyas.jpeg" },
    { id: 39, nama: "Arin Dwitya", role: "Divisi Penelitian dan Pengembangan", img: "/Divisi_Penelitian_dan_Pengembangan_-_Arin_Dwitya.jpeg" },
    { id: 40, nama: "Ciga Mila Reftiana", role: "Divisi Penelitian dan Pengembangan", img: "/Divisi_Penelitian_dan_Pengembangan_-_Ciga_Mila_Reftiana.jpeg" },
    { id: 41, nama: "Dwi Sulistyani", role: "Divisi Penelitian dan Pengembangan", img: "/Divisi_Penelitian_dan_Pengembangan_-_Dwi_Sulistyani.jpeg" },
    { id: 42, nama: "Hilda Bernika", role: "Divisi Penelitian dan Pengembangan", img: "/Divisi_Penelitian_dan_Pengembangan_-_Hilda_Bernika.jpeg" },
    { id: 43, nama: "Rahmania", role: "Divisi Penelitian dan Pengembangan", img: "/Divisi_Penelitian_dan_Pengembangan_-_Rahmania.jpeg" },
    { id: 44, nama: "Risky Deni", role: "Divisi Penelitian dan Pengembangan", img: "/Divisi_Penelitian_dan_Pengembangan_-_Risky_Deni.jpeg" },
    { id: 45, nama: "Aldi Renaldi", role: "Divisi Sosial Budaya", img: "/Divisi Sosial Budaya - Aldi Renaldi.jpeg" },
    { id: 46, nama: "Angelica Viona", role: "Divisi Sosial Budaya", img: "/Divisi Sosial Budaya - Angelica Viona.jpeg" },
    { id: 47, nama: "Meidiana Damayanti", role: "Divisi Sosial Budaya", img: "/Divisi Sosial Budaya - Meidiana Damayanti.jpeg" },
    { id: 48, nama: "Tania Triamanda", role: "Divisi Sosial Budaya", img: "/Divisi Sosial Budaya - Tania Triamanda.jpeg" },
    { id: 49, nama: "Theresia Shintia", role: "Divisi Sosial Budaya", img: "/Divisi Sosial Budaya - Theresia Shintia.jpeg" },
    { id: 50, nama: "Tumpal Horison Jimmy", role: "Divisi Sosial Budaya", img: "/Divisi Sosial Budaya - Tumpal Horison Jimmy.jpeg" },
]

const FEB = () => {
    return (
        <>
            <AppBEMF customString={importantProps[0].slug} blockCaption={importantProps[0].dept} blockSubCaption={importantProps[0].kabinet} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} videoId={importantProps[0].video} />
        </>
    )
}
 
export default FEB;