import AppBEMF  from "../../components/AppBEMF";


const importantProps = [
    { 
        dept: "Fakultas Keguruan dan Ilmu Pendidikan", 
        desc: "Badan Eksekutif Mahasiswa Fakultas Keguruan dan Ilmu Pendidikan (BEM FKIP) adalah organisasi mahasiswa fakultas keguruan dan ilmu pendidikan yang diresmikan pada tahun 2015 oleh Presiden Mahasiswa periode 2014/2015 yaitu Aditya Firdausyi.",
        kabinet: "2020/2021",
        logo: "/logo BEM Fakultas Keguruan dan Ilmu Pendidikan.jpg",
        slug: "fkip",
        video: "-Qd9sSnwhqQ"
    },
]

const orgMembers = [
    { id: 1, nama: "Mahwar Alfan Nisa", role: "Ketua", img: "/Ketua (Gubernur) BEMFKIP - Mahwar Alfan Nisa.png" },
    { id: 2, nama: "Asfi Manzilah", role: "Wakil Ketua", img: "/Wakil Ketua (Wakil Gubernur) BEMFKIP - Asfi Manzilah.png" },
    { id: 3, nama: "Fikra Hawa Alia", role: "Sekretaris", img: "/Sekretaris BEMFKIP - Fikra Hawa Alia.png" },
    { id: 4, nama: "Oktafiani", role: "Sekretaris", img: "/Sekretaris BEMFKIP - Oktafiani.png" },
    { id: 5, nama: "Belinda Bilqiis Hendrawan", role: "Bendahara", img: "/Bendahara bemfkip - Belinda Bilqiis Hendrawan.png" },
    { id: 6, nama: "Dwi Ayu Lestari", role: "Bendahara", img: "/Bendahara bemfkip - Dwi Ayu Lestari.png" },
    { id: 7, nama: "Aldina Siti Nur Aliyah.", role: "Divisi Agama", img: "/Anggota Divisi Agama - Aldina Siti Nur Aliyah.png" },
    { id: 8, nama: "Arif Ramdana", role: "Divisi Agama", img: "/Anggota Divisi Agama - Arif Ramdana.png" },
    { id: 9, nama: "Sekar Mayang Bonawati", role: "Divisi Agama", img: "/Anggota Divisi Agama - Sekar Mayang Bonawati.png" },
    { id: 10, nama: "Timbul Franciscus Manullang", role: "Divisi Agama", img: "/Anggota Divisi Agama - Timbul Franciscus Manullang.png" },
    { id: 11, nama: "Annisa Cahyani", role: "Divisi Eksternal", img: "/Anggota Divisi Eksternal - Annisa Cahyani.png" },
    { id: 12, nama: "Deltania", role: "Divisi Eksternal", img: "/Anggota Divisi Eksternal - Deltania.png" },
    { id: 13, nama: "Febriyanti Rizki Nurfadilah", role: "Divisi Eksternal", img: "/Anggota Divisi Eksternal - Febriyanti Rizki Nurfadilah.png" },
    { id: 14, nama: "Razela Regina Putri", role: "Divisi Eksternal", img: "/Anggota Divisi Eksternal - Razela Regina Putri.png" },
    { id: 15, nama: "Anes Eka Lestari", role: "Divisi Insospol", img: "/Anggota Divisi Insospol - Anes Eka Lestari.png" },
    { id: 16, nama: "Raabiah Adawiyah", role: "Divisi Insospol", img: "/Anggota Divisi Insospol - Raabiah Adawiyah.png" },
    { id: 17, nama: "Rikzan Kurniawan Hidayat", role: "Divisi Insospol", img: "/Anggota Divisi Insospol - Rikzan Kurniawan Hidayat.png" },
    { id: 18, nama: "Selli Wahyuningrum", role: "Divisi Insospol", img: "/Anggota Divisi Insospol - Selli Wahyuningrum.png" },
    { id: 19, nama: "Dian Erisa Nurmala Cahyaningrum", role: "Divisi Kewirausahaan", img: "/Anggota Divisi Kewirausahaan - Dian Erisa Nurmala Cahyaningrum.png" },
    { id: 20, nama: "Nurul Azkiya", role: "Divisi Kewirausahaan", img: "/Anggota Divisi Kewirausahaan - Nurul Azkiya.png" },
    { id: 21, nama: "Rani Mahareka", role: "Divisi Kewirausahaan", img: "/Anggota Divisi Kewirausahaan - Rani Mahareka.png" },
    { id: 22, nama: "Hercahyo Adhon Pribadi", role: "Divisi Minat dan Bakat", img: "/Anggota Divisi Minat _ Bakat - Hercahyo Adhon Pribadi.png" },
    { id: 23, nama: "Muhamaad Idzul Adha", role: "Divisi Minat dan Bakat", img: "/Anggota Divisi Minat _ Bakat - Muhamaad Idzul Adha.png" },
    { id: 24, nama: "Reza Putri Hardiyanti", role: "Divisi Minat dan Bakat", img: "/Anggota Divisi Minat _ Bakat - Reza Putri Hardiyanti.png" },
    { id: 25, nama: "Aulinda Nurfikriyah Suhaemi", role: "Divisi Pendidikan", img: "/Anggota Divisi Pendidikan - Aulinda Nurfikriyah Suhaemi.png" },
    { id: 26, nama: "Elisa Yolanda", role: "Divisi Pendidikan", img: "/Anggota Divisi Pendidikan - Elisa Yolanda.png" },
    { id: 27, nama: "Sumarni", role: "Divisi Pendidikan", img: "/Anggota Divisi Pendidikan - Sumarni.png" },
    { id: 28, nama: "Yudis Frandikta", role: "Divisi Pendidikan", img: "/Anggota Divisi Pendidikan - Yudis Frandikta.png" },
]

const FKIP = () => {
    return (
        <>
            <AppBEMF customString={importantProps[0].slug} blockCaption={importantProps[0].dept} blockSubCaption={importantProps[0].kabinet} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} videoId={importantProps[0].video} />
        </>
    )
}
 
export default FKIP;