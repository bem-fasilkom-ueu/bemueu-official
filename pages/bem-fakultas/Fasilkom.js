import AppBEMF  from "../../components/AppBEMF";


const importantProps = [
    { 
        dept: "Fakultas Ilmu Komputer", 
        desc: "Bem fasilkom berfungsi sebagai organisasi pelaksana kegiatan pengembangan kemahasiswaan fasilkom, terutama yang berkaitan dengan pengembangan penalaran dan keilmuan, pengembangan sikap kepemimpinan dan keterampilan manajemen.",
        kabinet: "Kabinet Askara",
        logo: "/Logo Bemf fasilkom.png",
        slug: "fasilkom",
        video: ""
    },
]

const orgMembers = [
    { id: 1, nama: "Muhammad Rizky Perdana", role: "Ketua", img: "/Ketua - Muhammad Rizky Perdana.jpg" },
    { id: 2, nama: "Irvan Trinanda", role: "Wakil Ketua", img: "/Wakil Ketua - Irvan Trinanda.jpg" },
    { id: 3, nama: "Sinta Adinda Azhari", role: "Sekretaris 1", img: "/Sekretaris 1 - Sinta Adinda Azhari.jpg" },
    { id: 4, nama: "Siti Meilianawati", role: "Sekretaris 2", img: "/Sekretaris 2 - Siti Meilianawati.jpg" },
    { id: 5, nama: "Aulia Rahman Putri", role: "Bendahara", img: "/Bendahara - Aulia Rahman Putri.jpeg" },
    { id: 6, nama: "Faldi Azmi", role: "Divisi Apresiasi Seni Budaya", img: "/Divisi Apresiasi Seni Budaya - Faldi Azmi.png" },
    { id: 7, nama: "Rudy Setiawan", role: "Divisi Apresiasi Seni Budaya", img: "/Divisi Apresiasi Seni Budaya - Rudy Setiawan.png" },
    { id: 8, nama: "Syafitri Ibrahim Latief", role: "Divisi Apresiasi Seni Budaya", img: "/Divisi Apresiasi Seni Budaya - Syafitri Ibrahim Latief.jpg" },
    { id: 9, nama: "Fikri Adiyatma", role: "Divisi Biro Rumah Tangga", img: "/Divisi Biro Rumah Tangga - Fikri Adiyatma.jpg" },
    { id: 10, nama: "Herdy Pramudyanto", role: "Divisi Biro Rumah Tangga", img: "/Divisi Biro Rumah Tangga - Herdy Pramudyanto.png" },
    { id: 11, nama: "Qhodir Jailani", role: "Divisi Biro Rumah Tangga", img: "/Divisi Biro Rumah Tangga - Qhodir Jailani.jpg" },
    { id: 12, nama: "Anggi Al Parizi", role: "Divisi Kaderisasi", img: "/Divisi Kaderisasi - Anggi Al Parizi.png" },
    { id: 13, nama: "Azhahra Vindy Ariesta", role: "Divisi Kaderisasi", img: "/Divisi Kaderisasi - Azhahra Vindy Ariesta.png" },
    { id: 14, nama: "Ramadhan Muhammad Fabrianto", role: "Divisi Kaderisasi", img: "/Divisi Kaderisasi - Ramadhan Muhammad Fabrianto.png" },
    { id: 15, nama: "Vrantika Br Samosir", role: "Divisi Kaderisasi", img: "/Divisi Kaderisasi - Vrantika Br Samosir.png" },
    { id: 16, nama: "Emon Sulaksana", role: "Divisi Kominfo", img: "/Divisi Kominfo - Emon Sulaksana.jpg" },
    { id: 17, nama: "Maylida Izattul Wardah", role: "Divisi Kominfo", img: "/Divisi Kominfo - Maylida Izattul Wardah.jpg" },
    { id: 18, nama: "Putri Nur Salsabilla", role: "Divisi Kominfo", img: "/Divisi Kominfo - Putri Nur Salsabilla.png" },
    { id: 19, nama: "Ridho Ramadhana", role: "Divisi Kominfo", img: "/Divisi Kominfo - Ridho Ramadhana.png" },
    { id: 20, nama: "Redja Djata Saputra", role: "Divisi Minat dan Bakat", img: "/Divisi Minat dan Bakat - Redja Djata Saputra.png" },
    { id: 21, nama: "Risda Tamam Aljava", role: "Divisi Minat dan Bakat", img: "/Divisi Minat dan Bakat - Risda Tamam Aljava.png" },
    { id: 22, nama: "Ronan Harris", role: "Divisi Minat dan Bakat", img: "/Divisi Minat dan Bakat - Ronan Harri.png" },
    { id: 23, nama: "Aji Sofyan", role: "Divisi Pendidikan", img: "/Divisi Pendidikan - Aji Sofyan.png" },
    { id: 24, nama: "Bonaventura Satrio Wicaksono", role: "Divisi Pendidikan", img: "/Divisi Pendidikan - Bonaventura Satrio Wicaksono.png" },
    { id: 25, nama: "Ranny Febriana", role: "Divisi Pendidikan", img: "/Divisi Pendidikan - Ranny Febriana.png" },
    { id: 26, nama: "Muhammad Alvi Adnan", role: "Divisi Pengabdian Masyarakat", img: "/Divisi Pengabdian Masyarakat - Muhammad Alvi Adnan.png" },
    { id: 27, nama: "Naufal Taufiqurrahman", role: "Divisi Pengabdian Masyarakat", img: "/Divisi Pengabdian Masyarakat - Naufal Taufiqurrahman.png" },
    { id: 28, nama: "Reza Mutia Arrahman", role: "Divisi PDSM", img: "/Divisi PSDM - Reza Mutia Arrahman.png" },
    { id: 29, nama: "Primas Pranata", role: "Ketua Divisi Sosial", img: "/Primas Pranata - Kepala Departemen Sosial.jpg" },
    { id: 30, nama: "Lutvia Nabila Putri", role: "Divisi Sosial Agama", img: "/Divisi Sosial Agama - Lutvia Nabila Putri.png" },
    { id: 31, nama: "Tobi Wahyu Prayoga", role: "Divisi Sosial Agama", img: "/Divisi Sosial Agama - Tobi Wahyu Prayoga.png" },
]

const Fasilkom = () => {
    return (
        <>
            <AppBEMF customString={importantProps[0].slug} blockCaption={importantProps[0].dept} blockSubCaption={importantProps[0].kabinet} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} videoId={importantProps[0].video} />
        </>
    )
}
 
export default Fasilkom;