import AppBEMF  from "../../components/AppBEMF";


const importantProps = [
    { 
        dept: "Fakultas Desain dan Industri Kreatif", 
        desc: "Menjadikan BEM FDIK sebagai organisasi yang lebih aktif serta kreatif, dan inovatif sebagai wadah aspirasi mahasiswa FDIK untuk mencapai tujuan bersama.",
        kabinet: "Kabinet Inovatif",
        logo: "/logo_bem_fdik.jpeg",
        slug: "fdik",
        video: "ncJJT1eppEk"
    },
]

const orgMembers = [
    { id: 1, nama: "Ferriansyah Husein", role: "Ketua", img: "/Ketua_BEM_-_Ferriansyah_Husein.jpg" },
    { id: 2, nama: "M Raihan Subagia", role: "Wakil Ketua", img: "/Wakil_Ketua_BEM_-_M_Raihan_Subagia.jpg" },
    { id: 3, nama: "Aulia Nisrina Salsabilla", role: "Sekertaris", img: "/Sekertaris_BEM_-_Aulia_Nisrina_Salsabilla.jpg" },
    { id: 4, nama: "Adhellia Purnamasari", role: "Bendahara", img: "/Bendahara_BEM_-_Adhellia_Purnamasari.jpg" },
    { id: 5, nama: "Dina Millenia Lestari", role: "Ketua Divisi Sosial Masyarakat", img: "/Ketua_Divisi_Sosial_Masyarakat_-_Dina_Millenia_Lestari.jpg" },
    { id: 6, nama: "Mohamad Dahlan", role: "Divisi Sosial Masyarakat", img: "/Divisi_Sosial_Masyarakat_-_Mohamad_Dahlan.jpg" },
    { id: 7, nama: "Salsabila Yusuf Putri", role: "Divisi Sosial Masyarakat", img: "/Divisi_Sosial_Masyarakat_-_Salsabila_Yusuf_Putri.jpg" },
    { id: 8, nama: "Tri Indriyani Nandasari", role: "Divisi Sosial Masyarakat", img: "/Divisi_Sosial_Masyarakat_-_Tri_Indriyani_Nandasari.jpg" },
    { id: 9, nama: "Albert Fransciano", role: "Divisi Media Kreatif", img: "/Divisi_Media_Kreatif_-_Albert_Fransciano.jpg" },
    { id: 10, nama: "Alyaa Yosi Fatika", role: "Divisi Media Kreatif", img: "/Divisi_Media_Kreatif_-_Alyaa_Yosi_Fatika.jpg" },
    { id: 11, nama: "M Alreizy Khossy Pratama", role: "Divisi Media Kreatif", img: "/Divisi_Media_Kreatif_-_M_Alreizy_Khossy_Pratama.jpg" },
    { id: 12, nama: "Vela Aditya", role: "Divisi Media Kreatif", img: "/Divisi_Media_Kreatif_-_Vela_Aditya.jpg" },
    { id: 13, nama: "Rendy Julianto", role: "Divisi Minat dan Bakat", img: "/Divisi_Minat_Bakat_-_Rendy_Julianto.jpg" },
    { id: 14, nama: "Silvi Fauziyah", role: "Divisi Minat dan Bakat", img: "/Divisi_Minat_Bakat_-_Silvi_Fauziyah.jpg" },
    { id: 15, nama: "Vivi Veggi Apriliani", role: "Divisi Minat dan Bakat", img: "/Divisi_Minat_Bakat_-_Vivi_Veggi_Apriliani.jpg" },
    { id: 16, nama: "Aulia Urfi", role: "Divisi Kewirausahaan", img: "/Divisi_Kewirausahaan_-_Aulia_Urfi.jpg" },
    { id: 17, nama: "Muhamad Andrean", role: "Divisi Kewirausahaan", img: "/Divisi_Kewirausahaan_-_Muhamad_Andrean.jpg" },
    { id: 18, nama: "Wayasirly Yola Pangestu", role: "Divisi Kewirausahaan", img: "/Divisi_Kewirausahaan_-_Wayasirly_Yola_Pangestu.jpg" },
]

const FDIK = () => {
    return (
        <>
            <AppBEMF customString={importantProps[0].slug} blockCaption={importantProps[0].dept} blockSubCaption={importantProps[0].kabinet} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} videoId={importantProps[0].video} />
        </>
    )
}
 
export default FDIK;