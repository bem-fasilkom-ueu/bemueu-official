import AppBEMF  from "../../components/AppBEMF";


const importantProps = [
    { 
        dept: "Fakultas Fisioterapi", 
        desc: "Badan Eksekutif Mahasiswa Fakultas (BEMF) Fisioterapi merupakan organisasi intra kampus yang merupakan lembaga eksekutif tingkat fakultas. Badan Eksekutif Mahasiswa Fakultas (BEMF) Fisioterapi memiliki peran dalam menampung aspirasi-aspirasi mahasiswa fakultas fisioterapi untuk dapat disuarakan dan dilaksanakan dalam bentuk program-program kerja. Badan Eksekutif Mahasiswa Fakultas (BEMF) Fisioterapi memiliki nilai-nilai yang diterapkan dalam berorganisasi yaitu nilai dedikatif, inovatif, aspiratif, dan optimal. Visi dari Badan Eksekutif Mahasiswa Fakultas (BEMF) Fisioterapi yaitu mewujudkan BEMF Fisioterapi sebagai lembaga yang aktif, profesional dan komunikatif dengan bermusyawarah dan misi yang terdiri dari memperhatikan aspirasi mahasiswa fakultas fisioterapi agar terwujudnya inovasi dalam bidang akademik dan non akademik dan meningkatkan sinergritas yang baik antar fakultas.",
        kabinet: "2020/2021",
        logo: "/Logo bemf fisioterapi.jpeg",
        slug: "fisioterapi",
        video: "sc5dTV6oUMQ"
    },
]

const orgMembers = [
    { id: 1, nama: "Bayhaqi Arrahman", role: "Ketua", img: "/Ketua - Bayhaqi Arrahman.jpg" },
    { id: 2, nama: "Florentina Caren Ekaristi Putri", role: "Sekretaris", img: "/Sekretaris - Florentina Caren Ekaristi Putri.jpg" },
    { id: 3, nama: "Pinkka Nabila Tahani", role: "Sekretaris", img: "/Sekretaris - Pinkka Nabila Tahani.jpg" },
    { id: 4, nama: "Euniche Dwi Sepnita", role: "Bendahara", img: "/Bendahara - Euniche Dwi Sepnita.jpg" },
    { id: 5, nama: "Yemima Picela Wulandary", role: "Bendahara", img: "/Bendahara - Yemima Picela Wulandary.jpg" },
    { id: 6, nama: "Devi Soleha", role: "Divisi Agama", img: "/Divisi Agama - Devi Soleha.jpg" },
    { id: 7, nama: "Difa Akmal Al Alifi", role: "Divisi Agama", img: "/Divisi Agama - Difa Akmal Al Alifi.jpg" },
    { id: 8, nama: "Maria Delatitis Forasensa", role: "Divisi Agama", img: "/Divisi Agama - Maria Delatitis Forasensa.jpg" },
    { id: 9, nama: "Silvalistha Izaadora Latuny", role: "Divisi Agama", img: "/Divisi Agama - Silvalistha Izaadora Latuny.jpg" },
    { id: 10, nama: "Fachruramadhan", role: "Divisi Dana dan Usaha", img: "/Divisi Dana dan Usaha - Fachruramadhan.png" },
    { id: 11, nama: "Maura Jessica Cahya Ningsih", role: "Divisi Dana dan Usaha", img: "/Divisi Dana dan Usaha - Maura Jessica Cahya Ningsih.jpg" },
    { id: 12, nama: "Uga Cahyani", role: "Divisi Dana dan Usaha", img: "/Divisi Dana dan Usaha - Uga Cahyani.jpeg" },
    { id: 13, nama: "Zikri Qalbi Omidani", role: "Divisi Dana dan Usaha", img: "/Divisi Dana dan Usaha - Zikri Qalbi Omidani.jpg" },
    { id: 14, nama: "Alfiyyah Salsabila Khansa", role: "Divisi Kominfo", img: "/Divisi Kominfo - Alfiyyah Salsabila Khansa.jpg" },
    { id: 15, nama: "Deni Wahyu Romadhan", role: "Divisi Kominfo", img: "/Divisi Kominfo - Deni Wahyu Romadhan.jpg" },
    { id: 16, nama: "Dinda Eka Nur Amalia Budiarto", role: "Divisi Kominfo", img: "/Divisi Kominfo - Dinda Eka Nur Amalia Budiarto.jpg" },
    { id: 17, nama: "Nabila Azzahra", role: "Divisi Kominfo", img: "/Divisi Kominfo - Nabila Azzahra.jpg" },
    { id: 18, nama: "Thia Arina Putri", role: "Divisi Kominfo", img: "/Divisi Kominfo - Thia Arina Putri.jpg" },
    { id: 19, nama: "Fabyan Karlos Saputra", role: "Divisi Olahraga", img: "/Divisi Olahraga - Fabyan Karlos Saputra.jpg" },
    { id: 20, nama: "M Naufal Raihan", role: "Divisi Olahraga", img: "/Divisi Olahraga - M. Naufal Raihan.jpg" },
    { id: 21, nama: "Risky Annisa Paisal", role: "Divisi Olahraga", img: "/Divisi Olahraga - Risky Annisa Paisal.jpg" },
    { id: 22, nama: "Rosa Amelia", role: "Divisi Olahraga", img: "/Divisi Olahraga - Rosa Amelia.jpg" },
    { id: 23, nama: "Alfi Lutfiani", role: "Divisi Pendidikan", img: "/Divisi Pendidikan - Alfi Lutfiani.png" },
    { id: 24, nama: "Febrianti Sulistia", role: "Divisi Pendidikan", img: "/Divisi Pendidikan - Febrianti Sulistia.jpg" },
    { id: 25, nama: "Joice Rambu Djadjila", role: "Divisi Pendidikan", img: "/Divisi Pendidikan - Joice Rambu Djadjila.jpeg" },
    { id: 26, nama: "Julia Franciska Rose Duski", role: "Divisi Pendidikan", img: "/Divisi Pendidikan - Julia Franciska Rose Duski.jpg" },
    { id: 27, nama: "Muhammad Dwipa Syuktrifadli", role: "Divisi Pendidikan", img: "/Divisi Pendidikan - Muhammad Dwipa Syuktrifadli .png" },
]

const Fisioterapi = () => {
    return (
        <>
            <AppBEMF customString={importantProps[0].slug} blockCaption={importantProps[0].dept} blockSubCaption={importantProps[0].kabinet} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} videoId={importantProps[0].video} />
        </>
    )
}
 
export default Fisioterapi;