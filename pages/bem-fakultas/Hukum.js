import AppBEMF  from "../../components/AppBEMF";


const importantProps = [
    { 
        dept: "Fakultas Hukum", 
        desc: "Badan Eksekutif Mahasiswa Fakultas Hukum adalah Lembaga tertinggi pemegang kekuasaan eksekutif yang diberi tugas dan tanggungjawab untuk melaksanakan program kegiatan kemahasiswaan di Fakultas Hukum Universitas Esa Unggul.",
        kabinet: "2020/2021",
        logo: "/LOGO BEM FH.jpg",
        slug: "hukum",
        video: "b7PDPIZljN4"
    },
]

const orgMembers = [
    { id: 1, nama: "Jonathan Simanjuntak", role: "Ketua", img: "/Ketua BEM FH - Jonathan Simanjuntak_.jpg" },
    { id: 2, nama: "Andre", role: "Wakil Ketua", img: "/Wakil Ketua BEM FH - Andre.jpg" },
    { id: 3, nama: "Ika Aprilia Paulina", role: "Sekretaris", img: "/Sekretaris BEM FH - Ika Aprilia Paulina.jpg" },
    { id: 4, nama: "Andra Flagesy", role: "Bendahara", img: "/Bendahara BEM FH - Andra Flagesy.jpg" },
    { id: 5, nama: "Intan Permata Sari", role: "Ketua Divisi Humas", img: "/Ketua divisi Humas - Intan Permata Sari.jpg" },
    { id: 6, nama: "Rafdi Hakim", role: "Wakil Ketua Divisi Humas", img: "/Wakil Ketua divisi Humas - Rafdi Hakim.png" },
    { id: 7, nama: "Muhammad Dwi Karcahyo", role: "Divisi Humas", img: "/Anggota divisi Humas - Muhammad Dwi Karcahyo.jpg" },
    { id: 8, nama: "Muhammad Tegar", role: "Divisi Humas", img: "/Anggota divisi Humas - Muhammad Tegar.jpg" },
    { id: 9, nama: "Salma Nurazizah", role: "Divisi Humas", img: "/Anggota divisi Humas - Salma Nurazizah.jpg" },
    { id: 10, nama: "Riyan Goklas", role: "Ketua Divisi Dana dan Usaha", img: "/Ketua divisi Dana dan Usaha - Riyan Goklas.jpg" },
    { id: 11, nama: "Jelny Meredith Napitupulu", role: "Wakil Ketua Divisi Dana dan Usaha", img: "/Wakil Ketua divisi Dana dan Usaha - Jelny Meredith Napitupulu.jpg" },
    { id: 12, nama: "Arina Pramudita", role: "Divisi Dana dan Usaha", img: "/Anggota divisi Dana dan Usaha - Arina Pramudita.png" },
    { id: 13, nama: "Muhamad Abudin", role: "Divisi Dana dan Usaha", img: "/Anggota divisi Dana dan Usaha - Muhamad Abudin.jpg" },
    { id: 14, nama: "Muhammad Najmi", role: "Ketua Divisi Kajian dan Strategis", img: "/Ketua divisi Kajian dan Stategis - Muhammad Najmi.jpg" },
    { id: 15, nama: "Aldy Prasetyo", role: "Wakil Ketua Divisi Kajian dan Strategis", img: "/Wakil Ketua divisi Kajian dan Strategis - Aldy Prasetyo.jpg" },
    { id: 16, nama: "Julio Padot Sitanggang", role: "Divisi Kajian dan Strategis", img: "/Anggota divisi Kajian dan Stategis - Julio Padot Sitanggang.jpg" },
    { id: 17, nama: "Abitiya Sastawiyana", role: "Divisi Kajian dan Strategis", img: "/Anggota divisi Kajian dan Stategis- Abitiya Sastawiyana.jpg" },
    { id: 18, nama: "Oktavianus Corsini Hadi", role: "Divisi Kajian dan Strategis", img: "/Anggota divisi Kajian dan Strategis - Oktavianus Corsini Hadi.jpg" },
    { id: 19, nama: "Muhammad Alfian", role: "Ketua Divisi Minat dan Bakat", img: "/Ketua divisi Minat dan bakat - Muhammad Alfian.jpg" },
    { id: 20, nama: "Harry E Rajagukguk", role: "Wakil Ketua Divisi Minat dan Bakat", img: "/Wakil Ketua divisi Minat dan Bakat - Harry E Rajagukguk.jpg" },
    { id: 21, nama: "Deiza Rizqi Ramadhan", role: "Divisi Minat dan Bakat", img: "/Anggota divisi Minat dan Bakat - Deiza Rizqi Ramadhan.png" },
    { id: 22, nama: "Nurul Mushlihah", role: "Divisi Minat dan Bakat", img: "/Anggota divisi Minat dan Bakat - Nurul Mushlihah.jpg" },
    { id: 23, nama: "Samuel Valentino Partogi", role: "Divisi Minat dan Bakat", img: "/Anggota divisi Minat dan Bakat - Samuel Valentino Partogi.jpg" },
    { id: 24, nama: "Yehezkiel Naftaly Pratama", role: "Divisi Minat dan Bakat", img: "/Anggota divisi Minat dan Bakat - Yehezkiel Naftaly Pratama.jpg" },
    { id: 25, nama: "Dilla Fanny", role: "Divisi Minat dan Bakat", img: "/Anggota divisi Minat dan Bakat- Dilla Fanny.jpg" },
    { id: 26, nama: "Deva Ghita Anggraini", role: "Ketua Divisi Pendidikan", img: "/Ketua divisi Pendidikan - Deva Ghita Anggraini.jpg" },
    { id: 27, nama: "Maulizidan Malik Fajar", role: "Wakil Ketua Divisi Pendidikan", img: "/Wakil Ketua divisi Pendidikan - Maulizidan Malik Fajar.jpg" },
    { id: 28, nama: "Dhika Surya", role: "Divisi Pendidikan", img: "/Anggota divisi Pendidikan - Dhika Surya.jpg" },
    { id: 29, nama: "Jeremi Laksamana Dennis", role: "Divisi Pendidikan", img: "/Anggota divisi Pendidikan - Jeremi Laksamana Dennis.jpg" },
    { id: 30, nama: "Mila Amalia", role: "Divisi Pendidikan", img: "/Anggota divisi Pendidikan - Mila Amalia.jpg" },
    { id: 31, nama: "Najla Annisa", role: "Divisi Pendidikan", img: "/Anggota divisi Pendidikan - Najla Annisa.png" },
    { id: 32, nama: "Rico Marleve Disaely", role: "Divisi Pendidikan", img: "/Anggota divisi Pendidikan - Rico Marleve Disaely.png" },
    { id: 33, nama: "Risma Damayanti", role: "Ketua Divisi Pengabdian Masyarakat", img: "/Ketua divisi Pengabdian Masyarakat - Risma Damayanti.jpeg" },
    { id: 34, nama: "Kezia Pradivta Arifani", role: "Wakil Ketua Divisi Pengabdian Masyarakat", img: "/Wakil Ketua divisi Pengabdian Masyarakat - Kezia Pradivta Arifani.png" },
    { id: 35, nama: "Guido Sabar Dame Situmorang", role: "Divisi Pengabdian Masyarakat", img: "/Anggota divisi  Pengabdian Masyarakat - Guido Sabar Dame Situmorang.jpg" },
    { id: 36, nama: "Jefry Pernando Sitepu", role: "Divisi Pengabdian Masyarakat", img: "/Anggota Divisi Pengabdian Masyarakat - Jefry Pernando Sitepu.jpg" },
    { id: 37, nama: "Muhammad Andre Pratama", role: "Divisi Pengabdian Masyarakat", img: "/Anggota divisi Pengabdian Masyarakat - Muhammad Andre Pratama.jpg" },
    { id: 38, nama: "Tiara Shavira", role: "Divisi Pengabdian Masyarakat", img: "/Anggota divisi Pengabdian Masyarakat -Tiara Shavira.jpg" },
]

const Hukum = () => {
    return (
        <>
            <AppBEMF customString={importantProps[0].slug} blockCaption={importantProps[0].dept} blockSubCaption={importantProps[0].kabinet} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} videoId={importantProps[0].video} />
        </>
    )
}
 
export default Hukum;