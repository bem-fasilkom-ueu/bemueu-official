import AppBEMF  from "../../components/AppBEMF";


const importantProps = [
    { 
        dept: "Fakultas Psikologi", 
        desc: "Badan Eksekutif Mahasiswa Fakultas Psikologi Universitas Esa Unggul adalah organisasi kemahasiswaan yang bergerak pada bidang eksekutif. BEMF Psikologi bersifat otonomi, yang diberi kebebasan untuk membuat dan mengelola suatu kegiatan yang akan diselenggarakan di Fakultas Psikologi.",
        kabinet: "Kabinet BESTARI",
        logo: "/Logo BEMF Psikologi.png",
        slug: "psikologi",
        video: "pMooP7kXGas"
    },
]

const orgMembers = [
    { id: 1, nama: "Muhammad Nur Huda", role: "Ketua", img: "/Gubernur - Muhammad Nur Huda.png" },
    { id: 2, nama: "Atur Ariyadi", role: "Wakil Ketua", img: "/Wakil Gubernur - Atur Ariyadi.png" },
    { id: 3, nama: "Putri Nuranida", role: "Sekretaris", img: "/Sekretaris - Putri Nuranida.png" },
    { id: 4, nama: "Mieke Ariestaningrum", role: "Bendahara", img: "/Bendahara - Mieke Ariestaningrum.png" },
    { id: 5, nama: "Nurfadilla", role: "Koordinator Divisi Humas", img: "/Koordinator Divisi Hubungan Masyarakat - Nurfadilla.png" },
    { id: 6, nama: "Linaldi Israz", role: "Staff Divisi Humas", img: "/Staff Divisi Hubungan Masyarakat - Linaldi Israz.png" },
    { id: 7, nama: "Nabilla Aulia Putri", role: "Staff Divisi Humas", img: "/Staff Divisi Hubungan Masyarakat - Nabilla Aulia Putri.png" },
    { id: 8, nama: "Maya Famela Oktaviani", role: "Koordinator Divisi Kewirausahaan", img: "/Koordinator Divisi Kewirausahaan - Maya Famela Oktaviani.png" },
    { id: 9, nama: "Kania Eka Putri", role: "Staff Divisi Kewirausahaan", img: "/Staff Divisi Kewirausahaan - Kania Eka Putri.png" },
    { id: 10, nama: "Muhammad Fadhol Riyadi", role: "Staff Divisi Kewirausahaan", img: "/Staff Divisi Kewirausahaan - Muhammad Fadhol Riyadi.png" },
    { id: 11, nama: "Dina Fawziah", role: "Koordinator Divisi Pendidikan", img: "/Koordinator Divisi Pendidikan - Dina Fawziah.png" },
    { id: 12, nama: "Lia Rahmayanti Yudistura Munthe", role: "Staff Divisi Pendidikan", img: "/Staff Divisi Pendidikan - Lia Rahmayanti Yudistura Munthe.png" },
    { id: 13, nama: "Muhammad Hafizh Athallah", role: "Staff Divisi Pendidikan", img: "/Staff Divisi Pendidikan - Muhammad Hafizh Athallah.png" },
    { id: 14, nama: "Muhammad Alif Bermansyah", role: "Koordinator Divisi PSDM", img: "/Koordinator Divisi Pengembangan Sumber Daya Mahasiswa - Muhammad Alif Bermansyah.png" },
    { id: 15, nama: "Annisa Rizkia Ananda Sismanita", role: "Staff Divisi PSDM", img: "/Staff Divisi Pengembangan Sumber Daya Mahasiswa - Annisa Rizkia Ananda Sismanita.png" },
    { id: 16, nama: "Farisca Aprilia Permatasari", role: "Staff Divisi PSDM", img: "/Staff Divisi Pengembangan Sumber Daya Mahasiswa - Farisca Aprilia Permatasari.png" },
    { id: 17, nama: "Abdul Rochim Fauzi", role: "Koordinator Divisi Sosial Politik", img: "/Koordinator Divisi Sosial Politik - Abdul Rochim Fauzi.png" },
    { id: 18, nama: "Mas Ngabai Buya Abie Pangestu", role: "Staff Divisi Sosial Politik", img: "/Staff Divisi Sosial Politik - Mas Ngabai Buya Abie Pangestu.png" },
    { id: 19, nama: "Rere Kenza Aprilio", role: "Staff Divisi Sosial Politik", img: "/Staff Divisi Sosial Politik - Rere Kenza Aprilio.png" },
]

const Psikologi = () => {
    return (
        <>
            <AppBEMF customString={importantProps[0].slug} blockCaption={importantProps[0].dept} blockSubCaption={importantProps[0].kabinet} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} videoId={importantProps[0].video} />
        </>
    )
}
 
export default Psikologi;