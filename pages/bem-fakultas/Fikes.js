import AppBEMF  from "../../components/AppBEMF";


const importantProps = [
    { 
        dept: "Fakultas Ilmu-Ilmu Kesehatan", 
        desc: "BEM FIKes berfungsi dalam berbagai aspirasi mahasiswa FIKes untuk disampaikan ke pihak fakultas untuk kelancaran dalam pemaksimalan hak mahasiswa FIKes UEU. Selain itu, BEM FIKes memiliki berbagai program yang mampu memberikan pengaruh positif kepada seluruh mahasiswa FIKes dalam program akademik dan non akademik.",
        kabinet: "2020/2021",
        logo: "/LOGO BEM FIKES.png",
        slug: "fikes",
        video: "G80fGyhwjrk"
    },
]

const orgMembers = [
    { id: 1, nama: "Affan Aminda Kapriadi", role: "Ketua", img: "/Ketua - Affan Aminda Kapriadi.jpg" },
    { id: 2, nama: "Elvira Harum", role: "Wakil Ketua", img: "/Wakil Ketua - Elvira Harum.jpg" },
    { id: 3, nama: "Iqbal Tri Putra", role: "Kepala Departemen Keuangan", img: "/Kepala Departemen Keuangan - Iqbal Tri Putra.jpg" },
    { id: 4, nama: "Christina Raindana", role: "Staff Departemen Keuangan", img: "/Staff Dept. Keuangan - Christina Raindana.jpg" },
    { id: 5, nama: "Venicia Eflin", role: "Staff Departemen Keuangan", img: "/Staff Dept. Keuangan - Venicia Eflin.jpg" },
    { id: 6, nama: "Tania Sekar Asmara", role: "Kepala Departemen Sosial", img: "/Kepala Departemen Sosial Masyarakat - Tania Sekar Asmara.jpg" },
    { id: 7, nama: "Amanda", role: "Staff Departemen Sosial", img: "/Staff Dept. Sosial Masyarakat - Amanda.jpg" },
    { id: 8, nama: "Angelina Pasaribu", role: "Staff Departemen Sosial", img: "/Staff Dept. Sosial Masyarakat - Angelina Pasaribu.jpg" },
    { id: 9, nama: "M Rizki Raflyanto", role: "Staff Departemen Sosial", img: "/Staff Dept. Sosial Masyarakat - M. Rizki Raflyanto.jpg" },
    { id: 10, nama: "Sonaria Tambunan", role: "Ketua Departemen Kestari", img: "/Kepala Departemen Kestari - Sonaria Tambunan.jpg" },
    { id: 11, nama: "Puti Samira Salsabila", role: "Staff Departemen Kestari", img: "/Staff Dept. Kestari - Puti Samira Salsabila.jpg" },
    { id: 12, nama: "Andre Oktadian", role: "Ketua Departemen Kajian Informasi Mahasiswa", img: "/Kepala Departemen Kajian Informasi Mahasiswa - Andre Oktadian.jpg" },
    { id: 13, nama: "Fitri Laila", role: "Staff Departemen Kajian Informasi Mahasiswa", img: "/Staff Dept. Kajian Informasi Mahasiswa - Fitri Laila.jpg" },
    { id: 14, nama: "Fitri Nadya", role: "Staff Departemen Kajian Informasi Mahasiswa", img: "/Staff Dept. Kajian Informasi Mahasiswa - Fitri Nadya.jpg" },
    { id: 15, nama: "Rizqiyani Fadillah", role: "Departemen Hubungan Diplomasi Eksternal", img: "/Kepala Departemen Hubungan Diplomasi Eksternal - Rizqiyani Fadillah.jpg" },
    { id: 16, nama: "Noveni Putri Dewanti", role: "Departemen Hubungan Diplomasi Eksternal", img: "/Staff Dept. Hubungan Diplomasi Eksternal - Noveni Putri Dewanti.jpg" },
    { id: 17, nama: "Uli Shalatiya", role: "Departemen Hubungan Diplomasi Eksternal", img: "/Staff Dept. Hubungan Diplomasi Eksternal - Uli shalatiya.jpg" },
    { id: 18, nama: "Putri Herliyana Sari", role: "Departemen Hubungan Diplomasi Eksternal", img: "/Staff Dept. Hubungan Diplomasi Eskternal - Putri Herliyana Sari.jpg" },
    { id: 19, nama: "Alifatul Aulia Sagita Putri", role: "Departemen Aparatur Pemberdayaan Mahasiswa", img: "/Kepala Departemen Aparatur Pemberdayaan Mahasiswa - Alifatul Aulia Sagita Putri.jpg" },
    { id: 20, nama: "Jihan Fakhira", role: "Departemen Aparatur Pemberdayaan Mahasiswa", img: "/Staff Dept. Aparatur Pemberdayaan Mahasiswa - Jihan Fakhira.jpg" },
    { id: 21, nama: "Roi Piter Serhalawan", role: "Departemen Aparatur Pemberdayaan Mahasiswa", img: "/staff Dept. Aparatur Pemberdayaan Mahasiswa - Roi Piter Serhalawan.jpg" },
    { id: 22, nama: "Syaida Nur Sapitri", role: "Departemen Aparatur Pemberdayaan Mahasiswa", img: "/Staff Dept. Aparatur Pemerdayaan Mahasiswaan - Syaida Nur Sapitri.jpg" },
]

const Fikes = () => {
    return (
        <>
            <AppBEMF customString={importantProps[0].slug} blockCaption={importantProps[0].dept} blockSubCaption={importantProps[0].kabinet} mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} videoId={importantProps[0].video} />
        </>
    )
}
 
export default Fikes;