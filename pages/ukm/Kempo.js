import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "Shorinji Kempo", 
        desc: "Unit Kegiatan Mahasiswa Shorinji Kempo merupakan salah satu organisasi kemahasiswaan di Universitas Esa Unggul. Organisasi ini adalah organisasi yang bergerak dalam bidang olahraga nasional yaitu bela diri kempo. UKM Shorinji Kempo bertekad membangun, membina dan mengembangkan bela diri kempo di lingkungan universitas Esa Unggul dan sekitarnya. Sebagai sebuah organisasi maka perlu adanya sebuah visi, misi dan program kerja yang strategis agar terciptanya tujuan organisasi.",
        logo: "/LOGO_UKM_KEMPO_ESA_UNGGUL.jpg"
    },
]

const orgMembers = [
    { id: 1, nama: "Leonardo Tan", role: "Ketua UKM", img: "/Ketua_-_Leonardo_Tan.jpg" },
    { id: 2, nama: "Rizky Sanjaya", role: "Wakil Ketua UKM", img: "/Wakil_Ketua_-_Rizky_Sanjaya.jpg" },
    { id: 3, nama: "Rina Setia Wardani", role: "Sekretaris UKM", img: "/Sekretaris_-_Rina_Setia_Wardani.jpg" },
    { id: 4, nama: "Adji Mutiara", role: "Bendahara UKM", img: "/Bendahara_-_Adji_Mutiara.jpg" },
    { id: 5, nama: "Muhammad Idzul Adha", role: "Penanggung Jawab Sekretariat 1", img: "/Penanggung_Jawab_Sekretariat_1_-_Muhammad_Idzul_Adha.jpg" },
    { id: 7, nama: "Maria Natalia Putri", role: "Penanggung Jawab Sekretariat 2", img: "/Penanggung_Jawab_Sekretariat_2_-_Maria_Natalia_Putri.jpg" },
    { id: 8, nama: "Yohanes Pakpahan", role: "Dewan Pengawas Dan Pertimbangan", img: "/Yohanes_Pakpahan_-_Dewan_Pengawas_Dan_Pertimbangan.jpg" },
    { id: 9, nama: "Giovanni Arissa Putri", role: "Humas", img: "/Humas_-_Giovanni_Arissa_Putri.jpg" },
]

const Kempo = () => {
    return (
        <>
            <AppUKM customString="Kempo" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default Kempo;