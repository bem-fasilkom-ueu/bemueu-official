import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "Futsal", 
        desc: "Description Info To Be Updated...",
    },
]

const orgMembers = [
    { id: 1, nama: "Richard Kogoya", role: "Ketua UKM", img: "/Ketua_-_Marlon_Richard_Kogoya.jpeg" },
    { id: 2, nama: "Aldy Sambonu", role: "Wakil Ketua UKM", img: "/Wakil_Ketua_-_Aldy_Sambonu.jpeg" },
    { id: 3, nama: "Yunus Rahman", role: "Sekretaris UKM", img: "/Yunus_Rahman_-_Sekretaris.jpeg" },
    { id: 4, nama: "Dika Aji", role: "Bendahara UKM", img: "/Bendahara_-_Dika_Aji.jpeg" },
    { id: 5, nama: "Oktavianus", role: "Pelatih", img: "/Pelatih_-_Oktavianus.jpeg" },
    { id: 6, nama: "Feby", role: "Bidang Kebaktian", img: "/BidangKebaktian-Feby.jpg" },
    { id: 7, nama: "Adivi Gaoz", role: "Anggota", img: "/Anggota_-_Adivi_Gaoz.png" },
    { id: 8, nama: "Aldo", role: "Anggota", img: "/Anggota_-_Aldo.png" },
    { id: 9, nama: "Hanif", role: "Anggota", img: "/Anggota_-_Hanif.png" },
    { id: 10, nama: "Marqis", role: "Anggota", img: "/Anggota_-_Marqis.png" },
    { id: 11, nama: "Ramadhana", role: "Anggota", img: "/Anggota_-_Ramadhana.png" },
    { id: 12, nama: "Ridwan", role: "Anggota", img: "/Anggota_-_Ridwan.png" },
    { id: 13, nama: "Rizki Fauzi", role: "Anggota", img: "/Anggota_-_Rizki_Fauzi.png" },
    { id: 14, nama: "Sulis", role: "Anggota", img: "/Anggota_-_Sulis.png" },
    { id: 15, nama: "Vela Aditya", role: "Anggota", img: "/Anggota_-_Vela_Aditya.png" },
]

const Futsal = () => {
    return (
        <>
            <AppUKM customString="Futsal" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default Futsal;