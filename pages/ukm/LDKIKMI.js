import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "Lembaga Dakwah Kampus Ikatan Keluarga Muslim Indonesia (LDK IKMI)", 
        desc: "Didirikan tanggal 8 Oktober 1995. berkedudukan di Kampus Universitas Esa Unggul, yang merupakan sebuah unit kegiatan mahasiswa di bawah BEMU (Badan Eksekutif Mahasiswa Universitas). Unit Kegiatan Mahasiswa  ini didirikan dari adanya kebersamaan dalam mensyiarkan islam dilingkungan kampus yang heterogen, maka beberapa civitas akademika bersama mahasiswa muslim berisiniatif membentuk sebuah wadah organisasi keislaman di Universitas Esa Unggul. Organisasi ini mempunyai visi mewujudkan masyarakat kampus yang berdikari, bersahabat, dan bermanfaat menuju civitas madani. Dan sebagai  bentuk komitmen dan langkah konkret kami , kami menjalankan misi yaitu diantaranya Membentuk muslim yang berakidah kuat ,taat beribadah.serta menjadikan Universitas Esa Unggul menjadi pusat keilmuan islam. Dengan mengajak kepada perbuatan ma’ruf dan mencegah dari perbuatan munkar. Selanjutnya Mengembangkan kemampuan sumber daya manusia dalam mengelola ilmu pengetahuan dan teknologi serta menjadikan Alquran dan As-Sunnah sebagai pedoman dasar dalam menyelesaikan masalah.",
        logo: "/LOGO_IKMI.png"
    },
]

const orgMembers = [
    { rotate: "transform rotate-0", id: 1, nama: "Muhammad Ichsan", role: "Ketua UKM", img: "/Ketua_Umum_- _Muhammad_Ichsan.jpg" },
    { rotate: "transform rotate-0", id: 2, nama: "Alifia Amanda Glesia", role: "Sekretaris 1 UKM", img: "/Sekretaris_1_-_Alifia_Amanda_Glesia.png" },
    { rotate: "transform rotate-0", id: 3, nama: "Nurul Izzah Ryani", role: "Sekretaris 2 UKM", img: "/Sekretaris_2_-_Nurul_Izzah_Ryani.jpg" },
    { rotate: "transform rotate-0", id: 4, nama: "Muhammad Farras Rahman", role: "Sekretaris Jenderal UKM", img: "/Sekretaris_Jendral_-_Muhammad_Farras_Rahman.jpg" },
    { rotate: "transform rotate-0", id: 5, nama: "Novita Sari", role: "Bendahara 1 UKM", img: "/Bendahara_1_-_Novita_Sari.jpeg" },
    { rotate: "transform rotate-0", id: 6, nama: "Ika Malikatul", role: "Bendahara 2 UKM", img: "/Bendahara_2_-_Ika_Malikatul.jpg" },
    { rotate: "transform rotate-0", id: 7, nama: "Suryadirja Lazuardi", role: "Divisi Humas", img: "/Humas_-_Suryadirja_Lazuardi.jpg" },
    { rotate: "transform rotate-0", id: 8, nama: "Adam Ismail", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Adam_Ismail.jpg" },
    { rotate: "transform rotate-0", id: 9, nama: "Ade Arianto", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Ade_Arianto.jpg" },
    { rotate: "transform rotate-0", id: 10, nama: "Anindita Kania", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Anindita_Kania.jpg" },
    { rotate: "transform rotate-0", id: 11, nama: "Djuhardi Hakim", role: "Divisi Kaderisasi", img: "/Divisi_Kaderisasi_-_Djuhardi_Hakim.jpg" },
    { rotate: "transform rotate-0", id: 12, nama: "Fadli Alatas", role: "Divisi Kaderisasi", img: "/Divisi_Kaderisasi_-_Fadli_Alatas.jpg" },
    { rotate: "transform rotate-0", id: 13, nama: "Firna Rahma Dewi", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Firna_Rahma_Dewi.jpg" },
    { rotate: "transform rotate-0", id: 14, nama: "Irmawati Ningsih", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Irmawati_Ningsih.jpg" },
    { rotate: "transform rotate-0", id: 15, nama: "Lies Masithoh", role: "Divisi Kaderisasi", img: "/Divisi_Kaderisasi_-_Lies_Masithoh.jpg" },
    { rotate: "transform rotate-0", id: 16, nama: "M. Ariadika Al-Kautsar", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_M_Ariadika_Al-Kautsar.jpg" },
    { rotate: "transform rotate-0", id: 17, nama: "Neng Sopiatunnisa", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Neng_Sopiatunnisa.jpg" },
    { rotate: "transform rotate-0", id: 18, nama: "Putri Azahra S", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Putri_Azahra_S.jpg" },
    { rotate: "transform rotate-0", id: 19, nama: "Putri Nuraini Sutomo", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Putri_Nuraini_Sutomo.jpg" },
    { rotate: "transform rotate-0", id: 20, nama: "Ridho Ari I", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Ridho_Ari_I.jpg" },
    { rotate: "transform rotate-0", id: 21, nama: "Siti Hardiyanti Ismi", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Siti_Hardiyanti_ismi.jpg" },
    { rotate: "transform -rotate-90 mb-12", id: 22, nama: "Zulfa Nadhifa Millatina", role: "Divisi Kaderisasi", img: "/divisi_kaderisasi_-_Zulfa_Nadhifa_Millatina.jpg" },
    { rotate: "transform rotate-90 mb-12", id: 23, nama: "Dea Anggita", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Dea_Anggita.jpg" },
    { rotate: "transform rotate-0", id: 24, nama: "Devi Soleha", role: "Divisi Dana Usaha", img: "/Divisi_Dana_Usaha_-_Devi_Soleha.JPG" },
    { rotate: "transform rotate-0", id: 25, nama: "Ditho Darmawan", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Ditho_Darmawan.jpg" },
    { rotate: "transform rotate-0", id: 26, nama: "Farah Zafirah Putri", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Farah_Zafirah_Putri.jpg" },
    { rotate: "transform rotate-0", id: 27, nama: "Ika Lutfiayu", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Ika_Lutfiayu.jpg" },
    { rotate: "transform rotate-0", id: 28, nama: "Kaya Muddin Ritonga", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Kaya_Muddin_Ritonga.jpg" },
    { rotate: "transform rotate-0", id: 29, nama: "Khusnul Khotimah", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Khusnul_Khotimah.jpg" },
    { rotate: "transform rotate-0", id: 30, nama: "Prika Unari", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Prika_Unari.jpg" },
    { rotate: "transform rotate-0", id: 31, nama: "Ridwan Ibnu K", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Ridwan_Ibnu_K.jpg" },
    { rotate: "transform rotate-0", id: 32, nama: "Rina Febriatin", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Rina_Febriatin.jpg" },
    { rotate: "transform rotate-90 mb-12", id: 33, nama: "Sastya Nur Rosyidah", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Sastya_Nur_Rosyidah.jpg" },
    { rotate: "transform rotate-0", id: 34, nama: "Sunarni", role: "Divisi Dana Usaha", img: "/divisi_dana_usaha_-_Sunarni.jpg" },
    { rotate: "transform rotate-0", id: 35, nama: "Fauzan Habibi Rahman", role: "Divisi Media Jaringan", img: "/divisi_media_jaringan_-_Fauzan_Habibi_Rahman.jpg" },
    { rotate: "transform rotate-0", id: 36, nama: "Khalid Fadlilah", role: "Divisi Media Jaringan", img: "/divisi_media_jaringan_-_Khalid_Fadlilah.jpg" },
    { rotate: "transform rotate-0", id: 37, nama: "Labib Warjih", role: "Divisi Media Jaringan", img: "/divisi_media_jaringan_-_Labib_Warjih.jpg" },
    { rotate: "transform rotate-0", id: 38, nama: "Miftania Putri", role: "Divisi Media Jaringan", img: "/divisi_media_jaringan_-_Miftania_Putri.jpg" },
    { rotate: "transform rotate-90 mb-12", id: 39, nama: "Muhammad Rayhan Bachtiar", role: "Divisi Media Jaringan", img: "/divisi_media_jaringan_-_Muhammad_Rayhan_Bachtiar.jpg" },
    { rotate: "transform rotate-0", id: 40, nama: "Octaria Ananda", role: "Divisi Media Jaringan", img: "/divisi_media_jaringan_-_Octaria_Ananda.jpg" },
    { rotate: "transform rotate-0", id: 41, nama: "Atik Rizkiyani", role: "Divisi Kemuslimahan", img: "/divisi_kemuslimahan_-_Atik_Rizkiyani.jpg" },
    { rotate: "transform -rotate-90 mb-12", id: 42, nama: "Chiara Diantha Amanda", role: "Divisi Kemuslimahan", img: "/divisi_kemuslimahan_-_Chiara_Diantha_Amanda.jpg" },
    { rotate: "transform rotate-0", id: 43, nama: "Femi Dwi Kurniasih", role: "Divisi Kemuslimahan", img: "/Divisi_Kemuslimahan_-_Femi_Dwi_Kurniasih.jpg" },
    { rotate: "transform rotate-0", id: 44, nama: "Gledys Oksecargra Heryadiani", role: "Divisi Kemuslimahan", img: "/divisi_kemuslimahan_-_Gledys_Oksecargra_Heryadiani.jpg" },
    { rotate: "transform rotate-0", id: 45, nama: "Hania Sofia", role: "Divisi Kemuslimahan", img: "/divisi_kemuslimahan_-_Hania_Sofia.jpg" },
    { rotate: "transform rotate-0", id: 46, nama: "Luthfi Nur Hanifah", role: "Divisi Kemuslimahan", img: "/Divisi_Kemuslimahan_-_Luthfi_Nur_Hanifah.jpg" },
    { rotate: "transform rotate-0", id: 47, nama: "Nurul Alma Luthfiah", role: "Divisi Kemuslimahan", img: "/divisi_kemuslimahan_-_Nurul_Alma_Luthfiah.jpg" },
    { rotate: "transform rotate-0", id: 48, nama: "Nurul lailatul Fajriah", role: "Divisi Kemuslimahan", img: "/divisi_kemuslimahan_-_Nurul_Lailatul_Fajriah.jpg" },
    { rotate: "transform rotate-0", id: 49, nama: "Renny Budi", role: "Divisi Kemuslimahan", img: "/divisi_kemuslimahan_-_Renny_Budi.jpg" },
    { rotate: "transform -rotate-90 mb-12", id: 50, nama: "Saskia Arivana", role: "Divisi Kemuslimahan", img: "/divisi_kemuslimahan_-_Saskia_Arivana.jpg" },
    { rotate: "transform rotate-0", id: 51, nama: "Ummi Thajanoor Rabiah", role: "Divisi Kemuslimahan", img: "/divisi_kemuslimahan_-_Ummi_Thajanoor_Rabiah.jpg" },
    { rotate: "transform rotate-0", id: 52, nama: "Choirin Nisa", role: "Divisi Kemuslimahan", img: "/divisi_kemuslimahan_-_Choirin_Nisa.jpg" },
    { rotate: "transform -rotate-90 mb-12", id: 53, nama: "Jannah", role: "Divisi Syiar Pendidikan Islam", img: "/divisi_syiar_pendidikan_islam _ -_Jannah.jpg" },
    { rotate: "transform rotate-0", id: 54, nama: "Fatkur Ridho", role: "Divisi Syiar Pendidikan Islam", img: "/Divisi_Syiar_Pendidikan_Islam_-_Fatkur_Ridho.jpg" },
    { rotate: "transform -rotate-90 mb-12", id: 55, nama: "Nurhalisa Anggraini", role: "Divisi Syiar Pendidikan Islam", img: "/divisi_syiar_pendidikan_islam_-_Nurhalisa_Anggraini.jpg" },
    { rotate: "transform rotate-0", id: 56, nama: "Putri Aliya", role: "Divisi Syiar Pendidikan Islam", img: "/divisi_syiar_pendidikan_islam_-_Putri_Aliya.jpg" },
    { rotate: "transform -rotate-90 mb-12", id: 57, nama: "Widya Istikomah", role: "Divisi Syiar Pendidikan Islam", img: "/divisi_syiar_pendidikan_islam_-_Widya_Istikomah.jpg" },
    { rotate: "transform rotate-0", id: 58, nama: "Afif  Awaludin", role: "Divisi Syiar Pendidikan Islam", img: "/Syiar_Pendidikan_Islam_-_Afif _Awaludin.jpg" },
    { rotate: "transform rotate-0", id: 59, nama: "Abdurrahaman Grahadi Maulana", role: "Divisi Sosial", img: "/Divisi_Sosial_-_Abdurrahaman_Grahadi_Maulana.png" },
    { rotate: "transform rotate-0", id: 60, nama: "Aditya Rizky", role: "Divisi Sosial", img: "/Divisi_sosial_-_Aditya_Rizky.jpg" },
    { rotate: "transform rotate-90 mb-12", id: 61, nama: "Anggie Hartaputra", role: "Divisi Sosial", img: "/Divisi_sosial_-_Anggie_Hartaputra.jpg" },
    { rotate: "transform rotate-0", id: 62, nama: "Dimas Ridho Irvansah", role: "Divisi Sosial", img: "/Divisi_Sosial_-_Dimas_Ridho_Irvansah.png" },
    { rotate: "transform rotate-0", id: 63, nama: "Eko Wahyu", role: "Divisi Sosial", img: "/Divisi_Sosial_-_Eko_Wahyu.jpg" },
    { rotate: "transform rotate-0", id: 64, nama: "Ela Hayati", role: "Divisi Sosial", img: "/Divisi_sosial_-_Ela_Hayati.jpg" },
    { rotate: "transform rotate-0", id: 65, nama: "Fadillah Listanto", role: "Divisi Sosial", img: "/Divisi_sosial_-_Fadillah_Listanto.jpg" },
    { rotate: "transform rotate-0", id: 66, nama: "Fathinatul Husnah", role: "Divisi Sosial", img: "/Divisi_sosial_-_Fathinatul_Husnah.jpg" },
    { rotate: "transform rotate-0", id: 67, nama: "Hana Wahyuti", role: "Divisi Sosial", img: "/divisi_sosial_-_Hana_Wahyuti.jpg" },
    { rotate: "transform rotate-0", id: 68, nama: "Meishe Agatha Vikcyyati", role: "Divisi Sosial", img: "/divisi_sosial_-_Meishe_Agatha_Vikcyyati.jpg" },
    { rotate: "transform rotate-0", id: 69, nama: "Ni Putu Ayu Darma", role: "Divisi Sosial", img: "/divisi_sosial_-_Ni_Putu_Ayu_Darma.jpg" },
    { rotate: "transform rotate-0", id: 70, nama: "Ramitha Dewi", role: "Divisi Sosial", img: "/divisi_sosial_-_Ramitha_Dewi.jpg" },
    { rotate: "transform rotate-0", id: 71, nama: "Ridwan Hanif", role: "Divisi Sosial", img: "/divisi_sosial_-_Ridwan_Hanif.jpg" },
    { rotate: "transform rotate-0", id: 72, nama: "Riyanti Lestari", role: "Divisi Sosial", img: "/divisi_sosial_-_Riyanti_Lestari.jpg" },
    { rotate: "transform rotate-0", id: 73, nama: "Robiah Al Adawiyah", role: "Divisi Sosial", img: "/divisi_sosial_-_Robiah_Al_Adawiyah.jpg" },
    { rotate: "transform rotate-90 mb-12", id: 74, nama: "Septia Kusherawati", role: "Divisi Sosial", img: "/divisi_sosial_-_Septia_Kusherawati.jpg" },
    { rotate: "transform rotate-0", id: 75, nama: "Shinta Dewi Condrowati", role: "Divisi Sosial", img: "/divisi_sosial_-_Shinta_Dewi_Condrowati.jpg" },
    { rotate: "transform rotate-0", id: 76, nama: "Siti Julaeha", role: "Divisi Sosial", img: "/Divisi_sosial_-_Siti_Julaeha.jpg" },
    { rotate: "transform rotate-0", id: 77, nama: "Widya Kusuma Rahayu", role: "Divisi Sosial", img: "/divisi_sosial_-_Widya_Kusuma_Rahayu.jpg" },
    { rotate: "transform rotate-0", id: 78, nama: "Widya Rovita", role: "Divisi Sosial", img: "/divisi_sosial_-_Widya_Rovita.jpg" },
]

const LDKIKMI = () => {
    return (
        <>
            <AppUKM customString="ldkikmi" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default LDKIKMI;