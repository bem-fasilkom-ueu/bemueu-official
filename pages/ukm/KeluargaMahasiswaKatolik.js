import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "Keluarga Mahasiswa Katolik", 
        desc: "Description Info To Be Updated...",
    },
]

const orgMembers = [
    { id: 1, nama: "Faustina.M.C.L.Soro", role: "Ketua UKM", img: "/Ketua_-_Faustina.M.C.L.Soro.jpg" },
    { id: 2, nama: "Fransiska.S.Sinaga", role: "Wakil Ketua UKM 1", img: "/Wakil_Ketua_1_-_Fransiska.S.Sinaga.jpg" },
    { id: 3, nama: "Cristofer.E.P.P", role: "Wakil Ketua UKM 2", img: "/Wakil_Ketua_2_-_Cristofer.E.P.P.jpg" },
    { id: 4, nama: "To be updated...", role: "Sekretaris UKM" },
    { id: 5, nama: "Maria.G.Meko", role: "Bendahara UKM", img: "/Bendahara_-_Maria.G.Meko.jpg" },
    { id: 6, nama: "Maria Delatitis.F", role: "Divisi Humas Internal", img: "/Divisi_Humas_Internal_-_Maria_Delatitis.F.jpg" },
    { id: 7, nama: "Johan Leonardo", role: "Koordinasi Divisi Humas Internal", img: "/Koordinasi_Divisi_Humas_Internal_-_Johan_Leonardo.jpg" },
    { id: 8, nama: "Ceryn Virginia.W", role: "Divisi Humas Eksternal", img: "/Divisi_Humas_Eksternal_-_Ceryn_Virginia.W.jpg" },
    { id: 9, nama: "Margaretha Rindi.W", role: "Koordinasi Divisi Humas Eksternal", img: "/Koordinasi_Divisi_Humas_Eksternal_-_Margaretha_Rindi.W.jpg" },
    { id: 10, nama: "Felisitas.S.Soda", role: "Koordinasi Divisi Dana dan Usaha", img: "/Koordinasi_Divisi_Dana_dan_Usaha_-_Felisitas.S.Soda.jpg" },
    { id: 11, nama: "Amelia.E", role: "Divisi Acara", img: "/Divisi_Acara_-_Amelia.E.jpg" },
    { id: 12, nama: "Maria.C.M", role: "Divisi Acara", img: "/Divisi_Acara_-_Maria.C.M.jpg" },
    { id: 13, nama: "Teodora Sari", role: "Koordinasi Divisi Acara", img: "/Koordinasi_Divisi_Acara_-_Teodora Sari.jpg" },
    { id: 14, nama: "Silbiano.G.S.D", role: "Divisi PSDM", img: "/Divisi_PSDM_-_Silbiano.G.S.D.jpg" },
    { id: 15, nama: "Mikhael.H.L", role: "Koordinasi Divisi PSDM", img: "/Koordinasi_Divisi_PSDM_-_Mikhael.H.L.jpg" },
]

const KeluargaMahasiswaKatolik = () => {
    return (
        <>
            <AppUKM customString="KMK" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default KeluargaMahasiswaKatolik;