import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "KMB Vajra Buddhis Indonusa", 
        desc: "Keluarga Mahasiswa Buddhis Vajra Buddhis Indonusa (KMB VBI) merupakan Unit Kegiatan Mahasiswa Esa Unggul yang bergerak pada bidang kerohanian, yaitu Agama Buddha. Sesuai dengan Namanya, kami berfokus pada kekeluargaan, sehingga kami saling merangkul satu sama lain tanpa memandang perbedaan. KMB VBI sudah berdiri sejak 27 September 1966 dan bertempat di gedung PKM (Pusat Kegiatan Mahasiswa) lantai 2 Universitas Esa Unggul Kebon jeruk, KMB VBI memiliki misi untuk berusaha mengembangkan dan melestarikan ajaran Buddha di lingkungan Universitas Esa Unggul.",
        logo: "/Logo_VBI.png",
    },
]

const orgMembers = [
    { id: 1, nama: "To be updated...", role: "Ketua UKM" },
    { id: 2, nama: "To be updated...", role: "Wakil Ketua UKM" },
    { id: 3, nama: "To be updated...", role: "Sekretaris UKM" },
    { id: 4, nama: "To be updated...", role: "Bendahara UKM" },
]

const KMBVajraBuddhisIndonusa = () => {
    return (
        <>
            <AppUKM customString="kmb vbi" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default KMBVajraBuddhisIndonusa;