import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "Korps Sukarela Palang Merah Indonesia (KSR-PMI)", 
        desc: "Unit Kegiatan Mahasiswa ini bernama Korps Sukarela Palang Merah Indonesia Unit Universitas Esa Unggul dan disingkat KSR PMI Unit Universitas Esa Unggul. KSR PMI Unit Universitas Esa Unggul bertempat di Universitas Esa Unggul dan berkedudukan di Jakarta Barat dibawah pembinaan Palang Merah Indonesia Kota Jakarta Barat dan berada dibawah Pelindung Ketua Pengurus Palang Merah Indonesia Kota Jakarta Barat dan Rektor Universitas Esa Unggul. Serta berada pada pembinaan Ka. Sie SDM Relawan PMI Kota Jakarta Barat.",
    },
]

const orgMembers = [
    { id: 1, nama: "Sekar Lady Arvilla", role: "Ketua UKM", img: "/Ketua_-_Sekar_Lady_Arvilla.png" },
    { id: 2, nama: "Ni Putu Ayu Darmayanti", role: "Sekretaris UKM", img: "/Sekretaris_-_Ni_Putu_Ayu_Darmayanti.png" },
    { id: 3, nama: "Ni Putu Ayu Darmayanti", role: "Bendahara UKM", img: "/Sekretaris_-_Ni_Putu_Ayu_Darmayanti.png" },
    { id: 4, nama: "Amalia Nur Azizah", role: "Dewan Pembina", img: "/Dewan_Pembina_-_Amalia_Nur_Azizah.jpg" },
    { id: 5, nama: "Annisa Nurul Latifah", role: "Dewan Pembina", img: "/Dewan_Pembina_-_Annisa_Nurul_Latifah.jpeg" },
    { id: 6, nama: "Iqrila", role: "Dewan Pembina", img: "/Dewan_Pembina_-_Iqrila.jpeg" },
    { id: 7, nama: "Kezia Irene Joseph", role: "Dewan Pembina", img: "/Dewan_Pembina_-_Kezia_Irene_Joseph.jpeg" },
    { id: 8, nama: "Puspita Dyah Ismoyowati", role: "Dewan Pembina", img: "/Dewan_Pembina_-_Puspita_Dyah_Ismoyowati.png" },
    { id: 9, nama: "Febry Fadhlan", role: "Divisi Kominfo", img: "/Divisi_Kominfo_-_Febry_Fadhlan.jpg" },
    { id: 10, nama: "Fitri Sumiyati", role: "Divisi Logistik", img: "/Divisi_Logistik_-_Fitri_Sumiyati.jpeg" },
    { id: 11, nama: "Khansa Muthia Nabila", role: "Divisi Pengabdian Masyarakat", img: "/Divisi_Pengabdian_Masyarakat_-_Khansa_Muthia_Nabila.png" },
    { id: 12, nama: "Yemima Picella Wulandari", role: "Divisi Pengabdian Masyarakat", img: "/Divisi_Pengabdian_Masyarakat_-_Yemima_Picella_Wulandari.png" },
    { id: 13, nama: "Jesisca Endah Artati Prasetyo", role: "Anggota Muda", img: "/Anggota_Muda_-_Jesisca_Endah_Artati_Prasetyo.png" },
    { id: 14, nama: "Putri Sukma Wardhani", role: "Anggota Muda", img: "/Anggota_Muda_-_Putri_Sukma_Wardhani.jpeg" },
    { id: 15, nama: "Rizka Aulia Putri", role: "Anggota Muda", img: "/Anggota_Muda_-_Rizka_Aulia_Putri.jpg" },
    { id: 16, nama: "Rohaniva Yusnia Sari", role: "Anggota Muda", img: "/Anggota_Muda_-_Rohaniva_Yusnia_Sari.jpeg" },
    { id: 17, nama: "Tri Handayani", role: "Anggota Muda", img: "/Anggota_Muda_-_Tri_Handayani.jpeg" },
    { id: 18, nama: "Ulfatul Hasanah", role: "Anggota Muda", img: "/Anggota_Muda_-_Ulfatul_Hasanah.jpg" },
]

const KSRPMI = () => {
    return (
        <>
            <AppUKM customString="ksrpmi" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default KSRPMI;