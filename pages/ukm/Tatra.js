import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "Tari Tradisional (Tatra)", 
        desc: "UKM Tatra dibentuk oleh Departemen Seni dan Budaya BEM UEU yang bekerjasama dengan mahasiswa fakultas fisioterapi dan mulai aktif pada 21 Maret 2015 sampai sekarang. Sesuai dengan GBHKO yang berlaku sebelum di sahkan dinamakan pra-UKM Tatra. Anggota pra-UKM Tatra berjumlah 96 anggota. Di dalam pra-UKM Tatra ada 2 kekhususan tari yang di pelajari yaitu tari ratoh jaroe dan tari kreasi lainnya.",
    },
]

const orgMembers = [
    { id: 1, nama: "Salsabillah Zahrah Hayati", role: "Ketua Umum", img: "/Ketua_Umum_-_Salsabillah_Zahrah_Hayati.jpg" },
    { id: 2, nama: "Nazwa Nurfadilah", role: "Ketua Tari Kreasi", img: "/Ketua_Tari_Kreasi_-_Nazwa_Nurfadilah.jpg" },
    { id: 3, nama: "Farah Zafirah Putri", role: "Ketua Tari Ratoh Jaroe", img: "/Ketua_Ratoh_Jaroe_-_Farah_Zafirah_Putri.jpeg" },
    { id: 4, nama: "Metta Shesylia", role: "Sekretaris Tari Kreasi", img: "/Sekretaris_-_Metta-Shesylia.jpg" },
    { id: 5, nama: "Roro Anggun Wijayanti", role: "Sekretaris Tari Ratoh Jaroe", img: "/Sekretaris_-_Roro_Anggun_Wijayanti.jpeg" },
    { id: 6, nama: "Aliyya Citra Tsamara", role: "Bendahara Tari Kreasi", img: "/Bendahara_-_Aliyya_Citra_Tsamara.jpg" },
    { id: 7, nama: "Deasy Prihati Sabariyanti ", role: "Bendahara Tari Ratoh Jaroe", img: "/Bendahara_-_Deasy_Prihati_Sabariyanti .jpeg" },
    { id: 8, nama: "Alinsyiroh", role: "Anggota Tari Kreasi", img: "/Anggota_- _Alinsyiroh.jpeg" },
    { id: 9, nama: "Aneswariendita", role: "Anggota Tari Kreasi", img: "/Anggota_- _Aneswariendita.jpg" },
    { id: 10, nama: "Aryani", role: "Anggota Tari Kreasi", img: "/Anggota_- _Aryani.png" },
    { id: 11, nama: "Harefa", role: "Anggota Tari Kreasi", img: "/Anggota_- _Harefa.jpg" },
    { id: 12, nama: "Lia Alawiah", role: "Anggota Tari Kreasi", img: "/Anggota_-_Lia_Alawiah.jpg" },
    { id: 13, nama: "Oktaviani Waruwu", role: "Anggota Tari Kreasi", img: "/Anggota_- _Oktaviani_Waruwu.jpg" },
    { id: 14, nama: "Anggi Amalia", role: "Anggota Tari Kreasi", img: "/Anggota_-_Anggi_Amalia.jpg" },
    { id: 15, nama: "Annisa Indah Amalia", role: "Anggota Tari Kreasi", img: "/Anggota_-_Annisa_Indah_Amalia.jpg" },
    { id: 16, nama: "Erika Febia Alviani Putri", role: "Anggota Tari Kreasi", img: "/Anggota_-_Erika_Febia_Alviani_Putri.jpg" },
    { id: 17, nama: "Ira Marshella", role: "Anggota Tari Kreasi", img: "/Anggota_-_Ira_Marshella.jpg" },
    { id: 18, nama: "Lisa Anggun Magdalena Gea", role: "Anggota Tari Kreasi", img: "/Anggota_-_Lisa_Anggun_Magdalena_Gea.jpg" },
    { id: 19, nama: "Martauli", role: "Anggota Tari Kreasi", img: "/Anggota_-_Martauli.jpg" },
    { id: 20, nama: "Nur Fatihah Adlia R", role: "Anggota Tari Kreasi", img: "/Anggota_-_Nur_Fatihah_Adlia_R.jpg" },
    { id: 21, nama: "Olivia Parlan", role: "Anggota Tari Kreasi", img: "/Anggota_-_Olivia_Parlan.jpg" },
    { id: 22, nama: "Putri Firna Julianti", role: "Anggota Tari Kreasi", img: "/Anggota_-_Putri_Firna_Julianti.jpg" },
    { id: 23, nama: "Putu Divya", role: "Anggota Tari Kreasi", img: "/Anggota_-_Putu_Divya.jpg" },
    { id: 24, nama: "Rahayu Putri", role: "Anggota Tari Kreasi", img: "/Anggota_-_Rahayu_Putri.jpg" },
    { id: 25, nama: "Ratu Dini Rahmawati", role: "Anggota Tari Kreasi", img: "/Anggota_-_Ratu_Dini_Rahmawati.jpg" },
    { id: 26, nama: "Ratu Mardillah", role: "Anggota Tari Kreasi", img: "/Anggota_-_Ratu_Mardillah.png" },
    { id: 27, nama: "Santiana", role: "Anggota Tari Kreasi", img: "/Anggota_-_Santiana.jpg" },
    { id: 28, nama: "Septia", role: "Anggota Tari Kreasi", img: "/Anggota_-_Septia.jpg" },
    { id: 29, nama: "Siti Aminah", role: "Anggota Tari Kreasi", img: "/Anggota_-_Siti_Aminah.jpeg" },
    { id: 30, nama: "Siti Hafizahtul Zalilah", role: "Anggota Tari Kreasi", img: "/Anggota_-_Siti_Hafizahtul_Zalilah.jpg" },
    { id: 31, nama: "Sri Bunga Ningrum", role: "Anggota Tari Kreasi", img: "/Anggota_-_Sri_Bunga_Ningrum.jpg" },
    { id: 32, nama: "Sunarni", role: "Anggota Tari Kreasi", img: "/Anggota_-_Sunarni.jpeg" },
    { id: 33, nama: "Vivin Tri Yuliani Nugroho", role: "Anggota Tari Kreasi", img: "/Anggota_-_Vivin_Tri_Yuliani_Nugroho.jpeg" },
    { id: 34, nama: "Yuni Mely Sapitri", role: "Anggota Tari Kreasi", img: "/Anggota_-_Yuni_Mely_Sapitri.jpg" },
    { id: 35, nama: "Adini Fasa Salzabila", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_- _Adini_Fasa_Salzabila.jpeg" },
    { id: 36, nama: "Anggi Amalia Putri", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_- _Anggi_Amalia_Putri.jpeg" },
    { id: 37, nama: "Arynta Disastra", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_- _Arynta_Disastra.jpeg" },
    { id: 38, nama: "Azizah", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Azizah.jpeg" },
    { id: 39, nama: "Fayza Maghfira Solachudin", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Fayza_Maghfira_Solachudin.jpeg" },
    { id: 40, nama: "Feby Permata Annisa", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Feby_Permata_Annisa.jpeg" },
    { id: 41, nama: "Gina Lestari", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Gina_Lestari.jpeg" },
    { id: 42, nama: "Hana Wahyuti", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Hana_Wahyuti.jpeg" },
    { id: 43, nama: "Jelita Kurnia Putri", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Jelita_Kurnia_Putri.jpeg" },
    { id: 44, nama: "Lintang Eka Valent", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Lintang_Eka_Valent.jpeg" },
    { id: 45, nama: "Mawaddatul Rahmah", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Mawaddatul_Rahmah.jpeg" },
    { id: 46, nama: "Nabila", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Nabila.jpeg" },
    { id: 47, nama: "Nabila Siti Khaliza", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Nabila_Siti_Khaliza.jpeg" },
    { id: 48, nama: "Nur Alisa Salsabila", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Nur_Alisa_Salsabila.jpeg" },
    { id: 49, nama: "Nurul Fajriah Haerunnisa", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Nurul_Fajriah_Haerunnisa.jpeg" },
    { id: 50, nama: "Zhafirah Rahmatina", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Zhafirah_Rahmatina.jpeg" },
    { id: 51, nama: "Adelia Dias Permata", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Adelia_Dias_Permata.jpeg" },
    { id: 52, nama: "Alda Gista Refita", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Alda_Gista_Refita.jpeg" },
    { id: 53, nama: "Deva Ghita Anggraini", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Deva_Ghita_Anggraini.jpeg" },
    { id: 54, nama: "Dewi Ayuningsih", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Dewi_Ayuningsih.jpg" },
    { id: 55, nama: "Erieke Aningtyas", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Erieke_Aningtyas.jpeg" },
    { id: 56, nama: "Esa Maulidya Putri", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Esa_Maulidya_Putri.jpeg" },
    { id: 57, nama: "Mitha Andriani Rahman", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Mitha_Andriani_Rahman.jpeg" },
    { id: 58, nama: "Nurul Febryana", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Nurul_Febryana.jpeg" },
    { id: 59, nama: "Reanida Fatonah", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Reanida_Fatonah.jpeg" },
    { id: 60, nama: "Regi Melati Fauziah", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Regi_Melati_Fauziah.jpeg" },
    { id: 61, nama: "Risna Widiyanti", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Risna_Widiyanti.jpeg" },
    { id: 62, nama: "Riyanti Lestari", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Riyanti_Lestari.jpeg" },
    { id: 63, nama: "Siti Annisa Hartati", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Siti_Annisa_Hartati.jpeg" },
    { id: 64, nama: "Sunarni", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Sunarni.jpeg" },
    { id: 65, nama: "Thasya Rohadatul Aisy", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Thasya_Rohadatul_Aisy.jpeg" },
    { id: 66, nama: "Vinska Adellia Desfitri", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Vinska_Adellia_Desfitri.jpeg" },
    { id: 67, nama: "Widia Fitaloca", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Widia_Fitaloca.jpeg" },
    { id: 68, nama: "Yustrie Nabila Harsu", role: "Anggota Tari Ratoh Jaroe", img: "/Anggota_-_Yustrie_Nabila_Harsu.jpeg" },
]

const Tatra = () => {
    return (
        <>
            <AppUKM customString="tatra" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default Tatra;