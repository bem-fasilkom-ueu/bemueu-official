import AppAnimatedBackgroundBlankLong from "../../components/AppAnimatedBackgroundBlankLong";
import AppNavbar  from "../../components/AppNavbar";
import Head from "next/head";

const kementerianArray = [
    { id: 1, img: "/Logo_VBI.png", title: "KMB Vajra Buddhis Indonusa", link: "/ukm/KMBVajraBuddhisIndonusa", desc: "To be updated..." },
    { id: 2, img: "/header_logo kabinet.png", title: "OMG Kristen", link: "/ukm/OMGKristen", desc: "To be updated..." },
    { id: 3, img: "/header_logo kabinet.png", title: "HIMPALA", link: "/ukm/Himpala", desc: "To be updated..." },
    { id: 4, img: "/logo_UKM_Karate.jpeg", title: "Karate", link: "/ukm/Karate", desc: "To be updated..." },
    { id: 5, img: "/header_logo kabinet.png", title: "Futsal", link: "/ukm/Futsal", desc: "To be updated..." },
    { id: 6, img: "/header_logo kabinet.png", title: "Golden Dance Campus (GDC)", link: "/ukm/GoldenDanceCampus", desc: "To be updated..." },
    { id: 7, img: "/header_logo kabinet.png", title: "Voli", link: "/ukm/Voli", desc: "To be updated..." },
    { id: 8, img: "/header_logo kabinet.png", title: "Korps Sukarela Palang Merah Indonesia (KSR-PMI)", link: "/ukm/KSRPMI", desc: "To be updated..." },
    { id: 9, img: "/header_logo kabinet.png", title: "Keluarga Mahasiswa Katolik", link: "/ukm/KeluargaMahasiswaKatolik", desc: "To be updated..." },
    { id: 10, img: "/LOGO_IKMI.png", title: "LDK IKMI", link: "/ukm/LDKIKMI", desc: "To be updated..." },
    { id: 11, img: "/LOGO_UKM_KEMPO_ESA_UNGGUL.jpg", title: "Kempo", link: "/ukm/Kempo", desc: "To be updated..." },
    { id: 12, img: "/header_logo kabinet.png", title: "Badminton", link: "/ukm/Badminton", desc: "To be updated..." },
    { id: 13, img: "/header_logo kabinet.png", title: "Band", link: "/ukm/Band", desc: "To be updated..." },
    { id: 14, img: "/header_logo kabinet.png", title: "Tari Tradisional", link: "/ukm/Tatra", desc: "To be updated..." },
    { id: 15, img: "/header_logo kabinet.png", title: "Teater", link: "/ukm/Teater", desc: "To be updated..." },
    { id: 16, img: "/header_logo kabinet.png", title: "Basket", link: "/ukm/Basket", desc: "To be updated..." },
]

const index = () => {
    return (
        <>  
            <Head>
                <title>Unit Kegiatan Mahasiswa - BEMUEU Official</title>
            </Head>
            <AppNavbar currentActive="ukm" />
            <AppAnimatedBackgroundBlankLong sourceArray={kementerianArray} blockCaption="unit kegiatan mahasiswa" blockSubCaption="description info to be updated ..."/>
        </>
    )
}
 
export default index;