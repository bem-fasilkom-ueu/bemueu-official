import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "Badminton", 
        desc: "Description Info To Be Updated...",
    },
]

const orgMembers = [
    { id: 1, nama: "To Be Updated Later...", role: "Ketua UKM" },
    { id: 1, nama: "To Be Updated Later", role: "Wakil Ketua UKM" },
    { id: 2, nama: "To Be Updated Later", role: "Sekretaris UKM" },
    { id: 4, nama: "To Be Updated Later", role: "Bendahara UKM" },
]

const Badminton = () => {
    return (
        <>
            <AppUKM customString="Badminton" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default Badminton;