import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "Band", 
        desc: "UKM Band dibentuk oleh Departemen Seni dan Budaya BEM UEU yang bekerjasama dengan Universitas Esa Unggul dan mulai aktif pada 21 November 2005 sampai sekarang. UKM Band tersebut merupakan salah satu forum untuk mahasiswa Esa Unggul mengembangkan minat dan bakat pada ruang lingkup musik. Pada Tahun 2018 UKM Band Universitas Esa Unggul  bekerja sama dengan Label Music Netrilis untuk merilis sebuah lagu yang diciptakan oleh Farrell Alberik dengan judul Metropole diberbagai Platforms Music Digital seperti Spotify, Apple Music, iTunes, dan Platforms Music Digital lainnya.",
    },
]

const orgMembers = [
    { id: 1, nama: "Ahmad Fauzi", role: "Role Info To Be Updated", img: "/Ahmad_Fauzi_20201002143.jpeg" },
    { id: 1, nama: "Anggara Machesa Putra", role: "Role Info To Be Updated", img: "/Anggara_Machesa_Putra_20200502022.jpg" },
    { id: 1, nama: "Arhan Rachmadan", role: "Role Info To Be Updated", img: "/Arhan_Rachmadan_20200102137.jpeg" },
    { id: 1, nama: "Ario Sadewo", role: "Role Info To Be Updated", img: "/Ario_Sadewo_20200606066.jpeg" },
    { id: 1, nama: "Arisman Jamili", role: "Role Info To Be Updated", img: "/Arisman_Jamili_20200801220.jpeg" },
    { id: 1, nama: "Bimabima AF", role: "Role Info To Be Updated", img: "/Bimabima_AF_20200503048.png" },
    { id: 1, nama: "Dimas Adi Wijaya", role: "Role Info To Be Updated", img: "/Dimas_Adi_Wijaya_20200201024.jpeg" },
    { id: 1, nama: "Dymas Albrian", role: "Role Info To Be Updated", img: "/Dymas_Albrian_20200502110.jpeg" },
    { id: 1, nama: "Elberd Jetrianos", role: "Role Info To Be Updated", img: "/Elberd_Jetrianos_20200501020.jpeg" },
    { id: 1, nama: "Elisabeth Lasmaida Pakpahan", role: "Role Info To Be Updated", img: "/Elisabeth_Lasmaida_Pakpahan_20180302081.jpeg" },
    { id: 1, nama: "Elvaret", role: "Role Info To Be Updated", img: "/Elvaret_20200801037.jpeg" },
    { id: 1, nama: "Farrell Alberik Pasifikus", role: "Role Info To Be Updated", img: "/Farrell_Alberik_Pasifikus_20180502091.jpeg" },
    { id: 1, nama: "Febrianty Melinda Artauli Manihuruk", role: "Role Info To Be Updated", img: "/Febrianty_Melinda_Artauli_Manihuruk_20201102020.jpeg" },
    { id: 1, nama: "Gading Pratiwi", role: "Role Info To Be Updated", img: "/Gading_Pratiwi_20200701094.jpeg" },
    { id: 1, nama: "Intan Hutabarat", role: "Role Info To Be Updated", img: "/Intan_Hutabarat_20190301100.jpeg" },
    { id: 1, nama: "Jena Putra Loreno Sitompul", role: "Role Info To Be Updated", img: "/Jena_Putra_Loreno_Sitompul_20190801254.jpeg" },
    { id: 1, nama: "Jessicca Inggrit Giovanni", role: "Role Info To Be Updated", img: "/Jessicca_Inggrit_Giovanni_20200311073.jpeg" },
    { id: 1, nama: "Junior Waworuntu", role: "Role Info To Be Updated", img: "/Junior_Waworuntu_20180606069.jpeg" },
    { id: 1, nama: "Keysha Adelia Irawan", role: "Role Info To Be Updated", img: "/Keysha_Adelia_Irawan_20200502079.jpeg" },
    { id: 1, nama: "Laurent Hari Adijati", role: "Role Info To Be Updated", img: "/Laurent_Hari_Adijati_20190306051.jpeg" },
    { id: 1, nama: "Mario Putra Pratama", role: "Role Info To Be Updated", img: "/Mario_Putra_Pratama_20190101005.jpeg" },
    { id: 1, nama: "Mochammad Arya Fathurrahman", role: "Role Info To Be Updated", img: "/Mochammad_Arya_Fathurrahman_20200502055.jpeg" },
    { id: 1, nama: "Muhammad Budi Kharisma", role: "Role Info To Be Updated", img: "/Muhammad_Budi_Kharisma_20201002136.jpeg" },
    { id: 1, nama: "Mutiara Nur Alamsyah Atjamalia", role: "Role Info To Be Updated", img: "/Mutiara_Nur_Alamsyah_Atjamalia_20200803071.jpeg" },
    { id: 1, nama: "Nadya Nadezhda", role: "Role Info To Be Updated", img: "/Nadya_Nadezhda_20200508078.jpeg" },
    { id: 1, nama: "Nathasya Fitriyani", role: "Role Info To Be Updated", img: "/Nathasya_Fitriyani_20201102024.jpeg" },
    { id: 1, nama: "Nur Alysa Qaulan Karima", role: "Role Info To Be Updated", img: "/Nur_Alysa_Qaulan_Karima_20200502015.jpeg" },
    { id: 1, nama: "Okky Jaunal Ghurfata", role: "Role Info To Be Updated", img: "/Okky_Jaunal_Ghurfata_20200301020.jpeg" },
    { id: 1, nama: "Priest Chandra", role: "Role Info To Be Updated", img: "/Priest_Chandra_20200508035.jpeg" },
    { id: 1, nama: "Rezanti Patricia Haryanti Putri", role: "Role Info To Be Updated", img: "/Rezanti_Patricia_Haryanti_Putri_20200311046.jpeg" },
    { id: 1, nama: "Rio Raynaldo", role: "Role Info To Be Updated", img: "/Rio_Raynaldo_20200701215.jpeg" },
    { id: 1, nama: "Rolyando Lio Suhendro", role: "Role Info To Be Updated", img: "/Rolyando_Lio_Suhendro_20200801225.jpeg" },
    { id: 1, nama: "Sabella Alya Fathanisa", role: "Role Info To Be Updated", img: "/Sabella_Alya_Fathanisa_20200501067.jpeg" },
    { id: 1, nama: "Salma Aura Inayah", role: "Role Info To Be Updated", img: "/Salma_Aura_Inayah_20200502083.jpeg" },
    { id: 1, nama: "Widia Fitaloca", role: "Role Info To Be Updated", img: "/Widia_Fitaloca_20200301052.jpeg" },
]

const Band = () => {
    return (
        <>
            <AppUKM customString="Band" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default Band;