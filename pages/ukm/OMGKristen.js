import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "OMG Kristen", 
        desc: "Description Info To Be Updated...",
    },
]

const orgMembers = [
    { id: 1, nama: "Thesalonica Chrystanti Isa", role: "Ketua UKM", img: "/Ketua-ThesalonicaChrystantiIsa.jpg" },
    { id: 2, nama: "Yemima Wulandari", role: "Sekretaris UKM", img: "/Sekretaris-YemimaWulandari.jpg" },
    { id: 3, nama: "Amida Morin", role: "Sekretaris UKM", img: "/Sekretaris-AmidaMorin.jpg" },
    { id: 4, nama: "Debby Carolina", role: "Bendahara UKM", img: "/Bendahara-DebbyCarolina.jpg" },
    { id: 5, nama: "Alfaizah Permadi", role: "Bidang Pembinaan", img: "/BidangPembinaan-AlfaizahPermadi.jpg" },
    { id: 6, nama: "Feby", role: "Bidang Kebaktian", img: "/BidangKebaktian-Feby.jpg" },
    { id: 7, nama: "Whelmy Meyok", role: "Bidang Kebaktian", img: "/BidangKebaktian-WhelmyMeyok.jpg" },
]

const OMGKristen = () => {
    return (
        <>
            <AppUKM customString="omg kristen" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default OMGKristen;