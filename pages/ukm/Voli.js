import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "Voli", 
        desc: "Unit kegiatan mahasiswa ( UKM ) Voli universitas Esa Unggul merupakan UKM olahraga para mahasiswa yang mampu menyalurkan aspirasi dan menyatukan seluruh potensi untuk menyalurkan bentuk kreatifitas yang ada dalam mahasiswa yang bersifat mengharumkan nama dan menjunjung tinggi nama baik Universitas di dunia luar dengan melalui Unit kegiatan mahasiswa ( UKM ) bola voli. Adanya UKM voli ini dapat memotivasi dan semangat UKM voli universitas esa unggul. Dengan mengembangkan minat maupun bakat dalam olahraga serta memiliki rasa tanggung jawab pada diri sendiri dalam berorganisasi. UKM voli universitas Esa Unggul  juga mempunyai agenda latihan rutin setiap hari Senin, Selasa dan Kamis pukul 15.00 – selesai di lapangan voli universitas esa unggul, Kebon jeruk Jakarta.",
    },
]

const orgMembers = [
    { id: 1, nama: "Mayang Cristianty Sairmaly", role: "Ketua UKM", img: "/Ketua_-_MAYANG_CRISTIANTY_SAIRMALY.jpeg" },
    { id: 2, nama: "Rifqi Desfriansyah", role: "Wakil Ketua UKM", img: "/Wakil_Ketua_-_RIFQI_DESFRIANSYAH.jpeg" },
    { id: 3, nama: "Anjelie Kartika Putri", role: "Sekretaris UKM", img: "/Sekretaris_-_Anjelie_Kartika_Putri.jpeg" },
    { id: 4, nama: "Icha Kiki Anggraini", role: "Sekretaris UKM", img: "/Sekretaris_-_Icha_Kiki_Anggraini.jpeg" },
    { id: 5, nama: "Wanda Hamidah", role: "Bendahara UKM", img: "/Bendahara_-_Wanda_Hamidah.jpeg" },
    { id: 6, nama: "Muhamad Sadam", role: "Koordinator Lapangan", img: "/Koordinator_Lapangan_-_Muhamad_Sadam.jpeg" },
    { id: 7, nama: "Cindy Mayrella", role: "Humas", img: "/Humas_-_Cindy_Mayrella.jpeg" },
    { id: 8, nama: "Teodora Reski Kurnia Sary", role: "Humas", img: "/Humas_-_Teodora_Reski_Kurnia_Sary.jpeg" },
    { id: 9, nama: "William", role: "Humas", img: "/Humas_-_William.jpeg" },
]

const Voli = () => {
    return (
        <>
            <AppUKM customString="voli" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default Voli;