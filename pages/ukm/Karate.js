import AppUKM  from "../../components/AppUKM";


const importantProps = [
    { 
        dept: "Karate", 
        desc: "Karate Esa Unggul dulu disebut Teknik Karate Club (TKC) di bawah naungan BEM Fakultas Teknik. Bersifat Dojo Wadokai pada saat tahun 2004. Pada saat pertama didirikan oleh kang Asep dari Fakultas Teknik, menjadi anggota DPMU periode pertama dan pelatih karate Senpai Aswan. Pada tahun 2003-2004 kang Asep mendorong pra UKM dengan background Wadokai. Selanjutnya pra UKM Karate akhirnya resmi berdiri menjadi UKM Karate pada tahun 2006 diketuai oleh Siti dari Fakultas Teknik Industri. Latihan karate dilaksanakan pada hari selasa dan kamis pukul 19.00 WIB. Latihan karate biasa dilakukan di depan perpustakaan ( Gedung Baru ). Pada hari selasa karate melaksanakan latihan basic, dan hari kamis melaksanakan latihan fisik dan komite.",
        logo: "/logo_UKM_Karate.jpeg"
    },
]

const orgMembers = [
    { id: 1, nama: "To Be Updated Later...", role: "Ketua UKM" },
    { id: 1, nama: "To Be Updated Later", role: "Wakil Ketua UKM" },
    { id: 2, nama: "To Be Updated Later", role: "Sekretaris UKM" },
    { id: 4, nama: "To Be Updated Later", role: "Bendahara UKM" },
]

const Karate = () => {
    return (
        <>
            <AppUKM customString="Karate" blockCaption={importantProps[0].dept} blockSubCaption="2020/2021" mainLogo={importantProps[0].logo} dummyDescription={importantProps[0].desc} orgMembers={orgMembers} />
        </>
    )
}
 
export default Karate;