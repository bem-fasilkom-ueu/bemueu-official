const { default: AppNavbar } = require("../../components/AppNavbar")
import { useState, useEffect } from "react";
import emailjs from "emailjs-com"
import Head from "next/head";
import dynamic from 'next/dynamic'

const AppSVGSchool = dynamic(() => import('../../components/AppSVGSchool'))

const index = () => {

    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [category, setCategory] = useState("")
    const [message, setMessage] = useState("")
    const [validationError, setValidationError] = useState(false)
    const [sentStatus, setSentStatus] = useState(false)

    var dataCurahan = {
        to_name: name,
        from_email: email,
        category: category,
        message: message,
    }

    const sendMessage = () => {
        emailjs.send(process.env.NEXT_PUBLIC_SERVICE_ID, process.env.NEXT_PUBLIC_TEMPLATE_ID, dataCurahan, process.env.NEXT_PUBLIC_USER_ID)
            .then((response) => {
                alert(`Curahan Telah Kami Terima, Terimakasih Telah Menghubungi Kami Melalui Form Ini !`);
                resetForm();
            }, (err) => {
                console.log('EmailJS Has Failed To Continue The Process. Reason : ', err);
                alert(`Curahan Gagal Terkirim !`);
        });
    }

    const resetForm = () => {
        setName("");
        setEmail("");
        setMessage("");
    }

    const formSubmit = (event) => {
        event.preventDefault();
        
        if (!event.target.checkValidity()) {
            setValidationError(true);
            return;
        }

        sendMessage();
    }

    return (
        <>
            <Head>
                <title>Curahan Sodara - BEMUEU Official</title>
            </Head>
            <AppNavbar currentActive="curahan sodara" />
            <div className="hidden xl:block flex-1 green-base-background overflow-hidden relative AppAnimatedBackground-50 mt-14">
                <AppSVGSchool />
                <div className="absolute bg-black w-full h-full top-0 bg-opacity-70 pt-20">
                    <div className="container mx-auto">
                        <img className="mx-auto" src="/Logo Curahan Sodara-01.png" width="550" />
                        <p className="text-lg text-white text-center font-regular rounded-xl my-16 w-4/6 mx-auto">Curahan sodara adalah sebuah platform dari BEM Universitas Esa Unggul untuk menampung aspirasi, keluh kesah, dan juga kritik serta saran dari seluruh masyarakat Universitas Esa Unggul tentang kehidupan kampus maupun permasalahan pribadi Sodara. Silakan curahkan semuanya di bawah.</p>
                    </div>
                    <div className="container mx-auto">
                        <div className="green-base-background rounded-xl shadow-2xl">
                            <form id="form_desktop" className="p-10 flex-1" onSubmit={formSubmit} noValidate>
                                <div className="flex-1 my-5">
                                    <label htmlFor="to_name_desktop" className="w-full font-black mb-3">Nama</label>
                                    <input type="text" name="to_name_desktop" id="to_name_desktop" className="w-full rounded-xl focus:shadow-xl focus:outline-none p-3" required autoComplete="off" onChange={(e) => setName(e.target.value)} value={name} />
                                </div>
                                <div className="flex-1 my-5">
                                    <label htmlFor="from_email_desktop" className="w-full font-black mb-3">Email</label>
                                    <input type="email" name="from_email_desktop" id="from_email_desktop" className="w-full rounded-xl focus:shadow-xl focus:outline-none p-3" required autoComplete="off" onChange={(e) => setEmail(e.target.value)} value={email} />
                                </div>
                                <div className="flex-1 my-5">
                                    <label htmlFor="category_desktop" className="w-full font-black mb-3">Tentang Apa ?</label>
                                    <select name="category_desktop" className="bg-white w-full rounded-xl focus:shadow-xl focus:outline-none p-3" required autoComplete="off" onChange={(e) => setCategory(e.target.value)} value={category}>
                                        <option>--Pilih Topik--</option>
                                        <option>Perkuliahan</option>
                                        <option>Sarana dan Prasarana</option>
                                        <option>Mental</option>
                                        <option>Keuangan</option>
                                    </select>
                                </div>
                                <div className="flex-1 my-5">
                                    <label htmlFor="message_desktop" className="w-full font-black mb-3">Coba Diungkapkan</label>
                                    <textarea type="textarea" name="message_desktop" id="message_desktop" rows="9" className="w-full rounded-xl focus:shadow-xl focus:outline-none p-3" required autoComplete="off" onChange={(e) => setMessage(e.target.value)} value={message} />
                                </div>
                                <div className="flex-1 my-5" onClick={() => setValidationError(false)}>
                                    <p className={"bg-gray-500 green-base-color p-5 font-black rounded-xl " + (validationError ? "block" : "hidden")}>Mohon lengkapi form diatas dengan data yang sesuai.</p>
                                </div>

                                <button id="button_desktop" className="my-5 p-5 bg-white green-base-color rounded-xl font-black focus:shadow-xl focus:bg-gray-500 focus:outline-none">Unggah Curahan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div className="block xl:hidden green-base-background">
                <div className="container mx-auto bg-black bg-opacity-70 h-full pt-28 pb-1">
                    <img className="mx-auto" src="/Logo Curahan Sodara-01.png" width="250" />
                    <p className="text-md text-white text-center font-regular rounded-xl my-10 w-11/12 mx-auto">Curahan sodara adalah sebuah platform dari BEM Universitas Esa Unggul untuk menampung aspirasi, keluh kesah, dan juga kritik serta saran dari seluruh masyarakat Universitas Esa Unggul tentang kehidupan kampus maupun permasalahan pribadi Sodara. Silakan curahkan semuanya di bawah.</p>
                </div>
                <div className="container mx-auto">
                    <div className="green-base-background rounded-xl">
                        <form id="form_mobile" className="px-10 py-5 flex-1" onSubmit={formSubmit} noValidate>
                            <div className="flex-1 my-5">
                                <label htmlFor="to_name_mobile" className="w-full font-black mb-3">Nama</label>
                                <input type="text" name="to_name_mobile" id="to_name_mobile" className="w-full rounded-xl focus:shadow-xl focus:outline-none p-3" required autoComplete="off" onChange={(e) => setName(e.target.value)} value={name} />
                            </div>
                            <div className="flex-1 my-5">
                                <label htmlFor="from_email_mobile" className="w-full font-black mb-3">Email</label>
                                <input type="email" name="from_email_mobile" id="from_email_mobile" className="w-full rounded-xl focus:shadow-xl focus:outline-none p-3" required autoComplete="off" onChange={(e) => setEmail(e.target.value)} value={email} />
                            </div>
                            <div className="flex-1 my-5">
                                <label htmlFor="category_desktop" className="w-full font-black mb-3">Tentang Apa ?</label>
                                <select name="category_desktop" className="bg-white w-full rounded-xl focus:shadow-xl focus:outline-none p-3" required autoComplete="off" onChange={(e) => setCategory(e.target.value)} value={category}>
                                    <option>--Pilih Topik--</option>
                                    <option>Perkuliahan</option>
                                    <option>Sarana dan Prasarana</option>
                                    <option>Mental</option>
                                    <option>Keuangan</option>
                                </select>
                            </div>
                            <div className="flex-1 my-5">
                                <label htmlFor="message_mobile" className="w-full font-black mb-3">Coba Diungkapkan</label>
                                <textarea type="textarea" name="message_mobile" id="message_mobile" rows="9" className="w-full rounded-xl focus:shadow-xl focus:outline-none p-3" required autoComplete="off" onChange={(e) => setMessage(e.target.value)} value={message} />
                            </div>
                            <div className="flex-1 my-5" onClick={() => setValidationError(false)}>
                                <p className={"bg-gray-500 green-base-color p-5 font-black rounded-xl " + (validationError ? "block" : "hidden")}>Mohon lengkapi form diatas dengan data yang sesuai.</p>
                            </div>

                            <button id="button_mobile" className="my-5 p-5 bg-white green-base-color rounded-xl font-black focus:shadow-xl focus:bg-gray-500 focus:outline-none">Unggah Curahan</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}
 
export default index;