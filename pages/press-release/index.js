import { getSortedPostsData } from '../../lib/posts'
import AppNavbar from '../../components/AppNavbar'
import Link from 'next/link'
import Date from '../../components/AppDate'
import { useState, useEffect } from 'react'
import Image from 'next/image'
import Head from 'next/head'

export async function getStaticProps() {

    const allPostsData = getSortedPostsData()
    
    return {
        props: {
            allPostsData
        }
    }
}

const index = ({allPostsData}) => {

    const [currentAuthor, setCurrentAuthor] = useState(null)
    const [authors, setAuthors] = useState([])
    const [firstLoad, setFirstLoad] = useState(true)
    const [searchKeyword, setSearchKeyword] = useState("")
    const authorsList = allPostsData.map((item) => item.author).filter((v, i, a) => a.indexOf(v) === i)
    const filterPosts = (refString) => {
        if (firstLoad) {
            setFirstLoad(false)
            setAuthors(allPostsData)
        } else {
            if (refString == "") {
                setAuthors(allPostsData)
            } else {
                const thisShit = allPostsData.filter((x) => x.author == refString)
                setAuthors(thisShit)
            }
        }
    }

    useEffect(() => {
        filterPosts(currentAuthor)
    }, [currentAuthor])

    return (
        <>
            <Head>
                <title>Press Release - BEMUEU Official</title>
            </Head>
            <AppNavbar currentActive="press release" />
            <div className="container mx-auto py-5 px-3 xl:px-0 grid flex-1 flex-row lg:grid-cols-12 mt-20">
                <div className="flex-1 order-2 lg:col-span-9 lg:order-1 mr-0 lg:mr-6">
                    {
                        authors.filter((x) => x.title.toLowerCase().includes(searchKeyword.toLowerCase())).map(post => (
                            <div className="mb-5 relative shadow AppBlog-thumbnail" key={post.id}>
                                <Image
                                    src={post.thumbnail}
                                    alt={post.thumbnail}
                                    layout="fill"
                                    objectFit="cover"
                                />
                                <div className="absolute bottom-0 p-5 bg-white text-left w-full">
                                    <Link href={`/posts/${post.id}`}>
                                        <a className="text-xl xl:text-3xl">{post.title}</a>
                                    </Link>
                                    <div className="mt-2">Oleh <strong>{post.author}</strong></div>
                                    <Date dateString={post.date} />
                                </div>        
                            </div>
                        ))
                    }
                </div>
                <div className="flex-1 order-1 lg:col-span-3 lg:order-2 p-3 border mb-3 lg:mb-0 bg-white blog-filter">
                    <div className="my-3">
                        <div className="mb-2">
                            <label htmlFor="keyword-search" className="text-left">Cari Menurut Judul</label>
                            <input type="text" name="keyword-search" id="keyword-search" className="w-full focus:shadow-xl focus:outline-none p-1 bg-gray-100" autoComplete="off" onChange={(e) => setSearchKeyword(e.target.value)} value={searchKeyword} />
                        </div>
                    </div>
                    <div className="my-3">
                        <div className="mb-2">
                            <label htmlFor="author-filter" className="text-left">Filter Menurut Kementerian</label>
                        </div>
                        <select name="authorFilter" id="author-filter" className="w-full focus:outline-none p-1 bg-gray-100" onChange={(e) => setCurrentAuthor(e.target.value)}>
                            <option></option>
                            {
                                authorsList.map((item, index) => (
                                    <option value={item} key={index}>{item}</option>
                                ))
                            }
                        </select>
                    </div>
                </div>
            </div>
        </>
    )
}
 
export default index;