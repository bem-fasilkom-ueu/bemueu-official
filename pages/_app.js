import '../styles/index.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faArrowUp, faBars, faTimes } from '@fortawesome/free-solid-svg-icons'
import AppScrollTop from '../components/AppScrollTop'
import AppHead from '../components/AppHead'
import AppFooter from '../components/AppFooter'

library.add(faArrowUp, faBars, faTimes)

function MyApp({ Component, pageProps }) {
  return (
    <>
      <AppHead />
      <Component {...pageProps} />
      <AppScrollTop />
      <AppFooter />
    </>
  )
}

export default MyApp
