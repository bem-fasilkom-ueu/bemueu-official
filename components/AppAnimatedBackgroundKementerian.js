import gsap from "gsap";
import { useEffect } from "react";
import dynamic from 'next/dynamic'
import Image from 'next/image';

const AppSVGSchool = dynamic(() => import('./AppSVGSchool'))

const AppAnimatedBackgroundKementerian = ({customString, repeatValue, noText, sourceArray, blockCaption, blockSubCaption, mainLogo, membersPicture, membersGallery, dummyDescription}) => {

    const defaultRepeat = repeatValue || 5
    const defaultString = customString || "lorem ipsum"
    const timeline = gsap.timeline({repeat: -1, repeatDelay: 0})

    useEffect(() => {
        if(noText == false) {
            Array.apply(null, {length : defaultRepeat}).map((x,i) => (
                timeline
                .set(".customString-" + i, {color: "rgba(255,255,255,1)", ease: "steps(1)"}, "<")
                .set(".customString-" + i, {color: "rgba(255,255,255,0)", ease: "steps(1)"}, "+=.5")
            ))
        }
    })

    return (
        <>
            <div className="hidden xl:block flex-1 green-base-background overflow-hidden relative AppAnimatedBackground-100">
                <AppSVGSchool />
                <div className="flex-1 absolute AppAnimatedBackground-parent-customString">
                {
                    noText == false ? 
                    Array.apply(null, {length : defaultRepeat}).map((x,i) =>(
                        <div className={"uppercase font-black text-18xl text-transparent text-center AppAnimatedBackground-stroke-customString whitespace-nowrap customString-" + i} key={i}>{defaultString}</div>
                    ))
                    : <></>
                }
                </div>
                <div className="absolute bg-black w-full h-full top-0 bg-opacity-70 pt-20">
                    <div className="flex-1 justify-center uppercase p-3">
                        <h1 className="text-xl xl:text-5xl text-white text-center font-black rounded-xl my-10">{blockCaption}</h1>
                        <p className="text-lg xl:text-2xl text-white text-center font-black rounded-xl my-10">{blockSubCaption}</p>
                    </div>
                    <div className="flex justify-center">
                        <div className="mx-10 image-500">
                            <Image 
                                src={mainLogo}
                                alt={mainLogo}
                                width={500}
                                height={500}
                                layout="responsive"
                                objectFit="fill"
                            />
                        </div>
                        <div className="border-r-4 border-white">

                        </div>
                        <div className="mx-10 image-500">
                            <Image 
                                src={membersPicture}
                                alt={membersPicture}
                                width={500}
                                height={500}
                                layout="responsive"
                                objectFit="fill"
                            />
                        </div>
                    </div>
                    <div className="w-3/6 text-center mx-auto text-white my-10">
                        {dummyDescription}
                    </div>
                    <div className="container mx-auto">
                        <div className="grid grid-col grid-cols-1 md:grid-cols-2">
                        {
                            sourceArray.map(item => (
                                <div className={(item.role == "Menteri" ? "ml-4" : "mr-4") + "flex-1 lg:flex items-start"} key={item.img}>
                                    <div className={(item.role == "Menteri" ? "order-2" : "order-1") + " flex-1"}>
                                        <div className="mx-auto AppAnimatedBackgroundStd-membersPicture">
                                            <Image 
                                                src={item.img}
                                                alt={item.img}
                                                width={300}
                                                height={362.34939759}
                                                layout="responsive"
                                                objectFit="fill"
                                            />
                                        </div>
                                    </div>
                                    <div className={(item.role == "Menteri" ? "order-1 text-left" : "order-2 text-right") + " flex-1 text-white mt-10"}>
                                        <h1 className="font-black text-5xl my-3">
                                            <strong>{item.nama}</strong>
                                        </h1>
                                        <p className="font-black text-xl">{item.role}</p>
                                        <p>{item.faculty}</p>
                                        <p className="mt-3">{item.desc}</p>
                                    </div>
                                </div>
                            ))
                        }
                        </div>
                        <div className="flex justify-center mt-5">
                        {
                            membersGallery.map(item => (
                                <div className="mt-5" key={item.img}>
                                    <div className="mx-5 image-377_99">
                                        <Image 
                                            src={item.img}
                                            alt={item.img}
                                            width={377.99}
                                            height={377.99}
                                            layout="responsive"
                                            objectFit="fill"
                                        />
                                    </div>
                                </div>
                            ))
                        }
                        </div>
                    </div>
                </div>
            </div>
            <div className="block xl:hidden green-base-background pt-16">
                <div className="bg-black w-full h-full bg-opacity-60 p-3">
                    <div className="flex-1 justify-center uppercase p-3">
                        <h1 className="text-xl xl:text-5xl text-white text-center font-black rounded-xl my-10">{blockCaption}</h1>
                        <p className="text-lg xl:text-2xl text-white text-center font-black rounded-xl my-10">{blockSubCaption}</p>
                    </div>
                    <div className="flex-1 lg:flex flex-row">
                        <div className="mx-auto my-2">
                            <Image 
                                src={mainLogo}
                                alt={mainLogo}
                                width={500}
                                height={500}
                                layout="responsive"
                                objectFit="fill"
                            />
                        </div>
                        <div className="mx-auto my-2">
                            <Image 
                                src={membersPicture}
                                alt={membersPicture}
                                width={500}
                                height={500}
                                layout="responsive"
                                objectFit="fill"
                            />
                        </div>
                    </div>
                    <div className="w-11/12 text-center mx-auto text-white my-10">
                        {dummyDescription}
                    </div>
                    <div className="grid grid-col grid-cols-1 md:grid-cols-2">
                    {
                        sourceArray.map(item => (
                            <div className="flex-1 lg:flex items-start" key={item.img}>
                                <div className="flex-1">
                                    <div className="mx-auto AppAnimatedBackgroundStd-membersPicture">
                                        <Image 
                                            src={item.img}
                                            alt={item.img}
                                            width={300}
                                            height={362.34939759}
                                            layout="responsive"
                                            objectFit="fill"
                                        />
                                    </div>
                                </div>
                                <div className="flex-1 text-white mt-10">
                                    <h1 className="font-black text-2xl xl:text-5xl my-3">
                                        <strong>{item.nama}</strong>
                                    </h1>
                                    <p className="font-black text-xl">{item.role}</p>
                                    <p>{item.faculty}</p>
                                    <p className="mt-3">{item.desc}</p>
                                </div>
                            </div>
                        ))
                    }
                    </div>
                    <div className="grid grid-col grid-cols-1 lg:grid-cols-3 mt-5">
                    {
                        membersGallery.map(item => (
                            <div className="mt-5" key={item.img}>
                                <div className="mx-auto">
                                    <Image 
                                        src={item.img}
                                        alt={item.img}
                                        width={377.99}
                                        height={377.99}
                                        layout="responsive"
                                        objectFit="fill"
                                    />
                                </div>
                            </div>
                        ))
                    }
                    </div>
                </div>
            </div>
        </>
    )
}
 
export default AppAnimatedBackgroundKementerian;