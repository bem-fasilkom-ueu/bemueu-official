import { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const AppScrollTop = () => {

    const [showButton, setShowButton] = useState(false)
    const toggleShowButton = () => {
        if (window.scrollY > 512) {
            setShowButton(true)
        } else {
            setShowButton(false)
        }
    }
    
    useEffect(() => {
        window.addEventListener("scroll", toggleShowButton)
    })

    return (
        <>
            <div className={(showButton ? "block" : "hidden") + " AppScrollTop shadow-xl flex items-center justify-center"} onClick={() => window.scroll({top: 0, left: 0, behavior: "smooth"})}>
                <FontAwesomeIcon icon="arrow-up" className="text-white"></FontAwesomeIcon>
            </div>
        </>
    )
}
 
export default AppScrollTop;