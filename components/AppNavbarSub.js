import Link from "next/link";
import { useState } from "react";

const AppNavbarSub = ({sourceArray, href, menuText, active, passiveMode}) => {

    const [toggleMenu, setToogleMenu] = useState(false)
    const isActive = active || false
    const currentLink = passiveMode ? "" : href

    return (
        <div className="relative xl:flex xl:justify-center">
            <Link href={currentLink}>
                <a>
                    <div onMouseOver={() => setToogleMenu(true)} onTouchStart={() => setToogleMenu(x => !x)} className={"AppNavbarSub-parentMenu green-base-color relative z-30 rounded-2xl p-1 whitespace-pre hover:text-white " + (isActive ? "shadow font-black" : "")}>
                        { menuText }
                    </div>
                </a>
            </Link>
            {
                passiveMode && toggleMenu ?
                <button className={(toggleMenu ? "flex" : "hidden") + " fixed inset-0 w-full h-full z-10"} tabIndex="-1" onMouseEnter={() => setToogleMenu(false)}></button>
                : <> </>
            }
            <div className={(toggleMenu ? "flex-1" : "hidden") + " relative xl:absolute w-auto 2xl:w-full z-20 bg-gray-100 xl:bg-white xl:shadow xl:top-8"}>
                {
                    sourceArray.map(item => (
                        <div className="flex-1 uppercase p-1 text-center hover:bg-green-300 hover:text-white" key={item}>
                            <Link href={`/${menuText}/${item}`}>
                                {item}
                            </Link>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}
 
export default AppNavbarSub;