import Head from 'next/head'

const AppHead = () => {
    return (
        <>
            <Head>
                <title>BEMUEU Official</title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Website Resmi BEM Universitas Esa Unggul" />
                <meta name="robots" content="index"></meta>
                <meta name="google-site-verification" content="KSePXgiqQt6mF9vJCGE-yY2Yi3PbiihoNVhimSAIgsI" />
            </Head>
        </>
    )
}
 
export default AppHead;