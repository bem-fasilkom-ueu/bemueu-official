import AppAnimatedBackgroundKementerian from "./AppAnimatedBackgroundKementerian";
import AppNavbar  from "./AppNavbar";
import Head from "next/head";
import dynamic from 'next/dynamic';

const AppCarousel = dynamic(() => import('./AppCarousel'));
const AppTooltip = dynamic(() => import('./AppTooltip'));

const AppKementerian = ({customString, blockCaption, blockSubCaption, mainLogo, dummyDescription, mostValuableMember, membersGallery, membersPicture, allPostsData, tooltipProps, departmentFilter}) => {
    return (
        <>
            <Head>
                <title>{`${blockCaption} - BEMUEU Official`}</title>
            </Head>
            <AppNavbar currentActive="kementerian" />
            <AppAnimatedBackgroundKementerian noText={false} repeatValue={7} customString={customString} blockCaption={blockCaption} blockSubCaption={blockSubCaption} sourceArray={mostValuableMember} membersGallery={membersGallery} membersPicture={membersPicture} mainLogo={mainLogo} dummyDescription={dummyDescription} />
            <div className="my-12 text-xl xl:text-3xl font-bold green-base-color text-center">Press Release</div>
            <AppCarousel sourceArray={allPostsData.filter(post => post.author === departmentFilter).slice(0, 10)} departmentFilter={departmentFilter} />
            <div className="container mx-auto py-5">
                <div className="my-5 text-4xl xl:text-5xl font-bold text-center green-base-color">Program Kerja</div>
                <div className="grid grid-col grid-cols-1 md:grid-cols-3">
                    {
                        tooltipProps.map(item => (
                            <AppTooltip key={item.title} buttonText={item.title} tooltipText={item.desc} tooltipPosition="top" buttonCustomClass="w-full tippy-customContent rounded-xl px-5 py-12" buttonParentCustomClass="m-4 rounded-xl font-bold text-2xl text-center" isArrow={true} />
                        ))
                    }
                </div>
            </div>
        </>
    )
}
 
export default AppKementerian;