import { format } from 'date-fns'

export default function Date({ dateString }) {
    return <time className="font-black" dateTime={dateString}>{format(dateString, 'eeee, d LLLL yyyy')}</time>
}