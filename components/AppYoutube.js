const AppYoutube = ({videoId}) => {

    const fallbackId = videoId || "8tPnX7OPo0Q"

    return (
        <>
            <div className="container mx-auto my-12 xl:mt-48">
                <div className="relative AppYoutube">
                    <iframe className="absolute top-0 left-0" width="100%" height="100%" src={`https://www.youtube.com/embed/${fallbackId}`} frameBorder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                </div>
            </div>
        </>
    )
}
 
export default AppYoutube;