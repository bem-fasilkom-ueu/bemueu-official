// import gsap from "gsap";
// import { useEffect } from "react";
import Link from "next/link";
import dynamic from 'next/dynamic'

const AppSVGSchool = dynamic(() => import('./AppSVGSchool'))

const AppAnimatedBackgroundBlankLong = ({customString, repeatValue, noText, sourceArray, blockCaption, blockSubCaption}) => {

    const defaultRepeat = repeatValue || 5
    const defaultString = customString || "lorem ipsum"
    // const timeline = gsap.timeline({repeat: -1, repeatDelay: 0})

    // useEffect(() => {
    //     if(noText == false) {
    //         Array.apply(null, {length : defaultRepeat}).map((x,i) => (
    //             timeline
    //             .set(".customString-" + i, {color: "rgba(255,255,255,1)", ease: "steps(1)"}, "<")
    //             .set(".customString-" + i, {color: "rgba(255,255,255,0)", ease: "steps(1)"}, "+=.5")
    //         ))
    //     }
    // })

    return (
        <>
            <div className="hidden xl:block flex-1 green-base-background overflow-hidden relative AppAnimatedBackground-220 mt-14">
                <AppSVGSchool />
                {/* <div className="flex-1 absolute AppAnimatedBackground-parent-customString">
                {
                    noText == false ? 
                    Array.apply(null, {length : defaultRepeat}).map((x,i) =>(
                        <div className={"uppercase font-black text-18xl text-transparent text-center AppAnimatedBackground-stroke-customString whitespace-nowrap customString-" + i} key={i}>{defaultString}</div>
                    ))
                    : <></>
                }
                </div> */}
                <div className="absolute bg-black w-full h-full top-0 bg-opacity-70 pt-20">
                    <div className="container mx-auto">
                        <h1 className="uppercase text-xl xl:text-5xl text-white font-black text-center mb-5">{blockCaption}</h1>
                        <p className="text-lg xl:text-2xl text-white text-center font-regular rounded-xl mt-5 mb-10">{blockSubCaption}</p>
                        <div className="grid grid-col grid-cols-1 md:grid-cols-3">
                            {
                                sourceArray.map(item => (
                                    <div key={item.id} className="text-center p-6 bg-white rounded-xl m-5">
                                        <Link href={item.link}>
                                            <a>
                                                <img src={item.img} className="mx-auto" width="250" height="250"></img>
                                            </a>
                                        </Link>
                                        <h1 className="font-medium text-lg mt-12 mb-2">
                                            <strong>{item.title}</strong>
                                        </h1>
                                        <p>{item.desc}</p>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                </div>
            </div>
            <div className="block xl:hidden green-base-background">
                <div className="container mx-auto bg-black bg-opacity-70 h-full py-10">
                    <h1 className="uppercase text-xl xl:text-5xl text-white font-black text-center px-2 pt-12 mb-5">{blockCaption}</h1>
                    <p className="text-lg xl:text-2xl text-white text-center font-regular rounded-xl mt-5 mb-10">{blockSubCaption}</p>
                    <div className="grid grid-col grid-cols-1 md:grid-cols-2">
                        {
                            sourceArray.map(item => (
                                <div key={item.id} className="text-center p-6 bg-white rounded-xl m-5">
                                    <Link href={item.link}>
                                        <a>
                                            <img src={item.img} className="mx-auto" width="180" height="180"></img>

                                        </a>
                                    </Link>
                                    <h1 className="font-medium text-lg my-3">
                                        <strong>{item.title}</strong>
                                    </h1>
                                    <p>{item.desc}</p>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </>
    )
}
 
export default AppAnimatedBackgroundBlankLong;