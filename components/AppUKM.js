import Head from "next/head"

const { default: AppNavbar } = require("./AppNavbar")
const { default: AppAnimatedBackgroundMinimalV } = require("./AppAnimatedBackgroundMinimalV")

const AppUKM = ({customString, blockCaption, blockSubCaption, mainLogo, dummyDescription, orgMembers}) => {
    return (
        <>
            <Head>
                <title>{`${blockCaption} - BEMUEU Official`}</title>
            </Head>
            <AppNavbar currentActive="ukm" />
            <AppAnimatedBackgroundMinimalV customString={customString} blockCaption={blockCaption} blockSubCaption={blockSubCaption} mainLogo={mainLogo} dummyDescription={dummyDescription} />
            <div className="container mx-auto py-5">
                <div className="my-5 text-xl xl:text-3xl green-base-color text-center">Kepengurusan</div>
                <div className="grid grid-col grid-cols-1 md:grid-cols-4">
                    {
                        orgMembers.map(item => (
                            <div key={item.id} className="text-center p-6 bg-white">
                                {
                                    item.img ?
                                    <img src={item.img} className="mx-auto" width="180" height="180"></img>
                                    : <div className="bg-gray-500 text-white">Foto Belum Ada</div>
                                }
                                <h1 className="font-medium text-lg my-3">
                                    <strong>{item.nama}</strong>
                                </h1>
                                <p>{item.role}</p>
                            </div>  
                        ))
                    }
                </div>
            </div>
        </>
    )
}
 
export default AppUKM;