import AppAnimatedBackgroundMinimalV from "./AppAnimatedBackgroundMinimalV";
import AppNavbar  from "./AppNavbar";
import { useState } from "react";
import Head from "next/head";

const AppBEMF = ({customString, blockCaption, blockSubCaption, mainLogo, dummyDescription, orgMembers, videoId}) => {

    const [showMembers, setShowMembers] = useState(false)

    return (
        <>
            <Head>
                <title>{`${blockCaption} - BEMUEU Official`}</title>
            </Head>
            <AppNavbar currentActive="fakultas" />
            <AppAnimatedBackgroundMinimalV customString={customString} blockCaption={blockCaption} blockSubCaption={blockSubCaption} mainLogo={mainLogo} dummyDescription={dummyDescription} videoId={videoId} />
            <div className="container mx-auto py-5">
                <div className="my-5 text-xl xl:text-3xl green-base-color text-center">Kepengurusan</div>
                <div className="flex flex-col">
                    <div className="flex mx-auto">
                        {
                            orgMembers.slice(0,2).map(item => (
                                <div key={item.id} className="text-center p-6">
                                    {
                                        item.img ?
                                        <img src={item.img} className="mx-auto" width="180" height="180"></img>
                                        : <div className="bg-gray-500 text-white">Foto Belum Ada</div>
                                    }
                                    <h1 className="font-medium text-lg my-3">
                                        <strong>{item.nama}</strong>
                                    </h1>
                                    <p>{item.role}</p>
                                </div>  
                            ))
                        }
                    </div>
                </div>
                <div className={(showMembers ? "hidden" : "flex") + " my-5"}>
                    <button className="bg-black text-white rounded-lg py-1 px-3 mx-auto" onClick={() => setShowMembers(true)}>Lihat Lebih Banyak...</button>
                </div>
                <div className={(showMembers ? "grid" : "hidden") + " grid-col grid-cols-1 md:grid-cols-4"}>
                    {
                        orgMembers.slice(2, orgMembers.length).map(item => (
                            <div key={item.id} className="text-center p-6">
                                {
                                    item.img ?
                                    <img src={item.img} className="mx-auto" width="180" height="180"></img>
                                    : <div className="bg-gray-500 text-white">Foto Belum Ada</div>
                                }
                                <h1 className="font-medium text-lg my-3">
                                    <strong>{item.nama}</strong>
                                </h1>
                                <p>{item.role}</p>
                            </div>  
                        ))
                    }
                </div>
            </div>
        </>
    )
}
 
export default AppBEMF;