import Link from 'next/link'
import { useState } from 'react';
import Image from 'next/image'

const AppCarouselCard = ({postThumbnail, postTitle, postLink}) => {

    const [showTitle, setShowTitle] = useState(false)

    return (
        <>
            <li className="glide__slide">
                <div className="relative flex-1">
                    <div className="AppCarouselCard" onMouseEnter={() => setShowTitle(true)}>
                        <Image
                            src={postThumbnail}
                            alt={postThumbnail}
                            layout="fill"
                            objectFit="cover"
                        />
                    </div>
                    <Link href={postLink}>
                        <a onMouseLeave={() => setShowTitle(false)}>
                            <div className={ (showTitle ? "flex" : "hidden") + " absolute bg-black text-white w-full h-full text-center items-center justify-around top-0 p-3"}>
                                <div>{postTitle}</div>
                            </div>
                        </a>
                    </Link>
                </div>
            </li>
        </>
    )
}
 
export default AppCarouselCard;