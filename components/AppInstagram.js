import { useState } from "react";
import { parseISO } from 'date-fns'

const AppInstagram = ({postLink, postThumbnail, postTimestamp, postMediaType, postVideoThumbnail}) => {

    const [showCount, setShowCount] = useState(false)
    const currentMedia = postMediaType === "VIDEO" ? postVideoThumbnail : postThumbnail

    return (
        <div>
            <div className="m-2 relative flex-1">
                <div className="AppInstagram-card mx-auto" onMouseEnter={() => setShowCount(true)}>
                    <img src={currentMedia} alt={currentMedia} height="350.14536" width="280" className="w-full h-full object-cover" loading="lazy" decoding="async" />
                </div>
                <a href={postLink} target="_blank" rel="noreferrer noopener" onMouseLeave={() => setShowCount(false)}>
                    <div className={ (showCount ? "flex" : "hidden") + " absolute bg-black text-white w-full h-full text-center items-center justify-around top-0"}>
                        { parseISO(postTimestamp).toDateString() }
                    </div>
                </a>
            </div>
        </div>
    )
}
 
export default AppInstagram;