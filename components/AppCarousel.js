import Glide, { Controls, Breakpoints, Swipe} from "@glidejs/glide/dist/glide.modular.esm"
import { useEffect, useState, createRef } from "react"
import AppCarouselCard from './AppCarouselCard'

const defaultConfig = {
    gap: 25,
    perView: 5,
    startAt: 0,
    type: "carousel",
    breakpoints: {
        1440: {
            perView: 4
        },
        1024: {
            perView: 3
        },
        720: {
            perView: 1
        }
    }
}

const AppCarousel = ({sourceArray, departmentFilter}) => {

    const [carouselInstance] = useState(new Glide('.glide', defaultConfig))
    const carouselParent = createRef()
    const carouselControl = (refDir) => {
        let tempElement = document.createElement("button")
        tempElement.setAttribute("class", refDir == "<" ? "glide__arrow glide__arrow--left hidden xl:block" : "glide__arrow glide__arrow--right hidden xl:block")
        tempElement.setAttribute("data-glide-dir", refDir)
        tempElement.textContent = refDir

        return tempElement
    }
    
    useEffect(() => {
        carouselParent.current.appendChild(carouselControl("<"))
        carouselParent.current.appendChild(carouselControl(">"))
        carouselInstance.mount({Controls, Breakpoints, Swipe})
        carouselInstance.on('run.before', (event) => {})

        return () => carouselInstance.destroy()
    }, [])

    return (
        <>
            <div className="mb-4 py-3 px-3 xl:px-12">
                <div className='glide'>
                    <div className='glide__track w-full lg:w-11/12 mx-auto' data-glide-el='track'>
                        <ul className='glide__slides'>
                            {
                                sourceArray.length > 0 ?
                                sourceArray.map(item => (
                                    <AppCarouselCard postLink={`/posts/${item.id}`} postThumbnail={item.thumbnail} postTitle={item.title} key={item.id} />
                                ))
                                :
                                <div></div>
                            }
                        </ul>
                        <div className="glide__bullets flex justify-center py-3" data-glide-el="controls[nav]">
                            {
                                sourceArray.length > 0 ?
                                sourceArray.map((x, i) => (
                                    <button className="glide__bullet" data-glide-dir={"=" + i} key={i}></button>
                                ))
                                :
                                <div></div>
                            }
                        </div>
                    </div>
                    <div className="glide__arrows" data-glide-el="controls" ref={carouselParent}></div>
                </div>
            </div>
        </>
    )
}
 
export default AppCarousel;