import Image from 'next/image'

const footerLinks = [
    { id: 1, img: "/footer_icon_gmail.png", link: "mailto:bemuesaunggul@gmail.com"},
    { id: 2, img: "/footer_icon_instagram.png", link: "https://www.instagram.com/bemueu_official"},
    { id: 3, img: "/footer_icon_twitter.png", link: "https://www.twitter.com/bemueu_official"},
    { id: 4, img: "/footer_icon_youtube.png", link: "https://www.youtube.com/channel/UCWsFx7vb-9se5V8E0h9Q8Dg"},
]

const AppFooter = () => {
    return (
        <footer className="flex-1 black-secondary-background py-10">
            <div className="container flex-1 md:flex flex-row mx-auto">
                <div className="flex items-center text-white px-6 lg:px-4 xl:px-0 px-auto text-center lg:text-left flex-col lg:flex-row">
                    <div className="relative mb-5 lg:mr-5 lg:mb-0 mx-auto AppFooter-icon-brand">
                        <Image 
                            src="/footer_logo kabinet.png"
                            alt="/footer_logo kabinet.png"
                            layout="fill"
                            objectFit="fill"
                        />
                    </div>
                    <div className="uppercase">
                        <h1 className="font-black xl:text-3xl">bem universitas esa unggul</h1>
                        <p  className="font-medium xl:text-xl">kabinet sodara 2020/2021</p>
                        <div className="mt-5">
                            <p className="text-sm">Lt. 2 Gedung PKM Universitas Esa Unggul <br />Jl. Arjuna Utara No. 9 RT 1/RW 2 Duri Kepa, <br /> Kecamatan Kb. Jeruk, Jakarta Barat - 11530</p>
                        </div>
                    </div>
                </div>
                <div className="flex md:flex-1 flex-wrap items-start justify-center lg:justify-end mt-3 md:mt-0">
                    {
                        footerLinks.map(item => (
                            <div className="px-3 py-3 lg:py-0" key={item.id}>
                                <a href={item.link} rel="noreferer noopener" target="_blank">
                                    <div className="AppFooter-icon-link">
                                        <Image 
                                            src={item.img}
                                            alt={item.img}
                                            width={92}
                                            height={92}
                                            layout="responsive"
                                            className="mx-auto"
                                            objectFit="fill"
                                        />
                                    </div>
                                </a>
                            </div>
                        ))
                    }
                </div>
            </div>
            <div className="container flex-1 mx-auto text-center lg:text-right lg:pr-3">
                <p className="text-white">Copyright Kementerian Komunikasi dan Informatika BEM UEU 2020/2021.</p>
            </div>
        </footer>
    )
}
 
export default AppFooter;