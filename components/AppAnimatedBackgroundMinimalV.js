// import gsap from "gsap";
// import { useEffect } from "react";
import AppYoutube from "./AppYoutube";
import dynamic from 'next/dynamic'

const AppSVGSchool = dynamic(() => import('./AppSVGSchool'))

const AppAnimatedBackgroundMinimalV = ({customString, repeatValue, noText, blockCaption, blockSubCaption, mainLogo, dummyDescription, videoId}) => {

    const defaultRepeat = repeatValue || 5
    const defaultString = customString || "lorem ipsum"
    // const timeline = gsap.timeline({repeat: -1, repeatDelay: 0})

    // useEffect(() => {
    //     if(noText == false) {
    //         Array.apply(null, {length : defaultRepeat}).map((x,i) => (
    //             timeline
    //             .set(".customString-" + i, {color: "rgba(255,255,255,1)", ease: "steps(1)"}, "<")
    //             .set(".customString-" + i, {color: "rgba(255,255,255,0)", ease: "steps(1)"}, "+=.5")
    //         ))
    //     }
    // })

    return (
        <>
            <div className="hidden xl:block flex-1 green-base-background overflow-hidden relative AppAnimatedBackground-100">
                <AppSVGSchool />
                {/* <div className="flex-1 absolute AppAnimatedBackground-parent-customString">
                {
                    noText == false ? 
                    Array.apply(null, {length : defaultRepeat}).map((x,i) =>(
                        <div className={"uppercase font-black text-18xl text-transparent text-center AppAnimatedBackground-stroke-customString whitespace-nowrap customString-" + i} key={i}>{defaultString}</div>
                    ))
                    : <></>
                }
                </div> */}
                <div className="flex-1 absolute bg-black w-full h-full top-0 bg-opacity-70 pt-20 flex-row">
                    <div className="flex items-center justify-center uppercase p-3 flex-row mt-10">
                        <div className="mx-10">
                            {
                                mainLogo ? 
                                <img src={mainLogo} width="300"></img>
                                : <></>
                            }
                        </div>
                        <div className="mx-10">
                            <h1 className="text-xl xl:text-5xl text-white font-black rounded-xl my-10">{blockCaption}</h1>
                            <p className="text-lg xl:text-2xl text-white font-black rounded-xl my-10">{blockSubCaption}</p>
                        </div>
                    </div>
                    <div className="flex-1 w-3/6 text-center mx-auto text-white my-10">
                        <p>{dummyDescription}</p>
                    </div>
                    <AppYoutube videoId={videoId} />
                </div>
            </div>
            <div className="block xl:hidden green-base-background pt-16">
                <div className="bg-black w-full h-full bg-opacity-60 p-3">
                    <div className="flex-1 md:flex items-center justify-center uppercase p-3">
                        <div className="flex-1">
                            {
                                mainLogo ? 
                                <img className="mx-auto" src={mainLogo} width="300"></img>
                                : <></>
                            }
                        </div>
                        <div className="flex-1">
                            <h1 className="text-xl xl:text-5xl text-white text-center md:text-left font-black rounded-xl mt-5">{blockCaption}</h1>
                            <p className="text-lg xl:text-2xl text-white text-center md:text-left font-black rounded-xl mt-5 mb-10">{blockSubCaption}</p>
                        </div>
                    </div>
                    <div className="w-11/12 text-center mx-auto text-white my-5">
                        {dummyDescription}
                    </div>
                    <AppYoutube videoId={videoId} />
                </div>
            </div>
        </>
    )
}
 
export default AppAnimatedBackgroundMinimalV;