import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import AppNavbarSub from "./AppNavbarSub";
import Image from 'next/image'

const AppNavbarLinks = [
    { href: '/', label: 'beranda', passive: false },
    { href: '/kementerian', label: 'kementerian', passive: true, sub: ["kemendak", "kemenkominfo", "kemenluk", "kemenag", "kemenkastrat", "kemenkeu", "kemensosling", "kemensenbud", "kemenpsdm", "kemenpora", "kemenkwu"]  },
    { href: '/bem-fakultas', label: 'bem fakultas', passive: false },
    { href: '/ukm', label: 'ukm', passive: false },
    { href: '/press-release', label: 'press release', passive: false },
    { href: '/curahan-sodara', label: 'curahan sodara', passive: false },
]

const AppNavbar = ({currentActive}) => {

    const [showMenu, setShowMenu] = useState(false);

    return (
        <>
            <nav className="fixed top-0 left-0 z-10 w-full bg-white shadow">
                <div className="container mx-auto">
                    <ul className="flex-1 flex-col xl:flex xl:flex-row items-center green-base-color py-3">
                        <li className="flex xl:mr-36 mr-0">
                            <div className="flex items-center">
                                <div className="xl:hidden w-1/12 ml-3 xl:ml-0 mr-3">
                                    <button onClick={() => setShowMenu(x => !x)}>
                                    {
                                        showMenu ?
                                        <FontAwesomeIcon icon="times" size="2x" className="green-base-color"></FontAwesomeIcon> :
                                        <FontAwesomeIcon icon="bars" size="2x" className="green-base-color"></FontAwesomeIcon>
                                        }
                                    </button>
                                </div>
                                <div className="mr-3 hidden xl:block">
                                    <div className="AppNavbar-logo">
                                        <Image 
                                            src="/header_logo kabinet.png"
                                            alt="/header_logo kabinet.png"
                                            width={90}
                                            height={90}
                                            layout="responsive"
                                            objectFit="fill"
                                        />
                                    </div>
                                </div>
                                <div className="w-full mr-auto">
                                    <Link href="/">
                                        <a>
                                            <div className="uppercase">
                                                <h1 className="font-black xl:text-2xl">bem universitas esa unggul</h1>
                                                <p>kabinet sodara 2020/2021</p>
                                            </div>
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </li>
                        <li className={showMenu ? "block flex-1 items-center xl:flex" : "hidden flex-1 items-center xl:flex"}>
                            <ul className="flex-1 xl:flex text-center items-center">
                                {
                                    AppNavbarLinks.map(item => (
                                        <li className="flex-1 uppercase" key={item.label}>
                                            {
                                                item.sub ? <AppNavbarSub sourceArray={item.sub} menuText={item.label} href={item.href} passiveMode={item.passive} active={currentActive == item.label} /> : <AppNavbarSub sourceArray={[]} menuText={item.label} href={item.href} passiveMode={item.passive} active={currentActive == item.label} />
                                            }
                                        </li>        
                                    ))
                                }
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}
 
export default AppNavbar;

