import gsap from "gsap";
import { useEffect, useRef } from "react";
import dynamic from 'next/dynamic'

const AppSVGSchool = dynamic(() => import('./AppSVGSchool'))

const AppAnimatedBackground = ({customString, repeatValue}) => {

    const defaultRepeat = repeatValue || 5
    const defaultString = customString || "lorem ipsum"
    const timeline = gsap.timeline({repeat: -1, repeatDelay: 0})

    useEffect(() => {
        Array.apply(null, {length : defaultRepeat}).map((x,i) => (
            timeline
            .set(".customString-" + i, {color: "rgba(255,255,255,1)", ease: "steps(1)"}, "<")
            .set(".customString-" + i, {color: "rgba(255,255,255,0)", ease: "steps(1)"}, "+=.5")
        ))
    })

    return (
        <>
            <div className="flex-1 green-base-background overflow-hidden relative AppAnimatedBackground">
                <AppSVGSchool />
                <div className="flex-1 absolute AppAnimatedBackground-parent-customString">
                {
                    Array.apply(null, {length : defaultRepeat}).map((x,i) =>(
                        <div className={"uppercase font-black text-18xl text-transparent text-center AppAnimatedBackground-stroke-customString whitespace-nowrap customString-" + i} key={i}>{defaultString}</div>
                        ))
                    }
                </div>
                <div className="flex-1 absolute bg-black w-full h-full top-0 bg-opacity-50"></div>
            </div>
        </>
    )
}
 
export default AppAnimatedBackground;