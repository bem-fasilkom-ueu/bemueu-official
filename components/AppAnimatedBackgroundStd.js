import gsap from "gsap";
import { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Image from 'next/image';
import dynamic from 'next/dynamic'

const AppSVGSchool = dynamic(() => import('./AppSVGSchool'))

const AppAnimatedBackgroundStd = ({customString, repeatValue, noText, sourceArray, blockCaption, miscPicture}) => {

    const defaultRepeat = repeatValue || 5
    const defaultString = customString || "lorem ipsum"
    const timeline = gsap.timeline({repeat: -1, repeatDelay: 0})
    const [showExecutiveMembers, setShowExecutiveMembers] = useState(false)

    useEffect(() => {
        if(noText == false) {
            Array.apply(null, {length : defaultRepeat}).map((x,i) => (
                timeline
                .set(".customString-" + i, {color: "rgba(255,255,255,1)", ease: "steps(1)"}, "<")
                .set(".customString-" + i, {color: "rgba(255,255,255,0)", ease: "steps(1)"}, "+=.5")
            ))
        }
    })

    return (
        <>
            <div className="hidden xl:block flex-1 green-base-background overflow-hidden relative AppAnimatedBackground">
                <AppSVGSchool />
                <div className="flex-1 absolute AppAnimatedBackground-parent-customString">
                {
                    noText == false ? 
                    Array.apply(null, {length : defaultRepeat}).map((x,i) =>(
                        <div className={"uppercase font-black text-18xl text-transparent text-center AppAnimatedBackground-stroke-customString whitespace-nowrap customString-" + i} key={i}>{defaultString}</div>
                    ))
                    : <></>
                }
                </div>
                <div className="absolute bg-black w-full h-full top-0 bg-opacity-70">
                    <div className="flex justify-center" onClick={() => setShowExecutiveMembers(true)}>
                        <h1 className="bg-white uppercase text-xl xl:text-5xl green-base-color p-3 text-center font-black rounded-xl my-10" onClick={() => setShowExecutiveMembers(true)}>{blockCaption}</h1>
                    </div>
                    <div className="container mx-auto">
                        <div className="grid grid-col grid-cols-1 md:grid-cols-2">
                        {
                            sourceArray.map(item => (
                                <div className={(item.role == "Presiden Mahasiswa" || item.role == "Bendahara Umum" ? "ml-4" : "mr-4") + " flex-1 lg:flex items-start"} key={item.img}>
                                    <div className={(item.role == "Presiden Mahasiswa" || item.role == "Bendahara Umum" ? "order-2" : "order-1") + " flex-1"}>
                                        <div className="mx-auto AppAnimatedBackgroundStd-membersPicture">
                                            <Image 
                                                src={item.img}
                                                alt={item.img}
                                                width={300}
                                                height={362.34939759}
                                                layout="responsive"
                                                objectFit="fill"
                                            />
                                        </div>
                                    </div>
                                    <div className={(item.role == "Presiden Mahasiswa" || item.role == "Bendahara Umum" ? "order-1 text-left" : "order-2 text-right") + " flex-1 text-white mt-10"}>
                                        <h1 className="font-black text-5xl my-3">
                                            <strong>{item.nama}</strong>
                                        </h1>
                                        <p className="font-black text-xl">{item.role}</p>
                                        <p>{item.faculty}</p>
                                        <p className="mt-3">{item.desc}</p>
                                    </div>
                                </div>
                            ))
                        }
                        </div>
                    </div>
                </div>
            </div>
            <div className="block xl:hidden green-base-background">
                <div className="bg-black w-full h-full bg-opacity-60 p-3">
                    <div className="flex justify-center">
                        <h1 className="bg-white uppercase text-xl xl:text-5xl green-base-color p-3 text-center font-black rounded-xl my-10" onClick={() => setShowExecutiveMembers(true)}>{blockCaption}</h1>
                    </div>
                    <div className="grid grid-col grid-cols-1 md:grid-cols-2">
                    {
                        sourceArray.map(item => (
                            <div className="flex-1 lg:flex items-start" key={item.img}>
                                <div className="flex-1">
                                    <div className="mx-auto AppAnimatedBackgroundStd-membersPicture">
                                        <Image 
                                            src={item.img}
                                            alt={item.img}
                                            width={300}
                                            height={362.34939759}
                                            layout="responsive"
                                            objectFit="fill"
                                        />
                                    </div>
                                </div>
                                <div className="flex-1 text-white mt-10">
                                    <h1 className="font-black text-2xl xl:text-5xl my-3">
                                        <strong>{item.nama}</strong>
                                    </h1>
                                    <p className="font-black text-xl">{item.role}</p>
                                    <p>{item.faculty}</p>
                                    <p className="mt-3">{item.desc}</p>
                                </div>
                            </div>
                        ))
                    }
                    </div>
                </div>
            </div>
            <div className={(showExecutiveMembers ? "block" : "hidden") + " fixed inset-0 bg-black bg-opacity-70 z-40"}>
                <div className="flex items-center justify-center h-full">
                    <div className="AppAnimatedBackgroundStd-dialog-groupPicture">
                        <Image 
                            src={miscPicture}
                            alt={miscPicture}
                            width={640}
                            height={640}
                            layout="responsive"
                            objectFit="fill"
                        />
                    </div>
                </div>
                <div className="flex items-center justify-center custom-rounded-button absolute top-0 right-0 mr-3 mt-3" onClick={() => setShowExecutiveMembers(false)}>
                    <FontAwesomeIcon icon="times" className="text-white"></FontAwesomeIcon>
                </div>
            </div>
        </>
    )
}
 
export default AppAnimatedBackgroundStd;