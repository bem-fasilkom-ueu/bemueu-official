import { useEffect } from "react";
import gsap from "gsap";

const AppHeader = () => {

    const timeline = gsap.timeline({repeat: -1, repeatDelay: 0, ease: "power4"})

    useEffect(() => {
        
        for (let i = 2; i >= 0; i--) {
            timeline
            .from(".bg-header-" + i, {display: "none", opacity: 0})
            .from(".bg-header-" + i, {display: "block", opacity: 100}, "+=6")
        }
    })

    return (
        <>
            <div className="green-secondary-background AppHeader"></div>
            <div className="bg-header-0 bg-no-repeat bg-center bg-cover AppHeader absolute inset-0"></div>
            <div className="bg-header-1 bg-no-repeat bg-center bg-cover AppHeader absolute inset-0"></div>
            <div className="bg-header-2 bg-no-repeat bg-center bg-cover AppHeader absolute inset-0"></div>
            <div className="absolute text-white text-center AppHeader flex flex-col inset-0">
                <div className="flex-1 mt-12">
                    <div className="flex flex-col justify-end h-full">
                        <span className="lg:text-2xl uppercase">Website Resmi</span>
                        <hr className="w-1/12 mx-auto border-2" />
                        <h1 className="font-black text-xl xl:text-6xl mt-5 uppercase">bem universitas esa unggul</h1>
                        <p className="text-md sm:text-3xl uppercase">kabinet sodara 2020/2021</p>
                    </div>
                </div>
                <div className="flex-1">
                    <div className="relative flex flex-col justify-end h-full">
                        <svg viewBox="0 0 2560 280" fill="#F0F0F0" stroke="#F0F0F0" xmlns="http://www.w3.org/2000/svg">
                            <path d="M2560 251.547V279.5H0V256.187H0.0400391V254.093C14.8267 254.453 30.12 254.44 45.8667 254.093C446.973 245.08 1144.69 13.52 1173.91 15.7467C1240.56 20.68 1304.05 15.4399 1364.91 -0.00012207H1364.92C1426.67 3.42654 1448.17 14.4799 1448.17 14.4799C1589.23 59.0799 1639.99 74.3599 1647.41 77.3466C1881.93 175.693 2309.12 263.853 2560 251.547Z" fill="#F0F0F0"/>
                        </svg>
                        <div className="gray-base-background py-px absolute bottom-0 left-0 right-0"></div>
                    </div>
                </div>
            </div>
        </>
    )
}
 
export default AppHeader;