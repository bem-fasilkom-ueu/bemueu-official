import { Component, createRef } from "react";

export default class AppComment extends Component {
    constructor(props) {
        super(props)
        this.utterancesParent = createRef()
    }

    componentDidMount() {
        let utterancesInstance = document.createElement("script")
        utterancesInstance.setAttribute("src", "https://utteranc.es/client.js")
        utterancesInstance.setAttribute("repo", "BEM-Fasilkom-UEU/bemueu-utterances")
        utterancesInstance.setAttribute("issue-term", "pathname")
        utterancesInstance.setAttribute("label", "utterances")
        utterancesInstance.setAttribute("theme", "boxy-light")
        utterancesInstance.setAttribute("crossorigin", "anonymous")
        utterancesInstance.setAttribute("async", true)
        this.utterancesParent.current.appendChild(utterancesInstance)
    }

    render() {
        return (
            <>
                <div ref={this.utterancesParent}>
                    <hr className="w-11/12 mx-auto border-t-2 my-6" />
                </div>
            </>
        )
    }
}