const withTM = require('next-transpile-modules')(['@glidejs/glide']);
const withPlugins = require('next-compose-plugins');
const withImages  = {
    images: {
        domains: [
            'fakeimg.pl',
        ],
    },
}
const nextConfig = {
    webpack5: false
}

module.exports = withPlugins([
    withImages,
    withTM,
], nextConfig);
